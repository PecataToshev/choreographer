import os
import re
from Tkinter import Tk
from tkFileDialog import askopenfilename

conf_last_dir = "last_dir"
conf_last_chosen_file = "last_file"
conf_last_chosen_prefix = "last_prefix"

config_file = "data.conf"
if os.path.exists(config_file) == False:
	open(config_file, "a").close()
f = open(config_file, "r+")
last_dir  = ""
last_file = ""
last_pref = ""
for line in f.readlines():
	if conf_last_dir in line:
		last_dir = line[len(conf_last_dir) + 1:]
	elif conf_last_chosen_file in line:
		last_file = line[len(conf_last_chosen_file) + 1:]
	elif conf_last_chosen_prefix in line:
		last_pref = line[len(conf_last_chosen_prefix) + 1:]

if last_dir == "":
	last_dir = "D:"

Tk().withdraw()
file_full_path = askopenfilename(initialdir=last_dir, filetypes=[("Wavefront", "*.obj")], title='Pick Wavefront file')
last_slash_index = file_full_path.rfind("/")
filename = file_full_path[last_slash_index+1:]
file_path = file_full_path[:last_slash_index]

if file_full_path.strip() == last_file.strip():
	variable_name_int = last_pref
else:
	print "\n" + filename
	variable_name_int = raw_input("""Select variable name:
	1 = Male
	2 = Female
	3 = Other
	4 = Stage
	""")

def variable_name_dict(x):
	return {
        1: 'personGeometryMale',
        2: 'personGeometryFemale',
		3: 'personGeometryOther',
		4: 'geometryStage',
    }[x]
variable_name = variable_name_dict(int(variable_name_int))

# write data to conf file
data = []
data.append(conf_last_dir + "=" + file_path)
data.append(conf_last_chosen_file + "=" + file_full_path)
data.append(conf_last_chosen_prefix + "=" + variable_name_int)
for line in f.readlines():
	if (conf_last_dir in line) or (conf_last_chosen_file in line) or (conf_last_chosen_prefix in line) or (line =="\n"):
		continue
	else:
		data.append(line)

f.seek(0)
f.write("\n".join(data))
f.truncate()
f.close()

# start read data
f = open(file_full_path, "r")
vertex = []
vertexN = []
faces = []
for line in f.readlines():
	if line[0:2] == "v ":
		vertex.append("\t[" + line[2:-1].replace(" ", ",") + "]")
	elif line[0:3] == "vn ":
		vertexN.append("\t[" + line[3:-1].replace(" ", ",") + "]")
	elif line[0:2] == "f ":
		faces.append("\t[0," + re.sub(r"([0-9]+)/[0-9]*/([0-9]+)", r"\1,\2", line[2:-1]).replace(" ", ",") + "]")

object_name = filename[0:filename.find(".")]

result = "var " + variable_name + " = {\nv:[\n"
result += ",\n".join(vertex)
result += "\n],\nvn: [\n"
result += ",\n".join(vertexN)
result += "\n],\nf: [\n"
result += ",\n".join(faces)
result += "\n]};"

f.close()

# write data to js files
def write_data_to_file(data, minified):
	result_file_path = file_path + "/" + object_name + ('.min' if minified else '') + ".js"
	if os.path.exists(result_file_path) == False:
		open(result_file_path, "a").close()
	f = open(result_file_path, "w")
	f.seek(0)
	f.write(data)
	f.truncate()
	f.close()

write_data_to_file("".join(result), False)
write_data_to_file("".join(result.replace("\n", "").replace("\t", "")), True)
