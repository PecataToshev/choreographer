package audio;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.IntStream;

import access.AccessLog;
import container.Box;
import javazoom.jl.decoder.Bitstream;
import javazoom.jl.decoder.BitstreamException;
import javazoom.jl.decoder.Decoder;
import javazoom.jl.decoder.DecoderException;
import javazoom.jl.decoder.Header;
import javazoom.jl.decoder.SampleBuffer;
import models.Rhythm;

public class MusicBeatFinder {
	/* CONSTANTS */
	// thread
	private final int DFTthreads = 4;

	// window
	private final double windowSizeInSec = 0.025;
	private final double windowStepInSec = 0.0125;
	private final int normalizationWindowSizeInSec = 5;
	private final double picksWindowSizeInSec = 0.2;

	// normalization
	private final int averageNormalizationCount = (int) Math.floor(normalizationWindowSizeInSec/windowStepInSec);
	private final int halfAverageNormalizationCount = averageNormalizationCount/2;
	private final int percentMin = 2;
	private final int percentMax = 2;
	private final int quantizationPrecision = 100;
	private final int numberOfNormalizedData = 32;//128;
	private final double baseFrequency = 27.50;
	private final double semitoneCoefficient = Math.pow(22000/27.5, 1.0/numberOfNormalizedData);

	// bands
	private final int bands = 3;
	private final int bandsFinderSizeInSec = 2;
	private final int minimumBandsSeparation = 2;
	private final double minimumBandGroupEnergy = 0.30/bands;//0.19
	private final double minimumBandGroupCountEnergy = 0.30/bands;
	private final double maximumConcatenateBandCoef = 1.50;

	/* END OF CONSTANTS */


	/* GENERATED DATA */
	// file
	private FileInputStream audio;

	// window
	private int windowSize;
	private int windowStep;

	// other
	private int frequency;
	private int resultLength;
	private double octavesNumber;
	private int averagePicksCount;
	private int[][] bandsBorder = null;

	/* END OF GENERATED DATA */


	/* RECIEVED DATA */
	private ArrayList<Rhythm> rhythms;
	private int timeStart; // in seconds
	private int timeEnd; // in seconds

	/* END OF RECIEVED DATA */

	public MusicBeatFinder(ArrayList<Rhythm> rhythms, int timeStart, int timeEnd) {
		this.rhythms = rhythms;
		this.timeStart = timeStart;
		this.timeEnd = timeEnd;
	}

	public void start() {
		try {
			double[][] refinedData = delog(discreteFourierTransformOnMusicSamples(setGeneratedConstants(getMusicSamples())));
			findBands(refinedData);
			refinedData = dataClarification(getEnergySum(refinedData));

			double[][] picks = picksFinder(refinedData);
			detectBeat(picks);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private short[] getMusicSamples() {
		Bitstream bitStream = new Bitstream(audio);
		Decoder decoder = new Decoder();
		short[] samples = new short[256 * 1024];
		int samplesLength = 0;

		Header frameHeader = null;
		SampleBuffer output = null;
		int chanelNumber;
		short[] buffer = null;

		try {

			while((frameHeader = bitStream.readFrame()) != null) {

				output = (SampleBuffer) decoder.decodeFrame(frameHeader, bitStream);
				frequency = decoder.getOutputFrequency();
				chanelNumber = decoder.getOutputChannels();
				buffer = output.getBuffer();

				// extend array
				if(samplesLength + buffer.length > samples.length) {
					short[] samples2 = new short[samples.length * 2];
					System.arraycopy(samples, 0, samples2, 0, samplesLength);
					samples = samples2;
				}

				// combine channels to 1
				int simplifiedBufferLength = buffer.length / chanelNumber;
				int bufferIndex = 0;
				for(int i = 0; i < simplifiedBufferLength; i++) {
					int tmpValue = 0;
					for(int j = 0; j < chanelNumber; j++)
						tmpValue += buffer[bufferIndex++];

					samples[samplesLength++] = (short)(tmpValue / chanelNumber);
				}
				bitStream.closeFrame();

			}

		} catch (BitstreamException | DecoderException e) {

			AccessLog.addLog("Error when getting bites", e, null);

		}

		// remove unused array space
		short[] samples2 = new short[samplesLength];
		System.arraycopy(samples, 0, samples2, 0, samplesLength);
		return samples2;
	}

	private short[] setGeneratedConstants(short[] musicSamples) {
		windowSize = (int) Math.round(frequency*windowSizeInSec);
		windowStep = (int) Math.round(frequency*windowStepInSec);
		resultLength = (musicSamples.length - windowSize)/windowStep;
		octavesNumber = (Math.log(8000)/Math.log(2))/(bands + 1);
		averagePicksCount = (int) (picksWindowSizeInSec*frequency/windowStep);
		return musicSamples;
	}

	private double[][] discreteFourierTransformOnMusicSamples(short[] musicSamples){
		double[][] dftResult = new double[resultLength][];

		FastFourierTransform ft = new FastFourierTransform();
		ft.Initialize(windowSize, 0, false);

		int partLength = resultLength / DFTthreads;
		int rest = resultLength - partLength*DFTthreads;
		FFTThread[] threadList = new FFTThread[DFTthreads + ((rest > 0) ? 1 : 0)];// if result length has remainder when divided by threads
		for(int i = 0;  i < DFTthreads;  i++) {
			FFTThread thread = new FFTThread(
					musicSamples,
					ft,
					dftResult,
					i * partLength,
					Math.min((i + 1) * partLength, resultLength),
					windowSize,
					windowStep
				);
			thread.start();
			threadList[i] = thread;
		}

		if(rest > 0) {
			FFTThread thread = new FFTThread(
					musicSamples,
					ft,
					dftResult,
					resultLength - rest,
					resultLength,
					windowSize,
					windowStep
				);
			thread.start();
			threadList[DFTthreads] = thread;
		}

		for(int i = 0;  i < DFTthreads + ((rest > 0) ? 1 : 0);  i++) {
			try {
				threadList[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		return dftResult;
	}

	private double[][] delog(double[][] dftData){
		double[][] delogData = new double[resultLength][numberOfNormalizedData];

		double[] indexFinder = new double[numberOfNormalizedData];
		indexFinder[0] = baseFrequency;
		for(int i = 1;  i < numberOfNormalizedData;  i++)
			indexFinder[i] = indexFinder[i-1]*semitoneCoefficient;


		int len = dftData[0].length;

		int tmpIndex = 0;
		int[] prefArray = new int[len];

		for(int p = 0;  p < len;  p++) {
			double f = p * (frequency / windowSize);

			while((f >= indexFinder[tmpIndex]) && (tmpIndex < indexFinder.length - 1))
				tmpIndex++;

			prefArray[p] = tmpIndex; 
		}

		for(int i = 0;  i < resultLength;  i++) {
			double[] tmpDftData = dftData[i];
			double[] tmpDelogData = delogData[i];

			for(int p = 0;  p < len;  p++)
				tmpDelogData[prefArray[p]] += tmpDftData[p];
		}
		return delogData;
	}

	private void quickSortDoubleWithIndex(double[] arr, int l, int r, int[] ind) {
		if(l >= r)
			return;

		int startL = l;
		int startR = r;

		double middle = arr[(r+l)/2];
		while(true) {
			while(arr[l] < middle)
				l++;

			while(arr[r] > middle)
				r--;

			if(l >= r)
				break;

			double tmp = arr[l];
			arr[l] = arr[r];
			arr[r] = tmp;
			int tmpInd = ind[l];
			ind[l] = ind[r];
			ind[r] = tmpInd;
			l++;
			r--;
		}

		quickSortDoubleWithIndex(arr, startL, r, ind);
		quickSortDoubleWithIndex(arr, r+1, startR, ind);
	}

	private void quickSortIntWithIndex(int[] arr, int l, int r, int[] ind) {
		if(l >= r)
			return;

		int startL = l;
		int startR = r;

		int middle = arr[(r+l)/2];
		while(true) {
			while(arr[l] < middle)
				l++;

			while(arr[r] > middle)
				r--;

			if(l >= r)
				break;

			int tmp = arr[l];
			arr[l] = arr[r];
			arr[r] = tmp;
			int tmpInd = ind[l];
			ind[l] = ind[r];
			ind[r] = tmpInd;
			l++;
			r--;
		}

		quickSortIntWithIndex(arr, startL, r, ind);
		quickSortIntWithIndex(arr, r+1, startR, ind);
	}

	private int[] countBands(double[][] dftResult, int frame, int frameCount, int[] fillIndex) {
		double[][][] TMPsum = new double[3][frameCount][numberOfNormalizedData];

		boolean[][] foundBandsArray = new boolean[frameCount][numberOfNormalizedData];
		int[] bandsCounter = new int[numberOfNormalizedData];

		int[] bandMap = new int[numberOfNormalizedData];
		ArrayList<BandGroup> maxFoundBands = new ArrayList<BandGroup>();
		ArrayList<Integer> foundBands = new ArrayList<Integer>();
		BandGroup[] bandGroups = new BandGroup[numberOfNormalizedData + 1];
		for(int i = 0;  i < numberOfNormalizedData + 1;  i++)
			bandGroups[i] = new BandGroup();

		double[] sum = new double[numberOfNormalizedData];
		int[] indexSum = new int[numberOfNormalizedData];
		for(int i = 0;  i < frameCount;  i++) {
			// calculate sum
			Arrays.fill(sum, 0.0);
			for(int j = 0;  j < frame;  j++) {
				double[] tmpResult = dftResult[i * frame + j];
				for(int p = 0;  p < numberOfNormalizedData;  p++) {
					sum[p] += tmpResult[p];
				}
			}

			// --
			System.arraycopy(sum, 0, TMPsum[0][i], 0, numberOfNormalizedData);

			// copy index array
			System.arraycopy(fillIndex, 0, indexSum, 0, numberOfNormalizedData);

			// sort
			quickSortDoubleWithIndex(sum, 0, numberOfNormalizedData-1, indexSum);

			// --
			System.arraycopy(sum, 0, TMPsum[1][i], 0, numberOfNormalizedData);



			double totalEnergy = 0;
			int usedBandGroups = 0;

			for(int p = numberOfNormalizedData-1;  p >= 0;  p--)
				totalEnergy += sum[p];

			double energyBound = totalEnergy*minimumBandGroupEnergy;
			BandGroup.setRequiredEnergyMin(energyBound);
			Arrays.fill(bandMap, 0);
			maxFoundBands.clear();
			foundBands.clear();

			for(int p = numberOfNormalizedData-1;  p >= 0;  p--) {
//				printInt(bandMap, numberOfNormalizedData);
//				tmpStringBuilder.append("\n");

				int index = indexSum[p];
				int bandMapIndex = bandMap[index];

				if(bandMapIndex > 0) {
					BandGroup bg = bandGroups[bandMapIndex];
					bg.addEnergy(sum[p]);
					if(!bg.isFound() && bg.checkEnergy()) {
						bg.setFound(true);
						foundBands.add(bandMapIndex);
					}
				} else {
					bandMapIndex = ++usedBandGroups;
					BandGroup bg = bandGroups[bandMapIndex];
					bandMap[index] = bandMapIndex;
					if(bg.reuse(index, index, sum[p]))
						foundBands.add(bandMapIndex);


					// check left
					for(int q = Math.max(0, index - minimumBandsSeparation);  q < index;  q++) {
						if(bandMap[q] > 0) {
							if(bg.isFound()) {
								bg.setFound(false);
								foundBands.remove(new Integer(bandMapIndex));
							}

							bandMapIndex = bandMap[q];
							bg = bandGroups[bandMapIndex];
							bg.setEnd(index);
							bg.addEnergy(sum[p]);

							for(int z = q + 1;  z <= index;  z++)
								bandMap[z] = bandMapIndex;

							if(!bg.isFound() && bg.checkEnergy()) {
								bg.setFound(true);
								foundBands.add(bandMapIndex);
							}
							break;
						}
					}


					// check right
					for(int q = Math.min(numberOfNormalizedData - 1, index + minimumBandsSeparation);  q > index;  q--) {
						if(bandMap[q] > 0) {
							if(bg.isFound()) {
								bg.setFound(false);
								foundBands.remove(new Integer(bandMapIndex));
							}

							int nbi = bandMap[q];
							BandGroup nbg = bandGroups[nbi];
							nbg.extend(bg);
							for(int z = nbg.getStart();  z < q;  z++)
								bandMap[z] = nbi;

							if(!nbg.isFound() && nbg.checkEnergy()) {
								nbg.setFound(true);
								foundBands.add(nbi);
							}
							break;
						}
					}
				}

				if(foundBands.size() > maxFoundBands.size()) {
					maxFoundBands.clear();
					for(int q = 0; q < foundBands.size(); q++) {
						maxFoundBands.add(bandGroups[foundBands.get(q)].clone());
					}
				}
			}

			maxFoundBands.sort(new Comparator<BandGroup>() {
				@Override
				public int compare(BandGroup a, BandGroup b) {
					return a.getStart() - b.getStart();
				}
			});



			double[] sums = TMPsum[2][i];
			boolean[] tmpFoundBands = foundBandsArray[i];
			for(int p = 0; p < maxFoundBands.size(); p++) {
				BandGroup bg = maxFoundBands.get(p);
				int start = Math.max(bg.getStart(), 0);
				int end   = Math.min(bg.getEnd(), numberOfNormalizedData-1);
				for(int q = start;  q <= end;  q++) {
					tmpFoundBands[q] = true;
					bandsCounter[q]++;
					sums[q] = 128 + p * 64;
				}
			}

//
//			dataToPrint.append(tmpStringBuilder);
//			tmpStringBuilder.setLength(0);
//			dataToPrint.append("\nSUMMARRY:\n" + i + "\t" + maxFoundBands.size() + "\n" + totalEnergy + "\t" + energyBound + "\n");
//			dataToPrint.append(maxFoundBands.toString());
//			dataToPrint.append("\n");
//			dataToPrint.append(Arrays.toString(sum));
//			dataToPrint.append("\n\n");

		}
//		DrawData("0-sums", TMPsum[0], 2);
//		DrawData("1-sums-Sort", TMPsum[1], 2);
//		DrawData("2-sums-Bands", TMPsum[2], 1);

		// printing
//		for(int i = 0;  i < frameCount;  i++) {
//			tmpStringBuilder.append(String.format("\n%4d  ", i));
//			printBoolean(foundBandsArray[i], numberOfNormalizedData);
//		}
//		dataToPrint.append(tmpStringBuilder);
//		tmpStringBuilder.setLength(0);
//
//		dataToPrint.append("\n\n\n\n\n\nBands Sum\n");
//		printInt(bandsCounter, numberOfNormalizedData);
//		dataToPrint.append(tmpStringBuilder);
//		tmpStringBuilder.setLength(0);
//		printBandsCountToImage(bandsCounter, numberOfNormalizedData, "3-bandsCounted");

		// remove noise in bands
		int noiseInBands = bandsCounter[0];
		for(int i = 1;  i < numberOfNormalizedData;  i++)
			noiseInBands = Math.min(noiseInBands, bandsCounter[i]);

		for(int i = 0;  i < numberOfNormalizedData;  i++)
			bandsCounter[i] -= noiseInBands;

//		printBandsCountToImage(bandsCounter, numberOfNormalizedData, "4-bandsRemovedNoise");
//		dataToPrint.append("\n\nBands removed noise\n");
//		printInt(bandsCounter, numberOfNormalizedData);
//		dataToPrint.append(tmpStringBuilder);
//		tmpStringBuilder.setLength(0);
//		dataToPrint.append("\n\n\n");

		return bandsCounter;
	}

	private void findBands(double[][] dftResult) {
		int frame = (int) Math.floor(bandsFinderSizeInSec / windowStepInSec);
		int frameCount = resultLength / frame;

		int[] boundsCounterIndex = IntStream.range(0, numberOfNormalizedData).toArray();
		int[] bandsCounter = countBands(dftResult, frame, frameCount, boundsCounterIndex);
//		printBandsCountToImage(bandsCounter, numberOfNormalizedData, "5-BandsCountedGet");
//		printInt(bandsCounter, numberOfNormalizedData);
//		tmpStringBuilder.append("\n");
//		printInt(boundsCounterIndex, numberOfNormalizedData);
//		tmpStringBuilder.append("\n\n");


		// sort array by value and store index
		quickSortIntWithIndex(bandsCounter, 0, numberOfNormalizedData-1, boundsCounterIndex);
//		printBandsCountToImage(bandsCounter, numberOfNormalizedData, "6-BandsCountedSorted");


//		dataToPrint.append("\n\nBands sorted\n");
//		printInt(bandsCounter, numberOfNormalizedData);
//		dataToPrint.append(tmpStringBuilder);
//		tmpStringBuilder.setLength(0);
//		dataToPrint.append("\n");
//		printInt(boundsCounterIndex, numberOfNormalizedData);
//		tmpStringBuilder.append("\n\n");
//		dataToPrint.append(tmpStringBuilder);
//		tmpStringBuilder.setLength(0);

		int[] bandMap = new int[numberOfNormalizedData];
		ArrayList<BandGroup> maxFoundBands = new ArrayList<BandGroup>();
		ArrayList<Integer> foundBands = new ArrayList<Integer>();
		boolean hasBeenChanged = false;
		BandGroup[] bandGroups = new BandGroup[numberOfNormalizedData + 1];
		for(int i = 0;  i < numberOfNormalizedData + 1;  i++)
			bandGroups[i] = new BandGroup();

		int usedBands = 0;

		double requiredEnergy = 0.0;
		for(int i = 0;  i < numberOfNormalizedData;  i++)
			requiredEnergy += bandsCounter[i];
		requiredEnergy = requiredEnergy*minimumBandGroupCountEnergy;
		BandGroup.setRequiredEnergyMin(requiredEnergy);
		BandGroup.setRequiredEnergyMax(requiredEnergy*maximumConcatenateBandCoef);

//		dataToPrint.append("\nMinimum energy " + requiredEnergy + "\nMaximum energy " + requiredEnergy*maximumConcatenateBandCoef + "\n\n");

		for(int i = numberOfNormalizedData - 1;  i >= 0;  i--) {
			if(bandsCounter[i] <= 0)
				continue;

			int index = boundsCounterIndex[i];
			int bandMapIndex = bandMap[index];
			hasBeenChanged = false;

			if(bandMapIndex > 0) {
				BandGroup bg = bandGroups[bandMapIndex];
				bg.addEnergy(bandsCounter[i]);
				if(!bg.isFound() && bg.checkEnergy()) {
					bg.setFound(true);
					foundBands.add(bandMapIndex);
					hasBeenChanged = true;
				}

//				tmpStringBuilder.append(" ");
//				printInt(bandMap, numberOfNormalizedData);
//				tmpStringBuilder.append(" " + (hasBeenChanged ? "t" : " ") + "\t\t");
			} else {
				bandMapIndex = ++usedBands;
				BandGroup bg = bandGroups[bandMapIndex];
				bandMap[index] = bandMapIndex;
				if(bg.reuse(index, index, bandsCounter[i])) {
					foundBands.add(bandMapIndex);
					hasBeenChanged = true;
				}

//				tmpStringBuilder.append("+");
//				printInt(bandMap, numberOfNormalizedData);
//				tmpStringBuilder.append(" " + (hasBeenChanged ? "t" : " ") + "\t\t");

				// check left
				for(int q = index-1;  q >= Math.max(0, index - minimumBandsSeparation);  q--) {
					if(bandMap[q] > 0) {
						if(bandGroups[bandMap[q]].stopConcatenating() && bg.stopConcatenating())
							break;

						if(bg.isFound()) {
							bg.setFound(false);
							foundBands.remove(new Integer(bandMapIndex));
							hasBeenChanged = true;
						}

						bandMapIndex = bandMap[q];
						bg = bandGroups[bandMapIndex];
						bg.setEnd(index);
						bg.addEnergy(bandsCounter[i]);

						for(int z = q + 1;  z <= index;  z++)
							bandMap[z] = bandMapIndex;

						if(!bg.isFound() && bg.checkEnergy()) {
							bg.setFound(true);
							foundBands.add(bandMapIndex);
						}
					}
				}


				// check right
				for(int q = index + 1;  q < Math.min(numberOfNormalizedData - 1, index + minimumBandsSeparation);  q++) {
					if(bandMap[q] > 0) {
						if(bandGroups[bandMap[q]].stopConcatenating() && bg.stopConcatenating())
							break;

						if(bg.isFound()) {
							bg.setFound(false);
							foundBands.remove(new Integer(bandMapIndex));
							hasBeenChanged = true;
						}

						int nbi = bandMap[q];
						BandGroup nbg = bandGroups[nbi];
						nbg.extend(bg);
						for(int z = nbg.getStart();  z < q;  z++)
							bandMap[z] = nbi;

						if(!nbg.isFound() && nbg.checkEnergy()) {
							nbg.setFound(true);
							foundBands.add(nbi);
						}
						break;
					}
				}
			}

			boolean b = false;
			if(foundBands.size() > maxFoundBands.size() || (hasBeenChanged && (foundBands.size() == maxFoundBands.size()))) {
				b = true;
				maxFoundBands.clear();
				for(int q = 0; q < foundBands.size(); q++) {
					maxFoundBands.add(bandGroups[foundBands.get(q)].clone());
				}
			}

//			printInt(bandMap, numberOfNormalizedData);
//			tmpStringBuilder.append(" " + foundBands.size() + "(" + maxFoundBands.size() + ") " + (b ? "t" : "") + "\n");
		}

//		dataToPrint.append(tmpStringBuilder);
//		tmpStringBuilder.setLength(0);

		maxFoundBands.sort(new Comparator<Group>() {
			@Override
			public int compare(Group a, Group b) {
				return a.getStart() - b.getStart();
			}
		});

		int[] printer = new int[numberOfNormalizedData];
		for(int i = 0;  i < maxFoundBands.size();  i++) {
			BandGroup bg = maxFoundBands.get(i);
			int start = Math.max(0, bg.getStart());
			int end = Math.min(numberOfNormalizedData-1, bg.getEnd());
			for(int z = start;  z <= end;  z++)
				printer[z] = i+1;
		}
//		printBandsCountToImage(printer, numberOfNormalizedData, "7-BandsCountFound");



	}

	public double[][] getEnergySum(final double[][] dftResult){
		int len = dftResult[0].length;
		int[] fb = new int[len];

		for(int i = 1; i < len; i++) {
			fb[i] = (int)Math.floor((Math.log((i * frequency / (double) windowSize) / baseFrequency) / Math.log(2)) / octavesNumber);
			if(fb[i] >= bands)
				fb[i] = bands - 1;
		}


		double[][] energySum = new double[resultLength][];
		for(int i = 0;  i < resultLength;  i++) {
			double[] res3 = dftResult[i];
			double[] c = new double[bands];
			energySum[i] = c;
			for(int p = 1;  p < len;  p++)
				c[fb[p]] += res3[p];

		}

		return energySum;
	}

	private int calculateSumCountPosition(double element, double min, double max) {
		return (int) Math.floor((element - min) / (max - min) * quantizationPrecision);
	}

	private double[][] dataClarification(double[][] energySum){

		double[][] displayer = new double[resultLength][bands];

		for(int b = 0;  b < bands;  b++) {

			// find min and max
			double min = energySum[0][b];
			double max = energySum[0][b];
			for(int i = 1;  i < resultLength;  i++) {
				double tmpSum = energySum[i][b];
				if(tmpSum > max)
					max = tmpSum;

				if(tmpSum < min)
					min = tmpSum;
			}



			// create and fill normalization array
			int[] sum = new int[averageNormalizationCount];
			for(int i = 0;  i < averageNormalizationCount;  i++)
				sum[calculateSumCountPosition(energySum[i][b], min, max)]++;


			// create pointers and find number to be excluded
			int positionL = 0;
			int positionR = 0;

			int numberL = averageNormalizationCount/quantizationPrecision*percentMin;
			int numberR = averageNormalizationCount/quantizationPrecision*percentMax;

			// start normalizing
			for(int i = 0;  i < averageNormalizationCount && (numberL > 0 || numberR > 0);  i++) {
				if(numberL > 0) {
					numberL -= sum[i];
					if(numberL <= 0)
						positionL = i;
				}

				if(numberR > 0) {
					numberR -= sum[averageNormalizationCount - i - 1];
					if(numberR <= 0)
						positionR = averageNormalizationCount - i - 1;
				}
			}


			for(int i = 0;  i < resultLength;  i++) {


				int tmpInd;
				double currentSum = energySum[i][b];

				// removing if inside
				if(i - halfAverageNormalizationCount >= 0) {
					tmpInd = calculateSumCountPosition(energySum[i-halfAverageNormalizationCount][b], min, max);

					if(tmpInd < positionL)
						numberL++;

					sum[tmpInd]--;
				}


				// adding if inside
				if(i + halfAverageNormalizationCount < resultLength) {
					tmpInd = calculateSumCountPosition(energySum[i+halfAverageNormalizationCount][b], min, max);

					if(tmpInd > positionR)
						numberR++;

					sum[tmpInd]++;
				}


				//current value
				sum[calculateSumCountPosition(currentSum, min, max)]++;


				// recalculating left and right position
				while(numberR < 0 && positionR + 1 != averageNormalizationCount)
					numberR += sum[++positionR];

				while(numberR > 0 && positionR != 0)
					numberR -= sum[--positionR];

				while(numberL < 0 && positionL != 0)
					numberL += sum[--positionL];

				while(numberL > 0 && positionL + 1 != averageNormalizationCount)
					numberL -= sum[++positionL];


				// add to displayer
				displayer[i][b] = (quantizationPrecision*(currentSum - min) - positionL*(max - min))/((max - min)*(positionR - positionL));

			}
		}

		return displayer;
	}

	private double[][] picksFinder(double[][] refinedData){
		double[][] displayer = new double[resultLength][bands];

		int bc = 32 - Integer.numberOfLeadingZeros(averagePicksCount) + 1;
		double[] maxValue = new double[1 << bc];
		int[] maxIndex = new int[1 << bc];
		int mask = (1 << bc) - 1;

		for(int p = 0;  p < bands;  p++) {

			int start = 0;
			int end = 0;
			int lastPick = -1000000;

			for(int i = 0;  i < resultLength;  i++) {
				double value = refinedData[i][p];
				while(end != start) {
					int previous = (end - 1) & mask;

					if (maxValue[previous] > value)
						break;

					end = previous;
				}
				maxValue[end] = value;
				maxIndex[end] = i;
				end = (end + 1) & mask;

				int cureentMaxIndex = maxIndex[start];
				if(cureentMaxIndex + averagePicksCount <= i) {
					if(cureentMaxIndex - lastPick >= (averagePicksCount >> 1)) {
						displayer[cureentMaxIndex][p] = refinedData[cureentMaxIndex][p];
						lastPick = cureentMaxIndex;
					}
					start = (start + 1) & mask;

				}
			}
		}

		return displayer;
	}

	private void detectBeat(double[][] picks) {
		int rythmLen = 4;
		Rythm[] rythm = new Rythm[rythmLen];
		rythm[0] = new Rythm("7/8 -> 3-2-2", new Box.Double(0.5, 1.1), new Box.Double[] {new Box.Double(0.0, 1.0), new Box.Double(0.5, 0.5), new Box.Double(0.75, 0.5)});
		rythm[1] = new Rythm("7/8 -> 2-2-3", new Box.Double(0.3, 0.7), new Box.Double[] {new Box.Double(0.0, 0.5), new Box.Double(0.25, 0.5), new Box.Double(0.5, 1.0)});
		rythm[2] = new Rythm("2/4 -> 2-2", new Box.Double(0.3, 0.7), new Box.Double[] {new Box.Double(0, 1.0), new Box.Double(0.5, 0.5)});
		rythm[3] = new Rythm("9/8 -> 2-3-2-2", new Box.Double(0.3, 0.7), new Box.Double[] {new Box.Double(0, 0.5), new Box.Double(0.20, 1.0), new Box.Double(0.6, 0.5), new Box.Double(0.8, 0.5)});
//
//		COMMENTED BECAUSE IS NOT BEING TESTED FOR NOW
//
//		dataToPrint.append("\n\n\n\n\nPicks\n");
//		for(int i = 0;  i < resultLength;  i++) {
//			dataToPrint.append(String.format("%5d ", i));
//			printDouble(picks[i], bands);
//			dataToPrint.append(tmpStringBuilder);
//			dataToPrint.append("\n");
//		}

		int picksNumber = 1 << 4;
		int pickIndex = 0;
		int[] index = new int[picksNumber];
		double[] data = new double[picksNumber];

		for(int b = 0;  b < bands;  b++) {
			pickIndex = 0;

			for(int i = 0;  i < resultLength;  i++) {
				if(picks[i][b] == 0.0)
					continue;


				if(pickIndex >= picksNumber) {
					pickIndex = picksNumber;
					picksNumber = picksNumber << 1;
					int[] tmpIndex = new int[picksNumber];
					double[] tmpData = new double[picksNumber];

					System.arraycopy(index, 0, tmpIndex, 0, pickIndex);
					System.arraycopy(data, 0, tmpData, 0, pickIndex);
					index = tmpIndex;
					data = tmpData;
				}

				index[pickIndex] = i;
				data[pickIndex] = picks[i][b];
				pickIndex++;
			}


			int sumNumber = -1;
			double[] sum = null;//new double [sumNumber];
			int indexInIndex = 0;
			int mask = 0;
			int distanceIndex = 0;
			int predictedIndex = 0;
			int[] distance = null;

			for(int ri = 0;  ri < rythmLen;  ri++){
				Rythm r = rythm[ri];
				if(sumNumber < r.getPatternLength()) {
					sumNumber = r.getPatternLength();
					sum = new double [sumNumber];
					distance = new int[sumNumber];
					mask = (1 << (32 - Integer.numberOfLeadingZeros(sumNumber) + 1)) - 1;
				}

				sumNumber = r.getPatternLength();
				indexInIndex = 0;
				distanceIndex = 0;
				predictedIndex = index[0];
				Arrays.fill(sum, 0.0);
				Arrays.fill(distance, 0);

				for(int s = 1;  s < sumNumber;  s++)
					distance[s-1] = (int)Math.floor(((double)(index[s] - index[s-1]))/(r.getPatternAt(s).l - r.getPatternAt(s-1).l));
				distance[sumNumber-1] = (int)Math.floor(((double)(index[sumNumber-1] - index[sumNumber-2]))/(1.0 - r.getPatternAt(sumNumber-1).l));

				while(indexInIndex < sumNumber) {
					predictedIndex += distance[distanceIndex];
					if(predictedIndex != index[indexInIndex]) {
						while(predictedIndex > index[indexInIndex])
							indexInIndex++;

						while(predictedIndex < index[indexInIndex])
							indexInIndex--;
					}

					if(predictedIndex == index[indexInIndex]) {
						sum[distanceIndex] += data[indexInIndex];
					}
					distanceIndex = (distanceIndex + 1) & mask;
					indexInIndex++;
				}

//				dataToPrint.append(Arrays.toString(sum));
			}

		}
	}

}
