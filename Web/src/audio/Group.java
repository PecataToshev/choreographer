package audio;

public class Group {
	private int start = -1;
	private int end   = -1;

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = end;
	}





	public Group clone() {
		Group clonning = new Group();
		clonning.start = start;
		clonning.end = end;
		return clonning;
	}

	public Object cloneTo(Object a) {
		if(a instanceof Group) {
			((Group) a).start = start;
			((Group) a).end = end;
		}

		return a;
	}

	public void extend(Group b) {
		start = ((b.start < start) ? b.start : start);
		end = ((b.end > end) ? b.end : end);
	}

	public void reset() {
		start = -1;
		end   = -1;
	}

	public void reuse(int start, int end) {
		this.start = start;
		this.end = end;
	}

	public void swap(Group b) {
		if(this == b)
			return;


		int tmpStart = b.start;
		int tmpEnd = b.end;

		b.start = start;
		b.end = end;

		start = tmpStart;
		end = tmpEnd;
	}





	@Override
	public String toString() {
		return "{" + start + " - " + end + "}";
	}
}
