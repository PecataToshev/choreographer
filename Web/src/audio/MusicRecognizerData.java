package audio;

import java.io.FileInputStream;
import java.util.ArrayList;

import models.Music;
import models.Rhythm;

public class MusicRecognizerData {
	private FileInputStream fileInputStream;
	private Music music;
	private ArrayList<Rhythm> rhythms;
	private int timeStart; // in seconds
	private int timeEnd; // in seconds

	public MusicRecognizerData(Music music, ArrayList<Rhythm> rhythms, int timeStart, int timeEnd) {
		super();
		this.music = music;
		this.rhythms = rhythms;
		this.timeStart = timeStart;
		this.timeEnd = timeEnd;
	}

	public FileInputStream getFileInputStream() {
		return fileInputStream;
	}

	public ArrayList<Rhythm> getRhythms() {
		return rhythms;
	}

	public int getTimeStart() {
		return timeStart;
	}

	public int getTimeEnd() {
		return timeEnd;
	}

	public boolean updateInDB(String data) {
		music.getAccessCode();
		return false;
	}
}
