package audio;

import container.Box;

class Rythm {

	private String name;
	Box.Double range;
	private Box.Double[] pattern = null; // l -> position, r -> value
	private int patternLength = -1;

	public Rythm(String name, Box.Double range, Box.Double[] pattern) {
		super();
		this.name = name;
		this.range = range;
		this.pattern = pattern;
		patternLength = pattern.length;
	}

	public String getName() {
		return name;
	}

	public Box.Double getRange() {
		return range;
	}

	public Box.Double[] getPattern() {
		return pattern;
	}

	public Box.Double getPatternAt(int index) {
		if(index < patternLength)
			return pattern[index];

		 return null;
	}

	public int getPatternLength() {
		return patternLength;
	}
}
