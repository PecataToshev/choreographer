package audio;

public class FastFourierTransform {

	/// <summary>
	/// DFT Class
	/// </summary>

	private double mDFTScale;	   // DFT ONLY Scale Factor
	private int mLengthTotal;	// mN + mZp
	private int mLengthHalf;	 // (mN + mZp) / 2

	private double[] mCosTerm;	  // Caching of multiplication terms to save time
	private double[] mSinTerm;	  // on smaller DFT's


	/// <summary>
	/// Pre-Initializes the DFT.
	/// Must call first and this anytime the FFT setup changes.
	/// </summary>
	/// <param name="inputDataLength"></param>
	/// <param name="zeroPaddingLength"></param>
	/// <param name="forceNoCache">True will force the DFT to not use pre-calculated caching.</param>
	public void Initialize(int inputDataLength, int zeroPaddingLength, boolean forceNoCache)
	{
		// Save the sizes for later
		mLengthTotal = inputDataLength + zeroPaddingLength;
		mLengthHalf = (mLengthTotal / 2) + 1;

		// Set the overall scale factor for all the terms
		mDFTScale = Math.sqrt(2) / (double)(inputDataLength + zeroPaddingLength);				 // Natural DFT Scale Factor										   // Window Scale Factor
		mDFTScale *= ((double)(inputDataLength + zeroPaddingLength)) / (double)inputDataLength;   // Account For Zero Padding						   // Zero Padding Scale Factor


		// Try to make pre-calculated sin/cos arrays. If not enough memory, then 
		// use a brute force DFT.
		// Note: pre-calculation speeds the DFT up by about 5X (on a core i7)
		mCosTerm = new double[mLengthTotal * mLengthTotal];
		mSinTerm = new double[mLengthTotal * mLengthTotal];

		double scaleFactor = 2.0 * Math.PI / mLengthTotal;
		double scale =  mDFTScale / 32768;

		//Parallel.For(0, mLengthHalf, (j) =>
		for (int j = 0; j < mLengthHalf; j++)
		{
			double a = j * scaleFactor;
			int multiplication = j * mLengthTotal;
			for (int k = 0; k < mLengthTotal; k++)
			{
				mCosTerm[multiplication + k] = Math.cos(a * k) * scale;
				mSinTerm[multiplication + k] = Math.sin(a * k) * scale;
			}
		} //);
	}

	/// <summary>
	/// A brute force DFT - Uses Task / Parallel pattern
	/// </summary>
	/// <param name="timeSeries"></param>
	/// <returns>Complex[] result</returns>
	public double[] Dft(short[] timeSeries, int index, int length)
	{
		if(length != mLengthTotal)throw new Error("Implementation error.");
		int n = mLengthTotal;
		int m = mLengthHalf;
		int bound = index + length;
		double[] result = new double[m];

		for(int j = 0; j < m; j++)
		{
			double r = 0, i = 0;
			int multiplication = j * n;
			for (int k = index, k2 = 0; k < bound; k++,k2++)
			{
				short tmpSeries = timeSeries[k];
				r += tmpSeries * mCosTerm[multiplication + k2];
				i -= tmpSeries * mSinTerm[multiplication + k2];
			}

			result[j] = (r * r + i * i);
		}

		result[0] /= 2;
		result[mLengthHalf - 1] /= 2;

		return result;
	}

	public double[] FrequencySpan(double samplingFrequencyHz)
	{
		int points = mLengthHalf;
		double[] result = new double[points];
		double stopValue = samplingFrequencyHz / 2.0;
		double increment = stopValue / ((double)points - 1.0);

		for (int i = 0; i < points; i++)
			result[i] += increment * i;

		return result;
	}

}

class FFTThread extends Thread
{
	private short[] musicSamples;
	private FastFourierTransform ft;
	private double[][] dftResult;
	private int from;
	private int to;
	private int windowSize;
	private int windowStep;

	public FFTThread(short[] musicSamples, FastFourierTransform ft, double[][] dftResult, int from, int to,
			int windowSize, int windowStep) {
		super();
		this.musicSamples = musicSamples;
		this.ft = ft;
		this.dftResult = dftResult;
		this.from = from;
		this.to = to;
		this.windowSize = windowSize;
		this.windowStep = windowStep;
	}

	@Override
	public void run()
	{
		for(int i = from;  i < to;  i++)
			dftResult[i] = ft.Dft(musicSamples, i*windowStep, windowSize);
	}
}