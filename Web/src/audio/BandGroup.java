package audio;

class BandGroup extends Group {
	private static double requiredEnergyMin = -1.0;
	private static double requiredEnergyMax = -1.0;

	public static void setRequiredEnergyMin(double requiredEnergy) {
		BandGroup.requiredEnergyMin = requiredEnergy;
	}
	public static void setRequiredEnergyMax(double requiredEnergy) {
		BandGroup.requiredEnergyMax = requiredEnergy;
	}

	public static void swap(BandGroup a, BandGroup b) {
		if(a == b)
			return;

		double tmpEnergy = b.energy;

		b.energy = a.energy;
		a.energy = tmpEnergy;
		a.swap(b);
	}





	private double energy = 0;
	private boolean found = false;

	public void addEnergy(double energy) {
		this.energy += energy;
	}

	public double getEnergy() {
		return energy;
	}

	public boolean checkEnergy() {
		return energy > requiredEnergyMin;
	}

	public boolean stopConcatenating() {
		return energy >= requiredEnergyMax;
	}

	public boolean isFound() {
		return found;
	}

	public void setFound(boolean found) {
		this.found = found;
	}





	public void extend(BandGroup b) {
		energy += b.energy;
		super.extend(b);
	}

	public void reset() {
		energy = 0;
		super.reset();
		found = false;
	}

	public boolean reuse(int start, int end, double energy) {
		super.reuse(start, end);
		this.energy = energy;
		this.found = checkEnergy();
		return found;
	}

	public BandGroup clone() {
		BandGroup cloning = (BandGroup) super.cloneTo(new BandGroup());
		cloning.energy = energy;
		cloning.found = found;
		return cloning;
	}







	@Override
	public String toString() {
		return "{(" + (found ? "t" : "f") + ")" + super.getStart() + " - " + energy + " - " + super.getEnd() + "}";
	}
}
