package audio;

import java.util.LinkedList;
import java.util.Queue;

public class Recognizer extends Thread {
	private Queue<MusicRecognizerData> dataToRecognize = new LinkedList<>();

	private boolean running = false;

	public Recognizer() {}

	public void run() {}

	public synchronized void addDataToRecognize(MusicRecognizerData data) {
		dataToRecognize.add(data);
		if(!running)
			main();
	}

	private void main() {
		running = true;
		MusicRecognizerData mrd;
		while((mrd = dataToRecognize.poll()) != null) {
			MusicBeatFinder mbf = new MusicBeatFinder(mrd.getRhythms(), mrd.getTimeStart(), mrd.getTimeEnd());
			mbf.start();
		}
		running = false;
	}
}
