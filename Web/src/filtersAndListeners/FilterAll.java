package filtersAndListeners;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import head.Config;
import head.SessionHandler;

@WebFilter("/*")
public class FilterAll implements Filter {

	final private String encoding = "UTF-8";

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
		//set encoding
		request.setCharacterEncoding(encoding);
		response.setCharacterEncoding(encoding);

		HttpServletRequest req = (HttpServletRequest) request;
		final String path = req.getServletPath();
		final boolean isLoggedIn = SessionHandler.isLoggedIn((HttpServletRequest) request);

		if(path.startsWith(Config.servletPrefixApp)) {
			if(isLoggedIn == false) {
				HttpServletResponse httpResponse = (HttpServletResponse) response;
				httpResponse.sendRedirect(Config.urlProjectPrefix + Config.servletPrefixSign + "in.jsp"); // -> sign/in.jsp
				return;
			}
		} else if(path.startsWith(Config.servletPrefixSign)) {
			if(isLoggedIn) {
				if(path.startsWith(Config.servletPrefixSign + "Out") == false){ // not signing out
					HttpServletResponse httpResponse = (HttpServletResponse) response;
					httpResponse.sendRedirect(Config.urlProjectPrefix + Config.servletPrefixApp); // -> app/
					return;
				}
			}
		} else if(path.startsWith(Config.servletPrefixPrivate)) {
			HttpServletResponse httpResponse = (HttpServletResponse) response;
			httpResponse.sendRedirect(Config.urlProjectPrefix); // -> /
			return;
		}


		filterChain.doFilter(request, response);
	}

	public void init(FilterConfig filterConfig) throws ServletException {}

	public void destroy() {}
}