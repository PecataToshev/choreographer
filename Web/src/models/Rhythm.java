package models;

import javax.persistence.*;

import container.GSONSkipSerializationRhythmMap;
import head.Basics;


@Entity
@Table(name="rhythm")
@NamedQueries({
	@NamedQuery(name="Rhythm.findBuiltIn", query="SELECT r FROM Rhythm r WHERE r.builtIn = true"),
	@NamedQuery(name="Rhythm.findAccessible", query="SELECT r FROM Rhythm r WHERE r.builtIn = true OR r.isPublic = true OR r.user = :user ORDER BY r.id")
})
public class Rhythm extends DBObject {
	private static final long serialVersionUID = 1L;

	@Column(name="built_in", nullable=false)
	@GSONSkipSerializationRhythmMap
	private Boolean builtIn;

	@Column(length=255)
	@GSONSkipSerializationRhythmMap
	private String description;

	@Column(name="max_length", nullable=false)
	private Double maxLength;

	@Column(name="min_length", nullable=false)
	private Double minLength;

	public Rhythm() { super(); }

	public Rhythm(String name, User user, String accessCode) {
		super(name, user, "[]", accessCode);
		description = "";
		minLength = 0.1;
		maxLength = 1.0;
		builtIn = false;
	}

	public Rhythm(Rhythm r, User user, String accessCode) {
		super((DBObject) r, user, accessCode);
		this.description = r.description;
		this.minLength = r.minLength;
		this.maxLength = r.maxLength;
		this.builtIn = false;
	}

	public Boolean getBuiltIn() {
		return this.builtIn;
	}

	public void setBuiltIn(Boolean builtIn) {
		this.builtIn = builtIn;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getMaxLength() {
		return Basics.roundDouble(this.maxLength, 3);
	}

	public void setMaxLength(Double maxLength) {
		this.maxLength = maxLength;
	}

	public Double getMinLength() {
		return Basics.roundDouble(this.minLength, 3);
	}

	public void setMinLength(Double minLength) {
		this.minLength = minLength;
	}
}