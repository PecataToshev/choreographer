package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import container.EnumContainer;


@Entity
@Table(name="performances_access")
@NamedQuery(name="PerformancesAccess.getPeopleAccess",
	query="SELECT ap FROM PerformancesAccess ap WHERE ap.performanceBean = :performance AND ap.userBean != :user ORDER BY ap.id"
)
public class PerformancesAccess extends Generic {
	private static final long serialVersionUID = 1L;

	@Column(name="rights", nullable=false, length=2147483647)
	@Enumerated(EnumType.STRING)
	private EnumContainer.DB.PerformanceAccess rights;

	//bi-directional many-to-one association to Performance
	@ManyToOne
	@JoinColumn(name="performance_id", nullable=false)
	private Performance performanceBean;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="user_id", nullable=false)
	private User userBean;

	public PerformancesAccess() { super(); }

	public PerformancesAccess(User u, Performance p, EnumContainer.DB.PerformanceAccess rights) {
		userBean = u;
		performanceBean = p;
		this.rights = rights;
	}

	public EnumContainer.DB.PerformanceAccess getRights() {
		return this.rights;
	}

	public void setRights(EnumContainer.DB.PerformanceAccess rights) {
		this.rights = rights;
	}

	public Performance getPerformanceBean() {
		return this.performanceBean;
	}

	public void setPerformanceBean(Performance performanceBean) {
		this.performanceBean = performanceBean;
	}

	public User getUserBean() {
		return this.userBean;
	}

	public void setUserBean(User userBean) {
		this.userBean = userBean;
	}

}