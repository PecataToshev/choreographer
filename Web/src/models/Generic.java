package models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.GsonBuilder;

import container.GSONSkipSerializationDefault;
import container.GSONSkipSerializationRhythmMap;

@MappedSuperclass
public class Generic  implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(insertable=false, unique=true, nullable=false)
	@GSONSkipSerializationRhythmMap
	private Integer id;

	@Column(name="row_creation_timestamp", insertable=false, nullable=false)
	@GSONSkipSerializationRhythmMap
	private Date rowCreationTimestamp;

	public Generic() {}

	public Integer getId() {
		return id;
	}

	public Date getRowCreationTimestamp() {
		return rowCreationTimestamp;
	}





	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (o instanceof Generic)
			return this.id.equals(((Generic) o).id);
		return false;
	}

	@Override
	public String toString(){
		return new GsonBuilder().addSerializationExclusionStrategy(new ExclusionStrategy() {

			@Override
			public boolean shouldSkipField(FieldAttributes arg0) {
				return (arg0.getAnnotation(GSONSkipSerializationDefault.class) != null);
			}

			@Override
			public boolean shouldSkipClass(Class<?> arg0) {
				return false;
			}
		}).create().toJson(this);
	}
}
