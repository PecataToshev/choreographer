package models;

import java.util.Base64;
import java.util.Random;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import container.GSONSkipSerializationRhythmMap;
import head.Basics;

@MappedSuperclass
public class DBObject extends Generic {
	private static final long serialVersionUID = 1L;

	public static String generateAccessCode() {
		byte[] arr = new byte[24];
		Random r = new Random();
		r.nextBytes(arr);
		return Base64.getUrlEncoder().encodeToString(arr);
	}


	@Column(name="access_code", nullable=false, length=32)
	private String accessCode;

	@Column(length=2147483647)
	private String data;

	@Column(name="is_public", nullable=false)
	@GSONSkipSerializationRhythmMap
	private Boolean isPublic;

	@Column(nullable=false, length=50)
	private String name;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="owner")
	@GSONSkipSerializationRhythmMap
	private User user;

	public DBObject() {}

	public DBObject(String name, User user, String data, String accessCode) {
		super();
		setName(name);
		setUser(user);
		this.data = data;
		this.accessCode = accessCode;
		this.isPublic = false;
	}

	public DBObject(DBObject copyFrom, User user, String accessCode) {
		this(copyFrom.name, user, copyFrom.data, accessCode);
	}

	public String getAccessCode() {
		return this.accessCode;
	}

	public String getData() {
		return this.data;
	}

	public void setData(String data) {
		this.data = Basics.escapeNewLineAndTabs(data);
	}

	public Boolean isPublic() {
		return this.isPublic;
	}

	public void setIsPublic(Boolean isPublic) {
		this.isPublic = (this.isPublic || isPublic); // prevent disabling public
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		String tmp = Basics.escapeHTML(name);
		tmp = tmp.substring(0, (tmp.length() < 50) ? tmp.length() : 50);
		this.name = tmp;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
