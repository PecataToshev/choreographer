package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import container.GSONSkipSerializationDefault;


@Entity
@Table(name="performances")
public class Performance  extends DBObject {
	private static final long serialVersionUID = 1L;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="music")
	private Music music;

	//bi-directional many-to-one association to PerformancesAccess
	@OneToMany(mappedBy="performanceBean", fetch=FetchType.EAGER)
	@GSONSkipSerializationDefault
	private List<PerformancesAccess> performancesAccesses;

	public Performance() { super(); }

	public Performance(String name, User user, String data, String accessCode) {
		super(name, user, data, accessCode);
	}

	public Performance(Performance p, User user, String accessCode) {
		super((DBObject) p, user, accessCode);
		this.music = p.music;
	}

	public Music getMusic() {
		return this.music;
	}

	public void setMusic(Music music) {
		this.music = music;
	}

	public List<PerformancesAccess> getPerformancesAccesses() {
		return this.performancesAccesses;
	}

	public void setPerformancesAccesses(List<PerformancesAccess> performancesAccesses) {
		this.performancesAccesses = performancesAccesses;
	}

	public PerformancesAccess addPerformancesAccess(PerformancesAccess performancesAccess) {
		getPerformancesAccesses().add(performancesAccess);
		performancesAccess.setPerformanceBean(this);

		return performancesAccess;
	}

	public PerformancesAccess removePerformancesAccess(PerformancesAccess performancesAccess) {
		getPerformancesAccesses().remove(performancesAccess);
		performancesAccess.setPerformanceBean(null);

		return performancesAccess;
	}
}