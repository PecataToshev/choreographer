package models;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name="logs")
@NamedQuery(name="Log.findAll", query="SELECT l FROM Log l")
public class Log extends Generic {
	private static final long serialVersionUID = 1L;

	@Column(name="error_message", nullable=false, length=2147483647)
	private String errorMessage;

	@Column(name="stack_trace", nullable=false, length=2147483647)
	private String stackTrace;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="user_id")
	private User user;



	public Log() { super(); }

	public Log(String errorMessage, Throwable e, User u) {
		this.errorMessage = errorMessage;
		stackTrace = throwableToString(e);
		user = u;
	}



	public String getErrorMessage() {
		return this.errorMessage;
	}

	public String getStackTrace() {
		return this.stackTrace;
	}

	public User getUser() {
		return this.user;
	}



	private String throwableToString(Throwable e) {
		if(e == null)
			return "";

		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		return sw.toString();
	}
}