package models;

import java.security.MessageDigest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import container.GSONSkipSerializationDefault;
import head.Basics;


@Entity
@Table(name="users")
@NamedQueries({
	@NamedQuery(name="User.findAll", query="SELECT u FROM User u"),
	@NamedQuery(name="User.doesMailExist", query="SELECT COUNT(u) FROM User u WHERE u.email = :email"),
	@NamedQuery(name="User.login", query="SELECT u FROM User u WHERE u.email = :email AND u.password = :password")
})
public class User extends Generic {
	private static final long serialVersionUID = 1L;

	@Column(nullable=false, length=255)
	private String email;

	@Column(name="first_name", nullable=false, length=35)
	private String firstName;

	@Column(nullable=false)
	private Boolean gender;

	@Column(name="last_login")
	private Date lastLogin;

	@Column(name="last_name", nullable=false, length=35)
	private String lastName;

	@Column(nullable=false, length=64)
	private String password;

	//bi-directional many-to-one association to Log
	@OneToMany(mappedBy="user", fetch=FetchType.EAGER)
	@GSONSkipSerializationDefault
	private List<Log> logs;

	//bi-directional many-to-one association to Music
	@OneToMany(mappedBy="user")
	@GSONSkipSerializationDefault
	private List<Music> musics;

	//bi-directional many-to-one association to Performance
	@OneToMany(mappedBy="user", fetch=FetchType.EAGER)
	@GSONSkipSerializationDefault
	private List<Performance> performances;

	//bi-directional many-to-one association to PerformancesAccess
	@OneToMany(mappedBy="userBean", fetch=FetchType.EAGER)
	@GSONSkipSerializationDefault
	private List<PerformancesAccess> performancesAccesses;

	//bi-directional many-to-one association to Performance
	@OneToMany(mappedBy="user", fetch=FetchType.EAGER)
	@GSONSkipSerializationDefault
	private List<Rhythm> rhythms;










	public User() { super(); }

	public User(String email, String firstName, String lastName, Boolean gender, String password) {
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.password = hashPass(password, email);
		updateLastLogin();
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Boolean getGender() {
		return this.gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	public Date getLastLogin() {
		return this.lastLogin;
	}

	public void updateLastLogin() {
		this.lastLogin = new Date();
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = hashPass(password, email);
	}













	public List<Log> getLogs() {
		return this.logs;
	}

	public void addLog(Log log) {
		getLogs().add(log);
	}





	public List<Music> getMusics() {
		return this.musics;
	}

	public Music addMusic(Music music) {
		getMusics().add(music);
		music.setUser(this);

		return music;
	}

	public Music removeMusic(Music music) {
		getMusics().remove(music);
		music.setUser(null);

		return music;
	}





	public List<Performance> getPerformances() {
		return this.performances;
	}

	public Performance addPerformance(Performance performance) {
		performances.add(performance);
		performance.setUser(this);

		return performance;
	}

	public Performance removePerformance(Performance performance) {
		performances.remove(performance);
		performance.setUser(null);

		return performance;
	}





	public List<PerformancesAccess> getPerformancesAccesses() {
		return this.performancesAccesses;
	}

	public Map<Performance, PerformancesAccess> getPerformancesWithAccess(){
		Map<Performance, PerformancesAccess> res = new HashMap<Performance, PerformancesAccess>();
		if(performancesAccesses != null) {
			PerformancesAccess[] performancesAccessesSorted = Basics.sortListForPrinting(PerformancesAccess.class, "getId", performancesAccesses);
			for(PerformancesAccess pa : performancesAccessesSorted)
				res.put(pa.getPerformanceBean(), pa);
		}

		return res;
	}

	public PerformancesAccess addPerformancesAccess(PerformancesAccess performancesAccess) {
		performancesAccesses.add(performancesAccess);
		performancesAccess.setUserBean(this);

		return performancesAccess;
	}

	public PerformancesAccess removePerformancesAccess(PerformancesAccess performancesAccess) {
		performancesAccesses.remove(performancesAccess);
		return performancesAccess;
	}






	public List<Rhythm> getRhythms() {
		return this.rhythms;
	}

	public Rhythm addRhythm(Rhythm rhythm) {
		rhythms.add(rhythm);
		rhythm.setUser(this);
		return rhythm;
	}

	public Rhythm removeRhythm(Rhythm rhythm) {
		rhythms.remove(rhythm);
		return rhythm;
	}










	public static String hashPass(String password, String email) {
		try{
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest((password + email).getBytes("UTF-8"));
			StringBuffer hexString = new StringBuffer();

			for (int i = 0; i < hash.length; i++) {

				String hex = Integer.toHexString(0xff & hash[i]);
				if(hex.length() == 1) hexString.append('0');
				hexString.append(hex);

			}

			return hexString.toString();

		} catch(Exception e){

			throw new RuntimeException(e);

		}
	}
}