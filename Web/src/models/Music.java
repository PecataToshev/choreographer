package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table(name="music")
@NamedQueries({
	@NamedQuery(name="Music.findAll", query="SELECT m FROM Music m"),
	@NamedQuery(name="Music.findByID", query="SELECT m FROM Music m WHERE m.id = :id"),
	@NamedQuery(name="Music.findPublic", query="SELECT m FROM Music m WHERE m.isPublic = true"),
	@NamedQuery(name="Music.findAccessible", query="SELECT m FROM Music m WHERE m.isPublic = true")
})
public class Music extends DBObject {
	private static final long serialVersionUID = 1L;

	@Column(name="generated_data", length=2147483647)
	private String generatedData;

	@Column(length=50)
	private String path;

	public Music() { super(); }

	public Music(String name, User user, String accessCode) {
		super(name, user, "[]", accessCode);
	}

	public Music(Music m, User user, String accessCode) {
		super((DBObject) m, user, accessCode);
		this.generatedData = m.generatedData;
		this.path = m.path;
	}

	public String getPath() {
		return this.path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getGeneratedData() {
		return generatedData;
	}

	public void setGeneratedData(String generatedData) {
		this.generatedData = generatedData;
	}
}