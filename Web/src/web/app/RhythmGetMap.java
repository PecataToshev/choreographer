package web.app;

import java.io.PrintWriter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.GsonBuilder;

import access.AccessRhythm;
import container.GSONSkipSerializationDefault;
import container.GSONSkipSerializationRhythmMap;
import head.Config;
import head.SessionHandler;
import web.ServletTemplate;

@WebServlet(Config.servletPrefixApp + "RhythmGetMap")
public class RhythmGetMap extends ServletTemplate {
	private static final long serialVersionUID = 1L;

	@Override
	protected void processDoGet(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			PrintWriter writer) {

		AccessRhythm ar = new AccessRhythm();
		writer.write("OK:"
			+ (
				new GsonBuilder().addSerializationExclusionStrategy(new ExclusionStrategy() {
					@Override
					public boolean shouldSkipField(FieldAttributes arg0) {
						return (
								(arg0.getAnnotation(GSONSkipSerializationDefault.class) != null)
								|| (arg0.getAnnotation(GSONSkipSerializationRhythmMap.class) != null)
								);
					}

					@Override
					public boolean shouldSkipClass(Class<?> arg0) {
						return false;
					}
				}).create().toJson(ar.getAccessible(SessionHandler.getUser(request)))
			)
		);
		ar.closeEntityManager();
	}

}
