package web.app;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import access.Access;
import access.AccessDBObject;
import access.AccessMusic;
import access.AccessPerformance;
import access.AccessRhythm;
import container.EnumContainer.Parameters;
import container.EnumContainer.Result;
import head.Basics;
import head.Config;
import head.SessionHandler;
import models.User;
import web.ServletTemplate;

@MultipartConfig
@WebServlet({
	Config.servletPrefixApp + "ObjectCopy",
	Config.servletPrefixApp + "ObjectCreate",
	Config.servletPrefixApp + "ObjectDataGet",
	Config.servletPrefixApp + "ObjectUpdate"
})
public class ObjectSelector extends ServletTemplate {
	private static final long serialVersionUID = 1L;

	private String partInputStreamToString(Part p) {
		if(p == null)
			return null;
		try {
			InputStream inputStream = p.getInputStream();
			ByteArrayOutputStream result = new ByteArrayOutputStream();
			byte[] buffer = new byte[1024];
			int length;
			while ((length = inputStream.read(buffer)) != -1) {
				result.write(buffer, 0, length);
			}
			// StandardCharsets.UTF_8.name() > JDK 7
			return result.toString("UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	protected <T> void handle(HttpServletRequest request, HttpServletResponse response, HttpSession session, PrintWriter writer) {
		T access = null;
		ArrayList<T> dataToSend = new ArrayList<>();
		User user = SessionHandler.getUser(session);
		dataToSend.add((T) user);

		boolean isObjectCopy    = request.getServletPath().endsWith("ObjectCopy");
		boolean isObjectCreate  = request.getServletPath().endsWith("ObjectCreate");
		boolean isObjectUpdate  = request.getServletPath().endsWith("ObjectUpdate");
		boolean isObjectDataGet = request.getServletPath().endsWith("ObjectDataGet");
		Parameters.ObjectData param = null;

		if(isObjectCreate)
			param = Parameters.ObjectData.NAME;
		else
			param = Parameters.ObjectData.CODE;

		String identifier = request.getParameter(param.toString());
		if(identifier == null || identifier.isEmpty() || identifier.equals("")) {
			printError(response, writer);
			return;
		}
		dataToSend.add((T) identifier);

		Parameters.Object requested = Parameters.Object.valueOf(
				request.getParameter(Parameters.Action.ACTION.toString())
			);

		if(isObjectUpdate) {
			String name = Parameters.ObjectData.NAME.toString();
			String data = Parameters.ObjectData.DATA.toString();
			String publ = Parameters.ObjectData.ISPUBLIC.toString();
			if(requested == Parameters.Object.MUSIC) {
				try {
					name = partInputStreamToString(request.getPart(name));
					data = partInputStreamToString(request.getPart(data));
					publ = partInputStreamToString(request.getPart(publ));
				} catch (Exception e) {
					printError(response, writer);
					return;
				}
			} else {
				name = request.getParameter(name);
				data = request.getParameter(data);
				publ = request.getParameter(publ);
			}

			dataToSend.add((T) name);
			if(requested != Parameters.Object.PERFORMANCE_SETTINGS) {
				dataToSend.add(1, null);
				dataToSend.add((T) data);
			}
			dataToSend.add((T) Boolean.valueOf("on".equals(publ)));
		}

		if(requested == Parameters.Object.PERFORMANCE || requested == Parameters.Object.PERFORMANCE_SETTINGS) {
			access = (T) new AccessPerformance();
			if(isObjectCreate) {
				dataToSend.add((T) Basics.getStaticResource(getServletContext(), "PerformanceDataDefault.json"));
			} else if(requested == Parameters.Object.PERFORMANCE_SETTINGS) {
				dataToSend.add((T) request.getParameter(Parameters.PerformanceSettings.MUSIC.toString()));
			}
		} else if(requested == Parameters.Object.MUSIC) {
			access = (T) new AccessMusic();
			if(isObjectUpdate) {
				/**
				 * 6 - FILE
				 */
				Part file = null;
				try {
					file = request.getPart(Parameters.Music.FILE.toString());
				} catch (Exception e) {}
				dataToSend.add((T) file);
			}
		} else if(requested == Parameters.Object.RHYTHM) {
			access = (T) new AccessRhythm();
			if(isObjectUpdate) {
				/**
				 * 6 - DESCRIPTION
				 * 7 - MINLENGTH
				 * 8 - MAXLENGTH
				 */
				dataToSend.add((T) request.getParameter(Parameters.Rhythm.DESCRIPTION.toString()));
				dataToSend.add((T) Double.valueOf(request.getParameter(Parameters.Rhythm.MINLENGTH.toString())));
				dataToSend.add((T) Double.valueOf(request.getParameter(Parameters.Rhythm.MAXLENGTH.toString())));
			}
		} else {
			printError(response, writer);
			return;
		}

		if(isObjectDataGet) {
			/**
			 * dataToSend:
			 * 0 - user
			 * 1 - code
			 */
			writer.write(((AccessDBObject) access).getObjectData(dataToSend));
		} else if(isObjectUpdate && requested == Parameters.Object.PERFORMANCE_SETTINGS) {
			/**
			 * dataToSend:
			 * 0 - user
			 * 1 - code
			 * 2 - name
			 * 3 - isPublic
			 * 4 - music
			 * 5 - performersAccess: [
			 *  	{
			 *  	performerID,
			 *  	access
			 *  	}
			 * 	]
			 * n - object specific data
			 */
			writer.write(((AccessPerformance) access).updateSettings(dataToSend).toString());
		} else if(isObjectUpdate) {
			/**
			 * dataToSend:
			 * 0 - user
			 * 1 - object selected by code (FOR NOW IS NULL)
			 * 2 - code
			 * 3 - name
			 * 4 - data
			 * 5 - isPublic
			 * n - object specific data
			 */
			writer.write(((AccessDBObject) access).update(dataToSend).toString());
		} else if(isObjectCreate) {
			/**
			 * dataToSend:
			 * 0 - user
			 * 1 - name
			 */
			writer.write(((AccessDBObject) access).createNew(dataToSend.toArray()));
		} else { // copy
			writer.write(((AccessDBObject) access).copyObject(identifier, user));
		}

		((Access) access).closeEntityManager();
		if(isObjectCopy || isObjectCreate || isObjectUpdate) // update user references in session
			SessionHandler.updateUserInSession(user, session);
	}

	protected void printError(HttpServletResponse response, PrintWriter writer) {
		writer.write(Result.EMPTY_REQUEST.toString());
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	}

	@Override
	protected void processDoGet(HttpServletRequest request, HttpServletResponse response, HttpSession session, PrintWriter writer) {
		handle(request, response, session, writer);
	}

	@Override
	protected <T> void processDoPost(HttpServletRequest request, HttpServletResponse response, HttpSession session, PrintWriter writer) {
		handle(request, response, session, writer);
	}
}
