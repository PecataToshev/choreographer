package web.app;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import access.AccessLog;
import access.AccessMusic;
import container.EnumContainer.Parameters;
import head.Config;
import models.Music;

@WebServlet(Config.servletPrefixApp + "MusicGetAudio")
public class MusicGetAudio extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//should check access
		String code = request.getParameter(Parameters.ObjectData.CODE.toString());
		if(code == null || code.isEmpty() || code.equals("")) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return;
		}

		AccessMusic am = new AccessMusic();
		String fileCustomPath = ((Music) am.getByAccessCode(code)).getPath();
		ServletOutputStream stream = null;
		BufferedInputStream buf = null;
		try {
			stream = response.getOutputStream();
			File mp3 = new File(Config.getAudioDataPath() + fileCustomPath);

			//set response headers
			response.setContentType("audio/mpeg"); 

			response.addHeader("Content-Disposition", "attachment; filename=" + fileCustomPath);

			response.setContentLength((int) mp3.length());

			FileInputStream input = new FileInputStream(mp3);
			buf = new BufferedInputStream(input);
			int readBytes = 0;

			//read from the file; write to the ServletOutputStream
			while ((readBytes = buf.read()) != -1)
				stream.write(readBytes);

		} catch (IOException ioe) {
			AccessLog.addLog(fileCustomPath, ioe, head.SessionHandler.getUser(request));
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		} finally {
			try {

				if (stream != null)
					stream.close();

				if (buf != null)
					buf.close();
			} catch (IOException e) {
				AccessLog.addLog(fileCustomPath, e, head.SessionHandler.getUser(request));
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			}
		}


		am.closeEntityManager();
	}
}
