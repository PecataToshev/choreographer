package web.sign;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import access.AccessUser;
import container.EnumContainer.Parameters.Sign;
import head.Config;
import head.SessionHandler;

@WebServlet(Config.servletPrefixSign + "Up")
public class SignUp extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public SignUp() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/**
		 *  1 successful registration
		 *  0 email exists in DB
		 * -1 password don't match
		 * -2 password too short
		 */

		HttpSession session = request.getSession();
		PrintWriter writer = response.getWriter(); 

		if(request.getParameter(Sign.PASSWORD.toString()).equals(request.getParameter(Sign.REPASSWORD.toString())) == false) {
			writer.write("-1");
		} else if(request.getParameter(Sign.PASSWORD.toString()).length() < 8) {
			writer.write("-2");
		} else {
			AccessUser au = new AccessUser();

			writer.write(
					SessionHandler.logInUser(
						au.addUser(
							getServletContext(),
							request.getParameter(Sign.EMAIL.toString()),
							request.getParameter(Sign.FIRST_NAME.toString()),
							request.getParameter(Sign.LAST_NAME.toString()),
							request.getParameter(Sign.GENDER.toString()) == "1",
							request.getParameter(Sign.PASSWORD.toString())
						),
						session
					)
				);

			au.closeEntityManager();
		}

		writer.close();
	}

}
