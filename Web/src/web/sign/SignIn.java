package web.sign;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import access.AccessUser;
import container.EnumContainer.Parameters.Sign;
import head.Config;
import head.SessionHandler;

@WebServlet(Config.servletPrefixSign + "In")
public class SignIn extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public SignIn() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/**
		 * 0 failed (not in db or wrong pass or email)
		 * 1 success login
		 */

		HttpSession session = request.getSession();
		PrintWriter writer = response.getWriter();

		AccessUser au = new AccessUser();

		writer.write(
				SessionHandler.logInUser(
					au.login(
						request.getParameter(Sign.EMAIL.toString()),
						request.getParameter(Sign.PASSWORD.toString())
					),
					session
				)
			);

		au.closeEntityManager();
		writer.close();
	}

}
