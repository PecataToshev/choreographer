package web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ServletTemplate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public ServletTemplate() { super(); }

	protected final void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		simpler(false, request, response);
	}


	protected final void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		simpler(true, request, response);
	}

	private void simpler(boolean isDoPost, HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession();
		PrintWriter writer = response.getWriter();

		response.setHeader("content-type", "text/plain; charset=utf-8");

		if(isDoPost)
			processDoPost(request, response, session, writer);
		else
			processDoGet(request, response, session, writer);

		writer.close();
	}

	protected <T> void processDoPost(HttpServletRequest request, HttpServletResponse response, HttpSession session, PrintWriter writer) {
		writeError(writer);
	}
	protected void processDoGet(HttpServletRequest request, HttpServletResponse response, HttpSession session, PrintWriter writer) {
		writeError(writer);
	}

	private final void writeError(PrintWriter writer) {
		writer.write("FORBIDDEN");
	}
}
