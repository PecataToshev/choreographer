package web;

import java.io.PrintWriter;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import container.EnumContainer.Parameters;
import head.MailSender;


@WebServlet("/about/ContactForm")
public class ContactForm extends ServletTemplate {
	private static final long serialVersionUID = 1L;

	@Override
	protected void processDoPost(HttpServletRequest request, HttpServletResponse response, HttpSession session,
			PrintWriter writer) {

		MailSender ms = new MailSender();
		if(ms.sendContactFormEmail(
				request.getParameter(Parameters.ContactForm.EMAIL.toString()),
				request.getParameter(Parameters.ContactForm.NAME.toString()),
				request.getParameter(Parameters.ContactForm.TITLE.toString()),
				request.getParameter(Parameters.ContactForm.CONTENT.toString()).trim().replaceAll("\n", "<br />")
			)
		) 
			writer.write("OK");
		else {
			writer.write("NOT-OK");
		}
	}

}
