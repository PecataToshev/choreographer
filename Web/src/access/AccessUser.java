package access;

import javax.persistence.NoResultException;
import javax.servlet.ServletContext;

import head.MailSender;
import models.User;

public class AccessUser extends Access {
	public AccessUser() {
		super(models.User.class);
	}

	public User addUser(ServletContext servletContext, String email, String firstName, String lastName,
			Boolean gender, String password) {
		/**
		 * null Email registered
		 * <User> User added
		 */

		if((long) entityManager.createNamedQuery(targetTable + ".doesMailExist").setParameter("email", email).getSingleResult() > 0) {
			return null;
		}

		User u = new User(email, firstName, lastName, gender, password);
		addToDB(u);
		MailSender ms = new MailSender(servletContext);
		ms.sendWelcomeMail(u);
		return login(email, password);
	}

	public User login(String email, String password) {
		password = User.hashPass(password, email);
		User user = null;

		try {
			user = (User) entityManager.createNamedQuery(targetTable + ".login").setParameter("email", email).setParameter("password", password).getSingleResult();
		} catch(NoResultException e) {}

		if(user != null) {
			user.updateLastLogin();
			updateInDB(user);
		}

		return user;
	}
}
