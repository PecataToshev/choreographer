package access;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;

import head.Credentials;

public class Access {
	private final String PERSISTENCE_UNIT_NAME = "Choreographer";
	private EntityManagerFactory entityManagerFactory = null;
	protected EntityManager entityManager = null;
	protected Class<?> tableModel;
	protected String targetTable;



	public Access(Class<?> tableModel) {
		this.tableModel = tableModel;
		targetTable = tableModel.getSimpleName();

		Map<String, String> properties = new HashMap<String, String>();
		properties.put("javax.persistence.jdbc.password", Credentials.dbPass);

		entityManagerFactory = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME, properties);
		entityManager = entityManagerFactory.createEntityManager();
	}

	public void closeEntityManager() {
		entityManager.close();
		entityManagerFactory.close();
	}



	public List<?> getAll(){
		/**
		 * Get all elements from model in DB
		 */

		try {
			return entityManager.createQuery("SELECT o FROM " + targetTable + " o ORDER BY o.id").getResultList();
		} catch(NoResultException e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public <T> T getByID(Integer ID) {
		/**
		 * Get element from model by ID
		 */

		try {
			return (T) entityManager.createQuery("SELECT o FROM " + targetTable + " o WHERE o.id = :id ORDER BY o.id")
					.setParameter("id", ID).getSingleResult();
		} catch(NoResultException e) {}

		return null;
	}




	public <T> boolean addToDB(T data) {
		/**
		 * Add new element in DB.
		 * If insert is successful return true
		 * else -> false
		 */
		if(data == null)
			return false;

		try {
			entityManager.getTransaction().begin();
			entityManager.persist(tableModel.cast(data));
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			AccessLog.addLog("Eror when adding new element to db\ntablename = " + targetTable
					+ "\ndata = " + data.toString(), e, null);
			return false;
		}

		return true;
	}

	public <T> boolean updateInDB(T data) {
		/**
		 * Update element in DB.
		 * If update is successful return true
		 * else -> false
		 */
		if(data == null)
			return false;

		try {
			entityManager.getTransaction().begin();
			entityManager.merge(tableModel.cast(data));
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			AccessLog.addLog("Eror when updating element to db\ntablename = " + targetTable
					+ "\ndata = " + data.toString(), e, null);
			return false;
		}

		return true;
	}
}
