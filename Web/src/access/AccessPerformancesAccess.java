package access;

import java.util.List;

import javax.persistence.NoResultException;

import container.EnumContainer.DB;
import models.Performance;
import models.PerformancesAccess;
import models.User;

public class AccessPerformancesAccess extends Access {
	public AccessPerformancesAccess() {
		super(models.PerformancesAccess.class);
	}

	public boolean addPerformanceAccess(User u, Performance p, DB.PerformanceAccess rights) {
		return addToDB(new PerformancesAccess(u, p, rights));
	}

	@SuppressWarnings("unchecked")
	public List<PerformancesAccess> getAllPeopleWithRights(Performance p) {
		try {
			return entityManager.createNamedQuery(targetTable + ".getPeopleAccess")
					.setParameter("performance", p).setParameter("user", p.getUser()).getResultList();
		} catch(NoResultException e) {}
		return null;
	}

}