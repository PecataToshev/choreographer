package access;

import java.io.File;
import java.util.ArrayList;

import container.Mutable;
import container.EnumContainer.DB.PerformanceAccess;
import container.EnumContainer.Result;
import head.Basics;
import models.Music;
import models.Performance;
import models.PerformancesAccess;
import models.User;

public class AccessPerformance extends AccessDBObject {
	public AccessPerformance() {
		super(models.Performance.class);
	}


	@Override
	@SuppressWarnings("unchecked")
	protected <T> T createNewObject(T[] args, String accessCode) {
		/**
		 * args[0] - User owner
		 * args[1] - String name
		 * args[2] - String defaultDataPath
		 */

		Performance p = new Performance((String) args[1], (User) args[0],
				Basics.readDefaultDataFromFile((File) args[2]), accessCode);
		((User) args[0]).addPerformance(p);
		return (T) p;
	}

	@Override
	protected <T> boolean updateGetDataPrefixAndCheckForShowRights(T obj, User user, Mutable.String prefix) {
		Performance p = (Performance) obj;
		if(getPerformanceRights(p, user) == null)
			return false;

		prefix.Value += ((p.getMusic() != null) ? p.getMusic().getAccessCode() : "0") + ",";
		return true;
	}

	@Override
	protected <T> boolean copyObjectCheckHasRights(T obj, User user) {
		return getPerformanceRights((Performance) obj, user) != null;
	}





	public PerformanceAccess getPerformanceRights(Performance p, User user) {
		if(p != null) {
			if(p.isPublic())
				return PerformanceAccess.READ;

			if(p.getUser().equals(user))
				return PerformanceAccess.EDIT;

			if(user.getPerformancesAccesses().size() > 0)
				for(PerformancesAccess pa : user.getPerformancesAccesses())
					if(pa.getPerformanceBean().equals(p))
						return pa.getRights();
		}

		return null;
	}

	public PerformanceAccess getPerformanceRights(String code, User user) {
		return getPerformanceRights((Performance) getByAccessCode(code), user);
	}





	public <T> Result updateSettings(ArrayList<T> dataToSend) {
		System.out.println(dataToSend);
		User user = (User) dataToSend.get(0);
		AccessPerformance ap = new AccessPerformance();
		Performance p = ap.getByAccessCode((String) dataToSend.get(1));
		ap.closeEntityManager();
		if(p ==  null)
			return Result.WRONG_CODE;
		if(p.isPublic())
			return Result.PUBLIC;
		if(getPerformanceRights(p, user) != PerformanceAccess.EDIT)
			return Result.NOT_ALLOWED;

		AccessMusic am = new AccessMusic();
		Music newMusic = am.getByAccessCode((String) dataToSend.get(4));
		am.closeEntityManager();
		if(newMusic != null)
			p.setMusic(newMusic);

		p.setName((String) dataToSend.get(2));
		p.setIsPublic((Boolean) dataToSend.get(3));
		if(updateInDB(p))
			return Result.OK;
		return Result.ERROR;
	}

	@Override
	protected <T> boolean updateObjectCheckForRights(T obj, User user) {
		if(getPerformanceRights((Performance)obj, user) != PerformanceAccess.EDIT)
			return false;
		return true;
	}

	protected <T> T updateObject(ArrayList<T> dataToSend) {
		return dataToSend.get(1);
	}

}
