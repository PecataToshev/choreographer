package access;

import java.util.ArrayList;
import java.util.List;

import container.Mutable;
import models.Rhythm;
import models.User;

public class AccessRhythm extends AccessDBObject {

	public AccessRhythm() {
		super(models.Rhythm.class);
	}


	@SuppressWarnings("unchecked")
	public List<Rhythm> getBuiltRhythms(){
		return entityManager.createNamedQuery(targetTable + ".findBuiltIn").getResultList();
	}





	@Override
	@SuppressWarnings("unchecked")
	protected <T> T createNewObject(T[] args, String accessCode) {
		/**
		 * args[0] - User user
		 * args[1] - String name
		 */

		Rhythm r = new Rhythm((String) args[1], (User)args[0], accessCode);
		((User) args[0]).addRhythm(r);
		return (T) r;
	}





	@Override
	@SuppressWarnings("unchecked")
	public List<Rhythm> getAccessible(User user) {
		return entityManager.createNamedQuery(targetTable + ".findAccessible").setParameter("user", user).getResultList();
	}





	@Override
	protected <T> boolean copyObjectCheckHasRights(T obj, User user) {
		return ((Rhythm) obj).getBuiltIn();
	}

	@Override
	protected <T> boolean updateGetDataPrefixAndCheckForShowRights(T obj, User user, Mutable.String prefix) {
		return ((Rhythm) obj).getBuiltIn();
	}

	@SuppressWarnings("unchecked")
	@Override
	protected <T> T updateObject(ArrayList<T> dataToSend) {
		Rhythm r = (Rhythm) dataToSend.get(1);
		r.setDescription((String) dataToSend.get(6));
		r.setMinLength((Double) dataToSend.get(7));
		r.setMaxLength((Double) dataToSend.get(8));
		return (T) r;
	}
}
