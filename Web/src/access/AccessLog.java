package access;

import models.Log;
import models.User;

public class AccessLog extends Access {

	public AccessLog() {
		super(models.Log.class);
	}

	public static void addLog(final String info, Throwable e, User u) {
		AccessLog al = new AccessLog();
		al.writeLog(info, e, u);
		al.closeEntityManager();

		e.printStackTrace();
	}

	public void writeLog(final String info, Throwable e, User u) {
		addToDB(new Log(info, e, u));
	}
}
