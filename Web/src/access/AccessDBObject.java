package access;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.NoResultException;

import container.EnumContainer.Result;
import container.Mutable;
import models.DBObject;
import models.User;

public abstract class AccessDBObject extends Access {

	public AccessDBObject(Class<?> tableModel) {
		super(tableModel);
	}

	private String findNotUsedAccessCode() {
		String code = null;
		do {
			code = DBObject.generateAccessCode();
		} while(
				(
					(Long) entityManager.createQuery(
						"SELECT COUNT(o) FROM " + targetTable + " o WHERE o.accessCode = :code"
					).setParameter("code", code).getSingleResult()
				) != 0
			);
		return code;
	}

	protected abstract <T> T createNewObject(T[] args, String accessCode);

	public <T> String createNew(T[] args) {
		/**
		 * Create new DBObject and insert it in DB
		 * parameters - Array of elements for creating new object
		 * If insert is successful return OK: with the accessCode of the object
		 * Else return ERROR
		 * 
		 */

		try {
			T newObject = createNewObject(args, findNotUsedAccessCode());
			if(addToDB(newObject))
				return "OK:" + ((DBObject) newObject).getAccessCode();
		} catch (Exception e) {
			AccessLog.addLog("Eror when adding new element\ntablename = " + targetTable
					+ "\nargs = " + Arrays.deepToString(args), e, null);
		}

		return Result.ERROR.toString();
	}

	@SuppressWarnings("unchecked")
	public <T> T getByAccessCode(String code) {
		if(code == null || code.isEmpty() || code.equals(""))
			return null;

		try {
			return (T) entityManager.createQuery("SELECT o FROM " + targetTable + " o WHERE o.accessCode = :code")
					.setParameter("code", code).getSingleResult();
		} catch(NoResultException e) {}

		return null;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getPublic() {
		try {
			return entityManager.createQuery("SELECT o FROM " + targetTable + " o WHERE o.isPublic = true ORDER BY o.id")
					.getResultList();
		} catch(NoResultException e) {}

		return null;
	}

	protected <T> boolean updateGetDataPrefixAndCheckForShowRights(T obj, User user, Mutable.String prefix) {
		return false;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getAccessible(User user) {
		return entityManager.createQuery("SELECT o FROM " + targetTable
					+ " o WHERE o.isPublic = true OR o.user = :user ORDER BY o.id")
				.setParameter("user", user).getResultList();
	}

	public <T> String getObjectData(ArrayList<T> dataToSend) {
		T obj = getByAccessCode((String) dataToSend.get(1));
		User user = (User) dataToSend.get(0);
		Mutable.String resultPrefix = new Mutable.String();
		resultPrefix.Value = "OK:";
		boolean show = updateGetDataPrefixAndCheckForShowRights(obj, user, resultPrefix)
				|| ((DBObject) obj).isPublic()
				|| ((DBObject) obj).getUser().equals((user));
		if(show)
			return resultPrefix.Value + ((DBObject) obj).getData();
		return Result.NOT_ALLOWED.toString();
	}

	protected <T> boolean updateObjectCheckForRights(T obj, User user) {
		return false;
	}

	protected abstract <T> T updateObject(ArrayList<T> dataToSend);

	public <T> Result update(ArrayList<T> dataToSend) {
		T obj = getByAccessCode((String) dataToSend.get(2));
		if(obj == null)
			return Result.WRONG_CODE;
		dataToSend.set(1, obj);
		if(((DBObject) obj).isPublic())
			return Result.PUBLIC;
		User user = (User) dataToSend.get(0);

		boolean canUpdate = (((DBObject) obj).getUser().equals((user))) || updateObjectCheckForRights(obj, user);
		if(!canUpdate)
			return Result.NOT_ALLOWED;

		String tmp = (String) dataToSend.get(3);
		if(tmp != null && !tmp.isEmpty() && !tmp.equals(""))
			((DBObject) obj).setName(tmp);
		tmp = (String) dataToSend.get(4);
		if(tmp != null && !tmp.isEmpty() && !tmp.equals(""))
			((DBObject) obj).setData(tmp);
		((DBObject) obj).setIsPublic(((Boolean) dataToSend.get(5)));

		if(updateInDB(updateObject(dataToSend)))
			return Result.OK;

		return Result.ERROR;
	}

	protected <T> boolean copyObjectCheckHasRights(T obj, User user) {
		return true;
	}

	@SuppressWarnings("unchecked")
	public <T> String copyObject(String code, User user) {
		T obj = getByAccessCode(code);
		if(obj == null)
			return Result.WRONG_CODE.toString();
		if((!((DBObject) obj).isPublic()) && !((DBObject) obj).getUser().equals(user) && !copyObjectCheckHasRights(obj, user))
			return Result.NOT_ALLOWED.toString();
		String newObjectCode = findNotUsedAccessCode(); 
		try {
			T newObject = (T) tableModel.getConstructor(new Class[] {tableModel, User.class, String.class}).newInstance(new Object[] {tableModel.cast(obj), user, newObjectCode});
			if(addToDB(newObject))
				return "OK:" + newObjectCode;
		} catch (Exception e) {
			AccessLog.addLog("Error when copying object:" + targetTable + ": " + code + "\nNew code: " + newObjectCode, e, user);
		}
		return Result.ERROR.toString();
	}
}
