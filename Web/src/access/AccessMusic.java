package access;

import java.util.ArrayList;

import javax.servlet.http.Part;

import head.MusicFileOrganizer;
import models.Music;
import models.User;

public class AccessMusic extends AccessDBObject {

	public AccessMusic() {
		super(Music.class);
	}

	@SuppressWarnings("unchecked")
	protected <T> T createNewObject(T[] args, String accessCode) {
		/**
		 * args[0] - User owner
		 * args[1] - String name
		 */

		Music music = new Music((String) args[1], (User) args[0], accessCode);
		((User) args[0]).addMusic(music);
		return (T) music;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected <T> T updateObject(ArrayList<T> dataToSend) {
		Music music = (Music) dataToSend.get(1);
		Part file = (Part) dataToSend.get(6);
		boolean isFileUploaded = (file != null && file.getName() != null && !file.getName().isEmpty() && file.getSize() != 0);
		if(!isFileUploaded)
			return (T) music;

		MusicFileOrganizer mfo = new MusicFileOrganizer();
		String filePath = mfo.upload("a_", String.format("%07d", music.getId()), ".mp3", file);
		if(filePath == null) {
			return null;
		}
		music.setPath(filePath);
		return (T) music;
	}
}
