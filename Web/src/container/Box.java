package container;

public class Box {
	public static class Double {
		public double l;
		public double r;

		public Double(double l, double r) {
			super();
			this.l = l;
			this.r = r;
		}
	}

	public static class Integer {
		public int l;
		public int r;

		public Integer(int l, int r) {
			super();
			this.l = l;
			this.r = r;
		}
	}
}
