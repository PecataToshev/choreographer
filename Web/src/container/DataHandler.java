package container;

import audio.MusicRecognizerData;
import audio.Recognizer;

public class DataHandler {
	private static Recognizer musicRecognizer = null;

	public void createMusicRecognizer () {
		musicRecognizer = new Recognizer();
		musicRecognizer.start();
	}

	public static void addDataToRegognizer(MusicRecognizerData data) {
		musicRecognizer.addDataToRecognize(data);
	}
}
