package container;

public class EnumContainer {
	public static enum Session {
		LOGGED,
		USER
	}

	public static class DB {
		public static enum PerformanceAccess {
			EDIT,
			READ
		}
	}

	public static class Parameters {
		public static enum ContactForm {
			EMAIL,
			NAME,
			TITLE,
			CONTENT
		}

		public static enum Action {
			ACTION
		}

		public static enum Object {
			MUSIC,
			PERFORMANCE,
			PERFORMANCE_SETTINGS,
			RHYTHM
		}

		public static enum ObjectData {
			ID,
			CODE,
			NAME,
			DATA,
			ISPUBLIC
		}

		public static enum Music {
			FILE
		}

		public static enum PerformanceSettings {
			MUSIC
		}

		public static enum Rhythm {
			DESCRIPTION,
			MINLENGTH,
			MAXLENGTH
		}

		public static enum Sign {
			EMAIL,
			PASSWORD,
			FIRST_NAME,
			LAST_NAME,
			REPASSWORD,
			GENDER
		}
	}

	public static enum Result {
		EMPTY_REQUEST,
		ERROR,
		OK,
		NOT_ALLOWED,
		PUBLIC,
		WRONG_CODE
	}
}
