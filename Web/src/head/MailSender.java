package head;

import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;

import access.AccessLog;
import models.User;

public class MailSender {
	private Session session;
	private ServletContext servletContext;


	public MailSender() {
		super();
		servletContext = null;
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.host", "shrek.icnhost.net");

		// SSL/TLS
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.socketFactory.fallback", "false");
		props.put("mail.smtp.socketFactory.port", "465");

		session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(Credentials.mailUser, Credentials.mailPass);
					}
				}
			);
	}

	public MailSender(ServletContext servletContext) {
		this();
		this.servletContext = servletContext;
	}




	public boolean sendContactFormEmail(final String senderEmail, final String senderName, final String title, String content) {
		content += "<br /><br /> -- <br />Sent via contact form on " + (new Date()).toString();
		return sendMail(senderEmail, senderName, title, content, new String[] {"support@mail.choreographer.tk"});
	}

	private String createWelcomeMailText(Object[] data) {
		return String.format(
				Basics.readDefaultDataFromFile(
						Basics.getStaticResource(servletContext, "email.html")
					).replaceAll("%{2,}", "%").replaceAll("%(?!\\w)", "%%"),
				data
			);
	}

	private String getWelcomeMailText(User u) {
		return createWelcomeMailText(new String[] {
				u.getFirstName()
			});
	}

	public void sendWelcomeMail(User u) {
		sendAutogeneatedMail(
			"Добре дошли в Choreographer",
			getWelcomeMailText(u),
			new String[] {u.getEmail()}
		);
	}

	private boolean sendAutogeneatedMail(final String title, final String content, final String[] recipientsEmail) {
		return sendMail(Credentials.mailUser, "Приложение Choreographer", title, content, recipientsEmail);
	}

	private boolean sendMail(final String senderEmail, final String senderName, final String title,
			final String content, final String[] recipientsEmail) {
		try {

			Message message = new MimeMessage(session);

			message.setFrom(new InternetAddress(senderEmail, senderName));
			for(String recipient : recipientsEmail)
				message.setRecipients(
						Message.RecipientType.TO,
						InternetAddress.parse(recipient)
					);

			message.setSubject(title);
			message.setContent(content, "text/html;  charset=utf-8");

			Transport.send(message);

		} catch (Exception e) {
			AccessLog.addLog(
					"Error Sending Mail\n\nemail: " + Arrays.toString(recipientsEmail)
					+ "\nfrom:  " + senderEmail + " <" + senderName + ">"
					+ "\ntitle: " + title
					+ "\n\nCONTENT:\n" + content,
					e,
					null
				);

			return false;
		}

		return true;
	}

}
