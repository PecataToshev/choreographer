package head;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import container.DataHandler;
import head.Config;

public class Startup implements ServletContextListener {


	@Override
	public void contextDestroyed(ServletContextEvent arg0) {}

	@Override
	public void contextInitialized(ServletContextEvent servletEvent) {
		org.postgresql.Driver driver = new org.postgresql.Driver();
		try {
			DriverManager.registerDriver(driver);
		} catch (SQLException e) {}
		Config.setDataPath(
				servletEvent.getServletContext().getInitParameter(
						"DataPath" + ((System.getProperty("os.name").contains("Windows")) ? "Win" : "Linux")
				)
			);

		DataHandler dh = new DataHandler();
		dh.createMusicRecognizer();
	}

}