package head;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.Part;

import access.AccessLog;

public class MusicFileOrganizer {

	private File rootDir;

	public MusicFileOrganizer() {
		rootDir = new File(Config.getAudioDataPath());
		if(!rootDir.exists())
			rootDir.mkdirs();
	}

	private boolean copyInputStreamToFile(InputStream in, File file ) {
		try {
			OutputStream out = new FileOutputStream(file);
			byte[] buf = new byte[1024];
			int len;

			while((len=in.read(buf))>0){
				out.write(buf,0,len);
			}

			out.close();
			in.close();
			return true;

		} catch (Exception e) {
			AccessLog.addLog("Error when copying input Stream to file\ninputStream = " + in + "\nfile = " + file, e, null);
		}
		return false;
	}

	public String upload(String prefix, String filename, String ext, Part file) {
		File saveFile = new File(Config.getAudioDataPath() + filename + File.separator);
		if(!saveFile.exists())
			saveFile.mkdirs();
		try {
			saveFile = new File(saveFile.getAbsolutePath() + File.separator + prefix + filename + ext);
			if(copyInputStreamToFile(file.getInputStream(), saveFile))
				return saveFile.getAbsolutePath().substring(Config.getAudioDataPath().length());
		} catch (IOException e) {
			AccessLog.addLog(
					"Error when copying music\nfile name = " + filename + "\n file path = " + saveFile.getAbsolutePath(),
					e,
					null
				);
		}

		return null;
	}
}
