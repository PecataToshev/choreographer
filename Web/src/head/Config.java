package head;

import java.io.File;

public class Config {
	private static String DataPath;
	private static String AudioDataPath;


	public final static void setDataPath(String dataPath) {
		DataPath = dataPath;
		AudioDataPath = DataPath + File.separator + "audio" + File.separator;
	}





	public final static String getAudioDataPath() {
		return AudioDataPath;
	}
	public final static String getDataPath() {
		return DataPath;
	}





	// WEB
	final public static String urlProjectPrefix = "/Choreographer";


	final public static String servletPrefixApp = "/app/";
	final public static String servletPrefixSign = "/sign/";
	final public static String servletPrefixPrivate = "/private/";

	final public static String staticDataPrefix = servletPrefixPrivate.substring(1) + "static/";
}
