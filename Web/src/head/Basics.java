package head;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletContext;

import access.AccessLog;

public class Basics {

	public static <T> T[] sortListForPrinting(Class<?> objectType, String functionName, List<?> data) {
		int arraySize = 0;
		if(data != null && data.isEmpty() == false && data.size() > 0)
			arraySize = data.size();

		@SuppressWarnings("unchecked")
		T[] result = (T[])java.lang.reflect.Array.newInstance(objectType, arraySize);

		try {
			if(arraySize == 0)
				throw new Exception("Empty data");

			for(int i = 0;  i < data.size();  i++)
				Array.set(result, i, data.get(i));
			Method method = objectType.getMethod(functionName);
			Arrays.sort((T[])result, new Comparator<Object>(){
				@Override
				public int compare(Object a, Object b) {
					try {
						return method.invoke(a).toString().compareToIgnoreCase(method.invoke(b).toString());
					} catch (Exception e) {
						e.printStackTrace();
					}
					return 0;
				}
			});
		} catch (Exception e) {
			AccessLog.addLog("Error when sorting data", e, null);
		}

		return result;
	}

	public static <T> T[] sortListForPrinting(Class<?> objectType, List<?> data) {
		return sortListForPrinting(objectType, "getName", data);
	}





	public static String escapeHTML(String s) {
		if(s == null || s.isEmpty() || s.equals(""))
			return "";
		StringBuilder sb = new StringBuilder();
		int len = s.length();
		for(int i = 0;  i < len;  i++) {
			char c = s.charAt(i);
			switch (c) {
			case '>':
				break;

			case '<':
				break;

			case '\'':
				break;

			case '"':
				break;

			case '/':
				break;

			default:
				sb.append(c);
				break;
			}
		}

		return sb.toString();
	}

	public static String escapeNewLineAndTabs(String s) {
		if(s == null || s.isEmpty() || s.equals(""))
			return "";
		StringBuilder sb = new StringBuilder();
		int len = s.length();
		for(int i = 0;  i < len;  i++) {
			char c = s.charAt(i);
			switch (c) {
			case '\r':
				break;

			case '\n':
				break;

			case '\t':
				break;

			default:
				sb.append(c);
				break;
			}
		}

		return sb.toString();
	}





	public static <T> T[] concatenateArrays(T[] a, T[] b) {
		int aLen = a.length;
		int bLen = b.length;

		@SuppressWarnings("unchecked")
		T[] c = (T[]) Array.newInstance(a.getClass().getComponentType(), aLen + bLen);
		System.arraycopy(a, 0, c, 0, aLen);
		System.arraycopy(b, 0, c, aLen, bLen);

		return c;
	}






	public static Double roundDouble(Double value, int roundingPower) {
		Double round = Math.pow(10, roundingPower);
		return Double.valueOf(((Math.round(value*round))/round));
	}

	public static String convertRhythmTimeToString(Double value) {
		return String.valueOf(roundDouble(value, 3));
	}





	public static String readDefaultDataFromFile(String path) {
		return readDefaultDataFromFile(new File(path));
	}

	public static String readDefaultDataFromFile(File file) {
		StringBuilder data = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(
				new InputStreamReader(
					new FileInputStream(file),
					"UTF8"
				)
			);

			String str;
			while ((str = in.readLine()) != null)
				data.append(str);

			in.close();
		} catch (Exception e) {
			AccessLog.addLog("Error when reading default performance data", e, null);
		}


		return data.toString();
	}





	public static File getStaticResource(ServletContext servletContext, String filename) {
		return new File(servletContext.getRealPath(Config.staticDataPrefix + filename));
	}
}
