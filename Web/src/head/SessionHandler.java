package head;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import access.AccessUser;
import container.EnumContainer.Session;
import models.User;

public class SessionHandler {
	public static String logInUser(User u, HttpSession session) {
		if(u == null)
			return "0";

		session.setAttribute(Session.LOGGED.toString(), "1");
		session.setAttribute(Session.USER.toString(), u);
		return "1";

	}

	public static void updateUserInSession(User user, HttpSession session) {
		AccessUser au = new AccessUser();
		User reselectedUser = au.getByID(user.getId());
		logInUser(reselectedUser, session);
		au.closeEntityManager();
	}

	public static boolean isLoggedIn(HttpSession session) {
		if(session.isNew()){
			//session is new created so log = no
			return false;

		} else if(session.getAttribute(Session.LOGGED.toString()) == null){
			//logged attribute is not created so log = no
			return false;

		} else if(session.getAttribute(Session.LOGGED.toString()).equals("1")) {
			//logged is created and equals 1 so
			return true;

		} else {
			//other cases log = no
			return false;

		}
	}

	public static boolean isLoggedIn(HttpServletRequest request) {
		return isLoggedIn(request.getSession());
	}

	public static User getUser(HttpSession session) {
		if(isLoggedIn(session))
			return (User) session.getAttribute(Session.USER.toString());

		return null;
	}

	public static User getUser(HttpServletRequest request) {
		return getUser(request.getSession());
	}

	public static String getLoggedUserFirstName(HttpSession session) {
		if(isLoggedIn(session))
			return getUser(session).getFirstName();

		return "";
	}
}
