function attachMouseDown(element, down, move, up){
	var mouseUp = function(e){
		$(document).css("pointer-events", "all");
		$(window).unbind("mouseup");
		$(window).unbind("mousemove");
		up(e, element);
	}

	var mouseMove = function(e){
		move(e, element);
	}

	element.mousedown(function(e){
		if(down(e, element)){
			preventAndStop(e);

			$(document).css("pointer-events", "none");
			$(window).bind("mouseup", mouseUp);
			$(window).bind("mousemove", mouseMove);
		}
	});
}

function cloneJSON(data){
	return JSON.parse(JSON.stringify(data));
}

function copyObject(code, type) {
	$.ajax({
		/* 
		 * Parameters.Action.ACTION
		 * Parameters.ObjectData.CODE 
		 */
		url: "/Choreographer/app/ObjectCopy?ACTION=" + type + "&CODE=" + code,
		method: "POST",
		success: function(result) {
			if(result.substr(0, 3) == "OK:")
				edit(result.substr(3));
			else
				errorOnAction(result);
		}, 
		error: errorOnAction
	});
}

function createAndDownloadFile(filename, text) {
	var element = document.createElement('a');
	element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
	element.setAttribute('download', filename);

	element.style.display = 'none';
	document.body.appendChild(element);

	element.click();

	document.body.removeChild(element);
}

function emptyFunction(){}

function errorOnAction(result){
	console.log(result);
	switch(result.status){
	case 403:
		Materialize.toast('Нямате нужните права за да извършите тази операция!', 10000);
		break;

	case 404:
		Materialize.toast('Липсва важен параметър! Моля презаредете страницата си!', 10000);
		break;

	case 500:
		Materialize.toast('Тази операция не може да бъде извършена поради сървърна грешка тип 500.', 10000);
		break;

	default:
		toastError();
		break;
	}

}

function formToJSON(/* jQuery object */form){
	return form.serializeArray().reduce(function(a, x) { a[x.name] = x.value; return a; }, {});
}

function getParameter(sParam){
	var sPageURL = decodeURIComponent(window.location.search.substring(1)),
		sURLVariables = sPageURL.split('&'),
		sParameterName,
		i;

	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');

		if (sParameterName[0] === sParam) {
			return sParameterName[1] === undefined ? true : sParameterName[1];
		}
	}
}

function objectUpdateResult(result) {
	if(result == "OK") {
		Materialize.toast('Успешно запаметено!', 10000);
		return;
	} else if(result == "PUBLIC") {
		Materialize.toast('Операцията не може да бъде изпълнена, защото обектът е публичен!', 10000);
		return;
	} else if(result == "WRONG_CODE") {
		result.status = 404;
	} else if(result == "NOT_ALLOWED") {
		result.status = 403;
	} else if(result == "ERROR") {
		result.status = 500;
	}
	errorOnAction(result);
}

function matrix2DToArray1D(_matrix){
	var data = [];
	for(var c = 0;  c < _matrix.length;  c++)
		for(var r = 0;  r < _matrix[c].length;  r++)
			data.push(_matrix[c][r]);
	return data;
}

function padLeft(text, symbol, length) {
	text = "" + text;
	while(text.length < length)text = symbol + text;
	return text;
}

function preventAndStop(e){
	if(e.stopPropagation)
		e.stopPropagation();
	if(e.preventDefault)
		e.preventDefault();
}

function updateHeight() {
	$("body > section").css("min-height", ($(window).height() - $("body > footer").outerHeight() - ($("body > nav").outerHeight() == undefined ? 0 : $("body > nav").outerHeight())) + "px");

	if (window.references)
		if (window.references.updateHeight)
			window.references.updateHeight();
}

function toastError(){
	Materialize.toast('Нещо се обърка! Моля опитайте по-късно!', 10000);
}

window.onresize = updateHeight;

$(document).ready(function(){
	updateHeight();
	$(".button-collapse:not(.skip)").sideNav();
	$('.modal').modal();
	$('.dropdown-button').dropdown({
		constrainWidth: false, // Does not change width of dropdown to that of the activator
		hover: true,
		belowOrigin: true
	});
	$("select:not(.skip)").material_select();
	$('.input-field > input[data-length]').characterCounter();

	//input invalid initialize
	$("input[required]:not([type=password]):not([oninvalid]):not(.skip), "
			+ "select:not([oninvalid]):not(.skip), textarea:not([oninvalid]):not(.skip)").each(function(){
		$(this)
			.attr("oninvalid", "this.setCustomValidity('Това поле е задължително!')")
			.attr("oninput", "this.setCustomValidity('')");
	});
	$("input[type=password]").each(function(){
		$(this)
			.attr("pattern", ".{8,}")
			.attr("oninvalid", "this.setCustomValidity('Паролата трябва да е минимум 8 символа!')")
			.attr("oninput", "this.setCustomValidity('')");
	});
});