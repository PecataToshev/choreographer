// control => keyboardHandler.checkKeyPressed(keyboardHandler.map.control)
var keyboardHandler = new function() {
	var self = this;
	var mapper = [];

	this.map = {
			backspace: 8,
			tab: 9,
			enter: 13,
			shift: 16,
			control: 17,
			alt: 18,
			pauseBreak: 19,
			capsLock: 20,
			escape: 27,
			pageUp: 33,
			pageDown: 34,
			end: 35,
			home: 36,
			left: 37,
			up: 38,
			right: 39,
			down: 40,
			insert: 45,
			del: 46,
			0: 48,
			1: 49,
			2: 50,
			3: 51,
			4: 52,
			5: 53,
			6: 54,
			7: 55,
			8: 56,
			9: 57,
			a: 65,
			b: 66,
			c: 67,
			d: 68,
			e: 69,
			f: 70,
			g: 71,
			h: 72,
			i: 73,
			j: 74,
			k: 75,
			l: 76,
			m: 77,
			n: 78,
			o: 79,
			p: 80,
			q: 81,
			r: 82,
			s: 83,
			t: 84,
			u: 85,
			v: 86,
			w: 87,
			x: 88,
			y: 89,
			z: 90,
			leftWindowKey: 91,
			rightWindowKey: 92,
			selectKey: 93,
			numpad0: 96,
			numpad1: 97,
			numpad2: 98,
			numpad3: 99,
			numpad4: 100,
			numpad5: 101,
			numpad6: 102,
			numpad7: 103,
			numpad8: 104,
			numpad9: 105,
			multiply: 106,
			add: 107,
			subtract: 109,
			decimalPoint: 110,
			divide: 111,
			f1: 112,
			f2: 113,
			f3: 114,
			f4: 115,
			f5: 116,
			f6: 117,
			f7: 118,
			f8: 119,
			f9: 120,
			f10: 121,
			f11: 122,
			f12: 123,
			numLock: 144,
			scrollLock: 145,
			semiColon: 186,
			equalSign: 187,
			comma: 188,
			dash: 189,
			period: 190,
			forwardSlash: 191,
			graveAccent: 192,
			openBracket: 219,
			backSlash: 220,
			closeBraket: 221,
			singleQuote: 222
		};

	this.checkKeyPressed = function(key) {
		return !!mapper[key];
	}

	this.keyDown = function(e) {
		var key = e.which;
		mapper[key] = true;
		if(window.references) {
			if(mapper[self.map.control] && key == self.map.z) {
				if(window.references.undo)
					window.references.undo();
			} else if(mapper[self.map.control] && key == self.map.y) {
				if(window.references.redo)
					window.references.redo();
			} else if(window.references.keyDown) {
				window.references.keyDown(e, key);
			}
		}
	}

	this.keyUp = function(e) {
		var key = e.which;
		mapper[key] = false;
		if(window.references) {
			if(window.references.keyUp)
				window.references.keyUp(e, key);
		}
	}
}();

$(window).keydown(keyboardHandler.keyDown).keyup(keyboardHandler.keyUp);