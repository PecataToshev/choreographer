<%@page import="access.AccessMusic"%>
<%@page import="container.EnumContainer"%>
<%@page import="head.SessionHandler"%>
<%@page import="java.util.List"%>
<%@page import="head.Basics"%>
<%@page import="models.Music"%>
<%@page import="models.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="body">
	<jsp:include page="/private/appHTML/basics.jsp">
		<jsp:param name="objectType" value="<%= EnumContainer.Parameters.Object.MUSIC %>" />
		<jsp:param name="objectName" value="музика" />
	</jsp:include>
	${createObjectForm}





	<h5>Моите музики</h5>
	<hr />
	<div class="bottom-padding-20">
	<%
	User user = SessionHandler.getUser(session);
	List<Music> lm = null;

	lm = user.getMusics();
	if(lm != null && lm.size() > 0){
		Music[] musics = Basics.sortListForPrinting(Music.class, lm);
		for(Music m : musics){
			%>
			<jsp:include page="/private/appHTML/components/dataShowRowLink.jsp">
				<jsp:param name="typeName" value="performance" />
				<jsp:param name="id" value='<%= m.getAccessCode() %>' />
				<jsp:param name="name" value='<%= m.getName() %>' />
				<jsp:param name="isPublic" value='<%= m.isPublic() ? "t" : "f" %>' />
				<jsp:param name="canEdit" value='<%= user.equals(m.getUser()) ? "t" : "f" %>' />
			</jsp:include>
			<%
		}
	} else {
	%>
		<h6 class="center bolded">Нямате собствени представления</h6>
	<%
	}

	%>
	</div>





	<h5>Публични музики</h5>
	<hr />
	<div id="publicPerformanceHolder">
	<%
	lm = null;

	AccessMusic am = new AccessMusic();
	lm = am.getPublic();

	if(lm != null && lm.size() > 0){
		Music[] musics = Basics.sortListForPrinting(Music.class, lm);
		for(Music m : musics){
			%>
			<jsp:include page="/private/appHTML/components/dataShowRowLink.jsp">
				<jsp:param name="typeName" value="performance" />
				<jsp:param name="id" value='<%= m.getAccessCode() %>' />
				<jsp:param name="name" value='<%= m.getName() %>' />
				<jsp:param name="isPublic" value='<%= m.isPublic() ? "t" : "f" %>' />
				<jsp:param name="canEdit" value='<%= user.equals(m.getUser()) ? "t" : "f" %>' />
			</jsp:include>
			<%
		}
	} else {
	%>
		<h6 class="center bolded">Няма налични публични представления</h6>
	<%
	}

	am.closeEntityManager();
	%>
	</div>
</c:set>

<t:basePage>
	<jsp:attribute name="pageTitle"></jsp:attribute>
	<jsp:attribute name="sectionClasses">row section container</jsp:attribute>
	<jsp:attribute name="_head">
		<script type="text/javascript">
			function edit(code){
				/* 
				 * Parameters.ObjectData.CODE
				 */
				window.location.href = ("/Choreographer/app/editMusic.jsp?CODE=" + code);
			}

			function copy(code) {
				/* 
				 * Parameters.Object.MUSIC
				 */
				copyObject(code, "MUSIC");
			}
		</script>
	</jsp:attribute>

	<jsp:body>${body}</jsp:body>
</t:basePage>