<%@page import="head.Basics"%>
<%@page import="head.SessionHandler"%>
<%@page import="Tester.TesterServlet"%>
<%@page import="container.EnumContainer"%>
<%@page import="access.AccessRhythm"%>
<%@page import="models.Rhythm"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
		pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<c:set var="body">
	<%
	String code = request.getParameter(EnumContainer.Parameters.ObjectData.CODE.toString());
	if(code == null || code.isEmpty() || code.equals("")){
		response.sendRedirect("/Choreographer/app/viewRhythm.jsp");
		return;
	}

	AccessRhythm ar = new AccessRhythm();
	Rhythm r = ar.getByAccessCode(code);

	if(r == null) {
		response.sendRedirect("/Choreographer/app/viewRhythm.jsp");
		response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		return;
	} else if(!r.getBuiltIn() && !r.isPublic() && !SessionHandler.getUser(request).equals(r.getUser())){
		// restrict access
		response.sendRedirect("/Choreographer/app/viewRhythm.jsp");
		response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		return;
	}
	%>
	<h4>Редакция на ритми</h4>
	<form id="rhythm" class="top-padding-20">
		<input name="<%= EnumContainer.Parameters.ObjectData.CODE %>" value="<%= r.getAccessCode() %>" type="hidden" />
		<input id="<%= EnumContainer.Parameters.ObjectData.DATA %>" name="<%= EnumContainer.Parameters.ObjectData.DATA %>" type="hidden" />

		<div class="input-field bottom-padding-20">
			<input id="<%= EnumContainer.Parameters.ObjectData.NAME %>" name="<%= EnumContainer.Parameters.ObjectData.NAME %>"
				class="validate" type="text" data-length="50" maxlength="50" value="<%= r.getName() %>" required />
			<label for="<%= EnumContainer.Parameters.ObjectData.NAME %>">Име</label>
		</div>

		<div id="ruler-container" class="col s12 m8">
			<div id="line">
				<div id="rhythmPlayMarker"></div>
			</div>
			<div id="line-label" class="centered">
				<div></div>
				<label for="line">1 такт</label>
			</div>

			<div id="ruler-functions" class="top-padding-20 centered">
				<div class="input-field">
					<i class="material-icons prefix">timelapse</i>
					<input id="play-length" class="disabled" type="number" step="0.01" oninvalid=""
						value="<%= Basics.convertRhythmTimeToString(Double.valueOf(r.getMinLength() + r.getMaxLength())/2) %>"
						min="<%= Basics.convertRhythmTimeToString(r.getMinLength()) %>"
						max="<%= Basics.convertRhythmTimeToString(r.getMaxLength()) %>" />
					<label for="play-length">Дължина на такта в секунди</label>
				</div>
				<div class="btn teal" onclick="rhythm.add();"><i class="material-icons left">add</i>Добави</div>
				<div id="playPause" class="btn teal" onclick="rhythm.player.playPause();"><i class="material-icons left">play_arrow</i>Пусни</div>
			</div>

			<audio id="beatPlayMusic" src="/Choreographer/app/audio/beat.mp3" type="audio/mpeg"></audio>
		</div>

		<div class="col s12 m4">
			<div class="input-field">
				<input id="<%= EnumContainer.Parameters.Rhythm.MINLENGTH %>" name="<%= EnumContainer.Parameters.Rhythm.MINLENGTH %>"
					value="<%= Basics.convertRhythmTimeToString(r.getMinLength()) %>"
					type="number" min="0.1" max="10" step="0.1"
					onchange="$('#play-length').attr('min', $(this).val())" required />
				<label for="<%= EnumContainer.Parameters.Rhythm.MINLENGTH %>">MIN дължина на такта в секунди</label>
			</div>
			<div class="input-field">
				<input id="<%= EnumContainer.Parameters.Rhythm.MAXLENGTH %>" name="<%= EnumContainer.Parameters.Rhythm.MAXLENGTH %>"
					value="<%= Basics.convertRhythmTimeToString(r.getMaxLength()) %>"
					type="number" min="0.1" max="10" step="0.1"
					onchange="$('#play-length').attr('max', $(this).val())" required />
				<label for="<%= EnumContainer.Parameters.Rhythm.MAXLENGTH %>">MAX дължина на такта в секунди</label>
			</div>
			<div class="input-field">
				<textarea id="<%= EnumContainer.Parameters.Rhythm.DESCRIPTION %>"
					name="<%= EnumContainer.Parameters.Rhythm.DESCRIPTION %>"
					class="materialize-textarea" data-length="255" maxlength="255" 
					required><%= r.getDescription() %></textarea>
				<label for="<%= EnumContainer.Parameters.Rhythm.DESCRIPTION %>">Описание</label>
			</div>
			<div class="switch">
				<label>
					Личен
					<input type="checkbox" id="<%= EnumContainer.Parameters.ObjectData.ISPUBLIC %>" 
						name="<%= EnumContainer.Parameters.ObjectData.ISPUBLIC %>" <%= r.isPublic() ? "checked" : "" %> />
					<span class="lever"></span>
					Публичен
				</label>
			</div>
		</div>


		<div class="centered clear top-padding-20">
			<jsp:include page="/private/appHTML/basics.jsp">
				<jsp:param name="floating" value="f" />
				<jsp:param name="dataFor" value="ритъма" />
				<jsp:param name="hasText" value="t" />
				<jsp:param name="isListedItem" value="f" />
			</jsp:include>
			${imortExport}
			<%
			final String [][] buttons = {
				// {buttonClass, buttonHref, onclickFunction, tooltip, icon, text}
				{"blue", "#", "rhythm.reload(event);", "Презареди данните за ритъма", "refresh", "Презареди"}
			};

			for(String[] button : buttons){
			%>
				<jsp:include page="/private/appHTML/components/actionButton.jsp">
					<jsp:param name="floating" value="f" />
					<jsp:param name="buttonClasses" value="<%= button[0] %>" />
					<jsp:param name="href" value="<%= button[1] %>" />
					<jsp:param name="onClick" value="<%= button[2] %>" />
					<jsp:param name="dataTooltip" value="<%= button[3] %>" />
					<jsp:param name="icon" value="<%= button[4] %>" />
					<jsp:param name="text" value="<%= button[5] %>" />
				</jsp:include>
			<% } %>
			<button class="btn blue" type="submit">Запази<i class="material-icons left">save</i></button>
		</div>

		<div id="beat-cont" class="beat-cont-handler-drag">
			<div class="beat-cont red grab">
				<div class="beat-cont-data white-text grab"></div>
				<i class="material-icons tiny white-text hand" onclick="rhythm.remove(this);">close</i>
			</div>
		</div>
	</form>
</c:set>


<t:generic nav="t" footer="f">
	<jsp:attribute name="pageTitle">Редактиране на ритми</jsp:attribute>
	<jsp:attribute name="sectionClasses">container row</jsp:attribute>
	<jsp:attribute name="_head">
		<link rel="stylesheet" href="/Choreographer/css/jquery-ui.min.css" />
		<script type="text/javascript" src="/Choreographer/js/jquery-ui.min.js"></script>

		<link rel="stylesheet" href="/Choreographer/app/editRhythm.css" />
		<script type="text/javascript" src="/Choreographer/app/editRhythm.js"></script>
	</jsp:attribute>

	<jsp:body>${body}</jsp:body>
</t:generic>