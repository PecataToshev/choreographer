<%@page import="container.EnumContainer"%>
<%@page import="javax.persistence.EnumType"%>
<%@page import="sun.security.pkcs11.Secmod.DbMode"%>
<%@page import="models.PerformancesAccess"%>
<%@page import="access.AccessPerformancesAccess"%>
<%@page import="models.User"%>
<%@page import="models.Music"%>
<%@page import="access.AccessMusic"%>
<%@page import="head.SessionHandler"%>
<%@page import="head.Config"%>
<%@page import="models.Performance"%>
<%@page import="access.AccessPerformance"%>
<%@page import="container.EnumContainer.Parameters"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
		pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
boolean hasAccess = true;
AccessPerformance ap = new AccessPerformance();
Performance p = ap.getByAccessCode(request.getParameter(Parameters.ObjectData.CODE.toString()));
User user = SessionHandler.getUser(session);
EnumContainer.DB.PerformanceAccess currentPerformancesAccess = null;
if(p == null) {
	hasAccess = false;
	response.setStatus(HttpServletResponse.SC_NOT_FOUND);
} else if((currentPerformancesAccess = ap.getPerformanceRights(p, user)) == null) {
	hasAccess = false;
	response.setStatus(HttpServletResponse.SC_FORBIDDEN);
}
ap.closeEntityManager();
if(!hasAccess) {
	response.sendRedirect(Config.urlProjectPrefix + Config.servletPrefixApp); // -> app/
	return;
}
%>
<jsp:include page="/private/appHTML/basics.jsp">
	<jsp:param name="isObjectPublic" value='<%= p.isPublic() ? "t" : "f" %>' />
	<jsp:param name="publicObjectText" value="Представлението е публично!" />
	<jsp:param name="playerClass" value="audioEditPerformance" />

	<jsp:param name="floating" value="f" />
	<jsp:param name="removeBTN" value="t" />
	<jsp:param name="tooltipPosition" value="left" />
	<jsp:param name="dataFor" value="представлението" />
	<jsp:param name="hasText" value="f" />
	<jsp:param name="isListedItem" value="t" />
</jsp:include>

<c:set var="actionButtons">
	<%
	final String [][] buttons = {
		/*
		{ // after click functions
			buttonClass,
			buttonHref,
			onclickFunction,
			tooltip,
			icon,
			text
		}
		*/
		{ // home redirect /app/index.jsp
			"teal",
			"/Choreographer/app/",
			"",
			"Към началото",
			"home",
			""
		},
		{ // help redirect /about/help.jsp
			"teal",
			"/Choreographer/about/help.jsp",
			"",
			"Помощ",
			"help",
			""
		},
		{ // actionButtons.performer.createNew(event)
			"modal-trigger green",
			"#modalNewPerson",
			"",
			"Добави изпълнител",
			"add",
			""
		},
		{ // [(modal-form open #modalOffsetPerson) && (actionButtons.performer.keepOffset())] >-(submit)-
			"modal-trigger yellow darken-2",
			"#modalOffsetPerson",
			"actionButtons.performer.keepOffset();",
			"Запази растоянието до изпълнител",
			"settings_ethernet",
			""
		},
		{ // [(modal-form open #modalOffsetPerson) && (actionButtons.performer.mirror())] >-(submit)-
			"modal-trigger yellow darken-2",
			"#modalOffsetPerson",
			"actionButtons.performer.mirror();",
			"Огледално на изпълнител",
			"flip",
			""
		},
		{ // [(modal-form open #modalOffsetPerson) && (actionButtons.performer.follow())] >-(submit)-
			"modal-trigger yellow darken-2",
			"#modalOffsetPerson",
			"actionButtons.performer.follow();",
			"Следвай изпълнител",
			"reply_all",
			""
		},
		{ // actionButtons.performer.bezierCurve()
			"yellow darken-2",
			"#",
			"actionButtons.performer.bezierCurve();",
			"Следвай път",
			"swap_calls",
			""
		},
		{ // actionButtons.performer.removeKeyFrame(event, true)
			"red",
			"#",
			"actionButtons.performer.removeKeyFrame(event, true);",
			"Премахни избраният важен момент",
			"filter_frames",
			""
		},
		{ // actionButtons.performer.remove(event)
			"red",
			"#",
			"actionButtons.performer.remove(event);",
			"Премахни избраният изпълнител",
			"account_circle",
			""
		},
		{ // actionButtons.saveData()
			"blue",
			"#",
			"actionButtons.saveData();",
			"Запази данните",
			"save",
			""
		},
		{ // (modal-form open #performanceSettings) -(submit)-> (actionButtons.saveSettings())
			"modal-trigger blue",
			"#performanceSettings",
			"",
			"Настройки на представлението",
			"settings",
			""
		},
		{ // actionButtons.reloadData(event)
			"blue",
			"#",
			"reloadSavedPerformanceData(true);",
			"Презареди данните",
			"refresh",
			""
		}
	};

	for(String[] button : buttons){
	%>
		<li>
			<jsp:include page="/private/appHTML/components/actionButton.jsp">
				<jsp:param name="floating" value="f" />
				<jsp:param name="removeBTN" value="t" />
				<jsp:param name="tooltipPosition" value="left" />
				<jsp:param name="buttonClasses" value="<%= button[0] %>" />
				<jsp:param name="href" value="<%= button[1] %>" />
				<jsp:param name="onClick" value="<%= button[2] %>" />
				<jsp:param name="dataTooltip" value="<%= button[3] %>" />
				<jsp:param name="icon" value="<%= button[4] %>" />
				<jsp:param name="text" value="<%= button[5] %>" />
			</jsp:include>
		</li>
	<% } %>
	${imortExport}
</c:set>





<c:set var="forms">
<form id="modalNewPerson" class="modal">
		<div class="modal-content">
			<div class="row">
				<h4>Добави изпълнител</h4>

				<div class="input-field">
					<input id="personName" name="name" type="text" class="validate" data-length="8" required />
					<label for="personName">Име</label>
				</div>

				<div class="top-padding-20"></div>
				<div class="col s3 center">
					Вид:
				</div>
				<div class="col s3 center">
					<input name="type" type="radio" id="r1" value="1" required>
					<label for="r1">Мъж</label>
				</div>

				<div class="col s3 center">
					<input name="type" type="radio" id="r2" value="2">
					<label for="r2">Жена</label>
				</div>

				<div class="col s3 center">
					<input name="type" type="radio" id="r3" value="3" >
					<label for="r3">Друг</label>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<a href="#!" class="btn btn-flat modal-action modal-close waves-effect waves-red">Прекъсни<i class="material-icons right">highlight_off</i></a>
			<button class="btn btn-flat modal-action waves-effect waves-green" type="submit">Добави<i class="material-icons right">send</i></button>
		</div>
	</form>


	<form id="modalOffsetPerson" class="modal">
		<input name="personToOffset" class="hidden" required></input>
		<div class="modal-content">
			<div class="row">
				<div class="col s12 m6">
					<div></div>
					<div></div>
				</div>
				<div class="col s12 m6 performersScrollContainer"></div>
			</div>
		</div>
		<div class="modal-footer">
			<a href="#" class="btn btn-flat modal-action modal-close waves-effect waves-red">Откажи<i class="material-icons right">highlight_off</i></a>
			<button type="submit" class="btn btn-flat hidden modal-action modal-close waves-effect waves-green">Добави<i class="material-icons right">send</i></button>
		</div>
	</form>

	<div id="mirrorDirection" class="hidden">
		<div style="height: 10px;"></div>
		<div class="switch">
			<label>
				<div style="width: 100px; display: inline-block;">По ширина</div>
				<input name="mirrorX" type="checkbox" />
				<span class="lever"></span>
			</label>
		</div>
		<div style="height: 10px;"></div>
		<div class="switch">
			<label>
				<div style="width: 100px; display: inline-block;">В дълбочина</div>
				<input name="mirrorY" type="checkbox" />
				<span class="lever"></span>
			</label>
		</div>
	</div>

	<form id="performanceSettings" class="modal">
		<div class="modal-content">
			<h4 class="row">Настройки на представление</h4>
			<div class="row input-field">
				<input type="text" class="validate" 
					id="<%= container.EnumContainer.Parameters.ObjectData.NAME %>" 
					name="<%= container.EnumContainer.Parameters.ObjectData.NAME %>" 
					value="<%= p.getName() %>" data-length="50" maxlength="50" required aria-required="true" />
				<label for="#<%= container.EnumContainer.Parameters.ObjectData.NAME %>">Име</label>
			</div>
			<% if(!p.isPublic() && currentPerformancesAccess == EnumContainer.DB.PerformanceAccess.EDIT) { %>

				${isPublic}
				<div class="row input-field" style="margin-top: 29px; text-align: left;">
					<select name="<%= container.EnumContainer.Parameters.PerformanceSettings.MUSIC %>"
						required aria-required="true">
						<% 
						AccessMusic am = new AccessMusic();
						List<Music> musics = am.<Music>getAccessible(user);
						am.closeEntityManager();
						for(Music m : musics) {
						%>
							<option value="<%= m.getAccessCode() %>"
								<%= (m.equals(p.getMusic())) ? "selected" : "" %>
								>
								<%= m.getId() + ". " + m.getName() %>
							</option>
						<% } %>
					</select>
					<label>Музика</label>
				</div>
				<div>
					<div></div>
					<div id="personContainer" class="collection">
						<a href="#" class="collection-item">
							<%= user.getFirstName()  + " " + user.getLastName() %>
							<span class="badge">
								<i class="material-icons">perm_identity</i>
							</span>
						</a>
						<%
						AccessPerformancesAccess apa = new AccessPerformancesAccess();
						List<PerformancesAccess> performancesAccess = apa.getAllPeopleWithRights(p);
						for(PerformancesAccess pa : performancesAccess) {
						%>
							<a href="#" class="collection-item">
								<%= pa.getUserBean().getFirstName()  + " " + pa.getUserBean().getLastName() %>
								<span class="badge">
									<i class="material-icons">
										<%= (
											(pa.getRights() == EnumContainer.DB.PerformanceAccess.EDIT)
												? "mode_edit"
												: "remove_red_eye"
											) %>
									</i>
								</span>
							</a>
						<%
						}
						%>
					</div>
				</div>
			<% } %>
		</div>
		<div class="modal-footer">
			<a href="#" class="modal-action modal-close waves-effect waves-red btn-flat">Откажи<i class="material-icons right">highlight_off</i></a>
			<button href="#" class="waves-effect waves-green btn-flat modal-action modal-close"
				onclick="setTimeout(function() { reloadSavedPerformanceData(true); }, 500);" type="submit">
				Обнови <i class="material-icons right">send</i>
			</button>
		</div>
	</form>
</c:set>





<t:generic nav="t" footer="f">
	<jsp:attribute name="pageTitle">Редактиране на представление</jsp:attribute>
	<jsp:attribute name="sectionClasses">row</jsp:attribute>
	<jsp:attribute name="_head">
		<link rel="stylesheet" href="/Choreographer/app/editPerformance.css" />
		<script src="/Choreographer/app/js/math.min.js"></script>
		<script src="/Choreographer/app/js/main.js"></script>
		<script src="/Choreographer/app/js/editPerformance/camera.js"></script>
		<script src="/Choreographer/app/js/editPerformance/geometry/male.js"></script>
		<script src="/Choreographer/app/js/editPerformance/geometry/female.js"></script>
		<script src="/Choreographer/app/js/editPerformance/geometry/other.js"></script>
		<script src="/Choreographer/app/js/editPerformance/geometry/stage.js"></script>
		<script src="/Choreographer/app/js/editPerformance/personModelClass.js"></script>
		<script src="/Choreographer/app/js/editPerformance/webGL.js"></script>
		<script src="/Choreographer/app/js/editPerformance/canvas.js"></script>
		<script src="/Choreographer/app/js/editPerformance/getPath.js"></script>
		<script src="/Choreographer/app/js/editPerformance/actionButtons.js"></script>
	</jsp:attribute>

	<jsp:body>
		<script id="vertexShader" type="x-shader/x-vertex">
			attribute vec3 aVertexPosition;
			attribute vec3 aVertexNormal;

			uniform mat4 uModelViewMatrix;
			uniform mat4 uProjectionMatrix;
			uniform vec3 uLigthVector;
			uniform vec3 uColor; 

			varying lowp vec4 vColor;

			void main() {
				vec4 transformedNormal = uModelViewMatrix * vec4(aVertexNormal, 0);
				vColor = vec4(uColor * (max(dot(normalize(transformedNormal.xyz), normalize(uModelViewMatrix * vec4(uLigthVector, 0)).xyz), 0.0) * 0.7 + 0.3), 1.0);
				gl_Position = uProjectionMatrix * uModelViewMatrix * vec4(aVertexPosition.x, aVertexPosition.y, aVertexPosition.z, 1.0);
			}
		</script>

		<script id="fragmentShader" type="x-shader/x-fragment">
			varying lowp vec4 vColor;

			void main() {
				gl_FragColor = vColor;
			}
		</script>
		<ul id="action-side-nav" class="side-nav fixed">${actionButtons}</ul>
		<a id="action-side-nav-button" class="hidden button-collapse skip show-on-large" data-activates="action-side-nav"></a>
		<a id="buttonShowHideActionButtons" class="white-text hand" state="1" onclick="actionButtons.showHideActionButtons(event, this);">
			<i class="material-icons">fast_forward</i>
		</a>





		<!-- BEGIN OF Forms -->
		${forms}
		<!-- END OF Forms -->





		<div id="keyFrameDataShow" class="hidden">
			<div class="card blue-grey darken-1">
				<div class="card-content white-text">
					<span class="hand tooltipped" data-tooltip="Затвори" style="position: absolute; right: 10px; top: 10px;"
						onclick="mouse.timeline.close();">X</span>
					<div>Такт: <span id="takt" class="bolded"></span></div>
					<div>
						<span id='idHolder' class="editKeyFramePrefix"></span>
						<div class="input-field inline">
							<input name="name" type="text" />
						</div>
					</div>
					<div>
						<span class="editKeyFramePrefix">X:</span>
						<div class="input-field inline">
							<input name="x" type="text" />
						</div>
					</div>
					<div>
						<span class="editKeyFramePrefix">Y:</span>
						<div class="input-field inline">
							<input name="y" type="text" />
						</div>
					</div>
					<div style="position: relative;">
						follow: 
						<span id='follow'></span>
						<input type='hidden' name='removeFollow' />
					</div>
				</div>
				<div class="card-action">
					<a href="#" onclick="mouse.timeline.remove(event);">Изтрий</a>
					<a href="#" onclick="mouse.timeline.update(event);">Обнови</a>
				</div>
			</div>
		</div>





		<div id="stageContainer" class="col s12 center-align teal z-depth-2">
			<canvas id="StageCanvas" width="10" height="10"></canvas>
			<canvas id="StageWebGL" width="10" height="10"></canvas>
		</div>

		<div class="clear"></div>


		${audioPlayer}


		<div id="timelineContainer"></div>


		<div style="display: none;">
			<div class="timelineDiv" id="timelineDivID">
				<div class="nameHolder"></div>
				<div class="timelineHolder"></div>
			</div>

			<div class="timeFrameDiv" id="timeFrameDivID">
				<span class="musicFrameSeparator"></span>
			</div>
			<div class="frame keyFrameDiv" id="keyFrameDivID"></div>
			<div class="frame" id="frameID"></div>
		</div>


		<script src="/Choreographer/app/editPerformance.js.jsp"></script>
	</jsp:body>
</t:generic>