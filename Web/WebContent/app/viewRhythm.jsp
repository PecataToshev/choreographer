<%@page import="access.AccessRhythm"%>
<%@page import="head.Basics"%>
<%@page import="container.EnumContainer"%>
<%@page import="head.SessionHandler"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.Comparator"%>
<%@page import="java.util.List"%>
<%@page import="models.Rhythm"%>
<%@page import="models.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="body">
	<jsp:include page="/private/appHTML/basics.jsp">
		<jsp:param name="objectType" value="<%= EnumContainer.Parameters.Object.RHYTHM %>" />
		<jsp:param name="objectName" value="ритъм" />
	</jsp:include>
	${createObjectForm}






	<h4>Всички ритми</h4>
	<h5>Лични ритми</h5>
	<hr/>
	<%
	User user = SessionHandler.getUser(session);
	AccessRhythm ar = new AccessRhythm();
	List<Rhythm> lr = user.getRhythms();

	if(lr != null && lr.size() > 0){
		Rhythm[] rhytms = Basics.sortListForPrinting(Rhythm.class, lr);
		for(Rhythm r : rhytms){
			%>
			<jsp:include page="/private/appHTML/components/dataShowRowLink.jsp">
				<jsp:param name="typeName" value="rhythm" />
				<jsp:param name="id" value='<%= r.getAccessCode() %>' />
				<jsp:param name="name" value='<%= r.getName() %>' />
				<jsp:param name="isPublic" value='<%= r.isPublic() ? "t" : "f" %>' />
				<jsp:param name="canEdit" value='<%= !r.isPublic() ? "t" : "v" %>' />
			</jsp:include>
			<%
		}
	} else {
		%>
		<h6 class="center bolded">Нямате собствени ритми</h6>
		<%
	}
	%>





	<h5 class="top-padding-20">Вградени ритми</h5>
	<hr/>
	<%
	lr = ar.getBuiltRhythms();
	if(lr != null && lr.size() > 0){
		Rhythm[] rhytms = Basics.sortListForPrinting(Rhythm.class, lr);
		for(Rhythm r : rhytms){
			%>
			<jsp:include page="/private/appHTML/components/dataShowRowLink.jsp">
				<jsp:param name="typeName" value="rhythm" />
				<jsp:param name="id" value='<%= r.getAccessCode() %>' />
				<jsp:param name="name" value='<%= r.getName() %>' />
				<jsp:param name="isPublic" value='f' />
				<jsp:param name="canEdit" value='v' />
			</jsp:include>
			<%
		}
	} else {
		%>
		<h6 class="center bolded">Няма налични вградени ритми</h6>
		<%
	}
	%>





	<h5 class="top-padding-20">Публични ритми</h5>
	<hr/>
	<%
	lr = ar.getPublic();
	if(lr != null && lr.size() > 0){
		Rhythm[] rhytms = Basics.sortListForPrinting(Rhythm.class, lr);
		for(Rhythm r : rhytms){
			%>
			<jsp:include page="/private/appHTML/components/dataShowRowLink.jsp">
				<jsp:param name="typeName" value="rhythm" />
				<jsp:param name="id" value='<%= r.getAccessCode() %>' />
				<jsp:param name="name" value='<%= r.getName() %>' />
				<jsp:param name="isPublic" value='t' />
				<jsp:param name="canEdit" value='v' />
			</jsp:include>
			<%
		}
	} else {
		%>
		<h6 class="center bolded">Няма налични публични ритми</h6>
		<%
	}

	ar.closeEntityManager();
	%>
</c:set>

<t:basePage>
	<jsp:attribute name="pageTitle">Всички ритми</jsp:attribute>
	<jsp:attribute name="sectionClasses">row section container</jsp:attribute>
	<jsp:attribute name="_head">
		<script type="text/javascript">
			function edit(code){
				/* 
				 * Parameters.ObjectData.CODE 
				 */
				window.location.href = ("/Choreographer/app/editRhythm.jsp?CODE=" + code);
			}

			function copy(code) {
				/* 
				 * Parameters.Object.RHYTHM
				 */
				copyObject(code, "RHYTHM");
			}
		</script>
		<style>
			.rhythm-holder {
				padding: 0px 19px;
			}
		</style>
	</jsp:attribute>

	<jsp:body>${body}</jsp:body>
</t:basePage>