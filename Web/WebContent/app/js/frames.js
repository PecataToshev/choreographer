function getKeyFrame(timeline, time){
	if(timeline.length == 0)
		return null;

	let l = 0;
	let r = timeline.length - 1;
	while(l <= r){
		var mid = Math.floor((l+r)/2);

		if(timeline[mid].time < time)
			l = mid + 1;
		else if(timeline[mid].time > time)
			r = mid - 1;
		else return timeline[mid];
	}

	return null;
}

function createKeyFrame(timeline, time){
	var res = getKeyFrame(timeline, time);

	if(res != null)
		return res;

	res = getData(timeline, time);
	res = {
		x: res.x,
		y: res.y,
		dz: res.dz,
		phi: res.phi,
		time: time
	};



	let l = 0;
	let r = timeline.length - 1;
	while(l <= r){
		var mid = Math.floor((l+r)/2);

		if(timeline[mid].time < time)
			l = mid + 1;
		else if(timeline[mid].time > time)
			r = mid - 1;
		else return timeline[mid];
	}


	timeline.splice(l, 0, res);
	return res;
}



function findClosestNextFrameIndex(timeline, takt) {
	let l = 0;
	let r = timeline.length - 1;
	while(l <= r) {
		var mid = Math.floor((l+r)/2);
		if(timeline[mid].time <= takt)
			l = mid + 1;
		else if(l == r)
			return l;
		else
			r = mid;
	}
	return l;
}