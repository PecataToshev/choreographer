function generateTimelineData(){
	if(audio.music.timeFrames === undefined || audio.music.timeFrames == null)
		return;

	timelineContainer.innerHTML = "";

	var containerWidth = 0;
	for(var p = 0;  p < audio.music.timeFrames.length;  p++)
		containerWidth += audio.music.timeFrames[p].number * 7 /* in editPerformance.css */;
	timelineDivID.style.width = (103 /* left margin*/ + 5 /* padding left */ + containerWidth) + "px";
	timelineDivID.querySelector(".timelineHolder").style.width = containerWidth + "px";

	for(var i = 0;  i < data.performers.length;  i++){
		let person = data.performers[i];
		let timeline = timelineDivID.cloneNode(true);
		timeline.removeAttribute("id");
		timeline.querySelector(".nameHolder").innerHTML = person.id + ". " + person.name;
		timeline.setAttribute("performerID", person.id);
		let container = timeline.querySelector(".timelineHolder");
		let time = 0;
		for(let p = 0;  p < audio.music.timeFrames.length;  p++){
			let timeFrame = timeFrameDivID.cloneNode(true);
			timeFrame.removeAttribute("id");

			for(let q = 0;  q < audio.music.timeFrames[p].number;  q++){
				let isKeyFrame = (getKeyFrame(person.timeline, time + q) != null);
				let frame = ((isKeyFrame) ? keyFrameDivID : frameID).cloneNode(true);
				frame.removeAttribute("id");
				frame.setAttribute("takt", time + q);
				frame.setAttribute("performerID", person.id);
				frame.setAttribute("onmousedown", (isKeyFrame ? "mouse.timeline.down(event, this);" : "mouse.timeline.close();"));
				timeFrame.appendChild(frame);
			}
			time += audio.music.timeFrames[p].number;
			container.appendChild(timeFrame);
		}
		attachMouseDown($(container), function(e){mouse.timeline.move(container, e); return true;}, function(e){mouse.timeline.move(container, e)}, function(){});
		timelineContainer.appendChild(timeline);
	}
	let tmp = document.createElement('div');
	tmp.style.height = "6px";
	timelineContainer.appendChild(tmp);

//	let clonePanel = keyFrameDataShowOriginal.cloneNode(true);
//	clonePanel.setAttribute('id', 'keyFrameDataShow');
//	timelineContainer.appendChild(clonePanel);

	if(selectedPerformer != undefined && selectedPerformer != null)
		mouse.select(selectedPerformer, currentTime);
}

function resetUI(){
	currentTime = 0;
	selectedFrame = null;
	selectedPerformer = null;
}
















/* =========== BUTTONS ACTIONS ============================= */

function reloadSavedPerformanceData(isReload){
	$.ajax({
		/* 
		 * Parameters.Action.ACTION
		 * Parameters.Object.PERFORMANCE
		 * Parameters.ObjectData.CODE 
		 */
		url: "/Choreographer/app/ObjectDataGet?ACTION=PERFORMANCE&CODE=" + getParameter("CODE"),
		method: "GET",
		success: function(result){
			if(result == "noSuchPerformance"){
				Materialize.toast('Несъществуващ танц! Моля проверете кода и опитайте отново!', 20000);
			} else if(result.substr(0, 3) == "OK:"){
				result = result.substr(3);
				audio.music.load(result.substr(0, result.indexOf(",")));
				data = JSON.parse(result.substr(result.indexOf(",") + 1));
				dataHistory.store();
				resetUI();
				camera.update();
				setTimeout(updateAll, 700);

				if(isReload)
					Materialize.toast('Данните бяха успешно презаредени!', 5000, "rounded");

			} else {
				toastError();
			}

		}
	});
}

function openPerformanceSettings(){}

