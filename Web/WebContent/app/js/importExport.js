var importExport = new function() {
	var self = this;
	const fileHolderJQuery = $("#importFileHolder");

	this.initialize = function() {
		// set upload file extension
		fileHolderJQuery.attr("accept", "." + references.getExtention());

		// add upload file handler
		fileHolderJQuery.change(function(event){
			let reader = new FileReader();
			reader.onload = function(e) {
				$("#ISPUBLIC").removeAttr("checked").removeAttr("disabled");
				let _data =  JSON.parse(e.target.result);
				for(let key in _data){
					if(key == "CODE"){ /* Parameters.ObjectData.CODE */
						continue;
					} else if(key == "ISPUBLIC"){ /* Parameters.ObjectData.ISPUBLIC */
						if(_data.ISPUBLIC == "on")
							$("#ISPUBLIC").attr("checked", "true").attr("disabled", "true");
					} else if(key == "DATA") { /* Parameters.ObjectData.DATA */
						references.parseData(_data.DATA);
					} else {
						$("#" + key).val(_data[key]);
					}
				}

				Materialize.toast('Успешно вмъкнат файл!', 10000);
			};
			reader.readAsText(event.target.files[0]);
		});
	};

	this.importData = function(e) {
		e.preventDefault();
		fileHolderJQuery.click();
	}

	this.exportData = function(e) {
		e.preventDefault();

		var _data = formToJSON(references.getForm());
		_data.DATA = references.calculateData();
		createAndDownloadFile(_data.NAME + '.' + references.getExtention(), JSON.stringify(_data, null, '\t'));
	}
}();

$(document).ready(function(){ importExport.initialize(); });