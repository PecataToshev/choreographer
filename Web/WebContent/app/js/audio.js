var audio = new function(){
	var selfMain = this;

	this.music = new function(){
		var self = this;
		this.timeFrames = null;

		this.load = function(musicCode){
			selfMain.player.setSource(musicCode);
			$.ajax({
				/* 
				 * Parameters.Action.ACTION
				 * Parameters.Object.MUSIC
				 * Parameters.ObjectData.CODE 
				 */
				url: "/Choreographer/app/ObjectDataGet?ACTION=MUSIC&CODE=" + musicCode,
				method: "GET",
				success: function(result){
					if(result.substr(0, 3) == "OK:"){
						self.timeFrames = JSON.parse(result.substr(3));
						if(window.resetUI)
							resetUI();
						if(window.references.update)
							window.references.update();
					} else {
						Materialize.toast('Грешка при взимането на музикалните данни!', 5000, "rounded");
					}
				},
				error: function(data){
					console.log(data);
					Materialize.toast('Грешка при взимането на музикалните данни!', 5000, "rounded");
				}
			});
		}
	}();

	this.player = new function(){
		var self = this;
		var input = $("#audioPlayerRange");
		var buttonPlay = $("#audioButtonPlayPause");
		var audioElement = audioFileHolder;
		var changing = false;
		var interval = 20;

		this.inputTime = function(){
			changing = true;
			input.val(audioElement.currentTime);
			changing = false;
		};

		this.setTimeFromTakt = function(takt) {
			audioElement.currentTime = audio.player.taktToTime(takt);
			self.inputTime();
		};

		this.onClickOver = function(){
			if(changing)
				return;

			var timeInSeconds = parseFloat(input.val());
			currentTime = Math.round(self.timeToTakt(timeInSeconds));
			self.setTimeFromTakt(currentTime);
			if(window.updateAll)
				updateAll();
		}

		this.setTaktFromTime = function(){
			currentTime = self.timeToTakt(parseFloat(input.val()));
		}

		var cnt = [];

		this.onPlay = function() {
			var now = (new Date()).getTime();
			cnt.push(now);
			if((cnt.length % 20) == 0) {
				var frames = 0;
				for(var i = cnt.length - 1; i >= 0; i--) {
					if(cnt[i] < now - 2000) { 
						if(i > 10) cnt.splice(0, i);
						break;
					}
					frames++;
				}
				window.fps = frames / 2;
			}
			self.inputTime();
			self.setTaktFromTime();
			if(window.redraw.now)
				redraw.now();
		}

		this.playPause = function(){
			if(buttonPlay.attr("stat") == "pause"){
				// start playing
				updateTimeFunction = setInterval(self.onPlay, interval);
				audioElement.play();
				buttonPlay.attr("stat", "play");
				buttonPlay.find("i:first").html("pause");
			} else if(buttonPlay.attr("stat") == "play"){
				// stop playing
				clearInterval(updateTimeFunction);
				audioElement.pause();
				buttonPlay.attr("stat", "pause");
				buttonPlay.find("i:first").html("play_arrow");

				if(selectedFrame != null){
					selectedFrame.removeAttr("selected");
					selectedFrame.parent().parent().parent().removeClass("active");
				}
				currentTime = Math.round(currentTime);
				self.setTimeFromTakt(currentTime);
				if(window.references.update)
					window.references.update();
			}
		};

		this.ready = function(){
			input.val(0);
			input.attr("max", audioElement.duration);
			selfMain.timeline.buildAudioTimeline(audioElement.duration);

			if(window.UpdateTimeframes)
				UpdateTimeframes();
			if(window.references.update)
				window.references.update();

			/*
			if(window.reloadDraggable)
				reloadDraggable();
			if(window.reloadTimeframes)
				reloadTimeframes();
*/
		}

		this.setSource = function(musicCode){
			audioElement.src = "/Choreographer/app/MusicGetAudio?CODE=" + musicCode;
		};

		this.timeToTakt = function(time) {
			var previousNumberOfTakts = 0;
			var i = 0;
			for(i = 0;  i < selfMain.music.timeFrames.length;  i++) {
				var timeframe = selfMain.music.timeFrames[i];
				if(timeframe.start + timeframe.length >= time)
					break;
				previousNumberOfTakts += timeframe.number;
			}

			var taktArr = selfMain.music.timeFrames[i].takts;
			time -= selfMain.music.timeFrames[i].start;
			for(i = 0;  i < taktArr.length;  i++)
				if(taktArr[i][1] > time)
					break;

			if(i == 0)
				return taktArr[0][0] + previousNumberOfTakts;

			if(i >= taktArr.length)
				return taktArr[taktArr.length - 1][0] + previousNumberOfTakts;

			var k = (time - taktArr[i-1][1])/(taktArr[i][1] - taktArr[i-1][1]);
			return taktArr[i-1][0]*(1-k) + taktArr[i][0]*k + previousNumberOfTakts;
		};

		this.taktToTime = function(takt){
			var i = 0;
			var number = 0;
			var previous = 0;
			for(i = 0;  i < selfMain.music.timeFrames.length;  i++) {
				previous = number;
				number += selfMain.music.timeFrames[i].number;
				if(number >= takt)
					break;
			}
			// go to local takt number
			takt -= previous;
			var startTime = selfMain.music.timeFrames[i].start;
			var taktArr = selfMain.music.timeFrames[i].takts;
			for(i = 0;  i < taktArr.length;  i++)
				if(taktArr[i][0] > takt)
					break;

			var left;
			if(i == 0)
				left = [0, 0];
			else
				left = taktArr[i-1];

			if(i >= taktArr.length)
				return taktArr[taktArr.length - 1][1];

			var k = (takt - left[0])/(taktArr[i][0] - left[0]);
			return left[1]*(1-k) + taktArr[i][1]*k + startTime;
		};
	};

	this.timeline = new function(){
		const zoomLength = 2;
		var self = this;
		var timestampDistance = 50;
		var pixelsPerSecond = 6;

		this.getPixelsPerSecond = function() { return pixelsPerSecond; };

		$("#audioTimelineContainer").bind('DOMMouseScroll', function(e){
			if(keyboardHandler.checkKeyPressed(keyboardHandler.map.control)){
				if(e.stopPropagation) e.stopPropagation();
				if(e.preventDefault) e.preventDefault();

				if (e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0) {
					// scroll up
					self.zoomIn(zoomLength);
				} else {
					// scroll down
					self.zoomOut(zoomLength);
				}


				return false;
			}
		});

		this.buildAudioTimeline = function(length) {
			audioTimeline.innerHTML = "";
			audioTimeline.style.width = (length * pixelsPerSecond + 40) + "px";
			audioTimelineCustom.style.width = audioTimeline.style.width;
			audioPlayerRange.style.width = (length * pixelsPerSecond + 16) + "px";
			var secondsPerStamp = timestampDistance / pixelsPerSecond;
			secondsPerStamp = Math.ceil(secondsPerStamp);
			if(secondsPerStamp > 5) secondsPerStamp = Math.ceil(secondsPerStamp / 5) * 5;
			for(let i = 0; i <= length; i += secondsPerStamp) {
				if (i + secondsPerStamp > length) i = length; 
				let timestamp = audioTimestamp.cloneNode(true);
				timestamp.removeAttribute("id");
				timestamp.querySelector(".audioTimestampTime").innerHTML = padLeft(Math.floor(i / 60), "0", 2) + ":" + padLeft(Math.floor(i % 60), "0", 2);
				timestamp.style.left = (i * pixelsPerSecond - 75 + 20) + "px";
				audioTimeline.appendChild(timestamp);
			}
		};

		this.zoomIn = function(len){
			if(pixelsPerSecond >= 500){
				pixelsPerSecond = 500;
				return;
			}
			pixelsPerSecond += len;
			selfMain.player.ready();
		};

		this.zoomOut = function(len){
			if(pixelsPerSecond <= 3){
				pixelsPerSecond = 3;
				return;
			}
			pixelsPerSecond -= len;
			selfMain.player.ready();
		};
	}();
}();