var music = new function() {
	var selfMain = this;
	var audioTimelineContainer;
	var pixelsPerSecond = audio.timeline.getPixelsPerSecond();
	var globalMaximum = parseInt(parseFloat(audioPlayerRange.getAttribute("max")) * pixelsPerSecond) + 2;

	var updateConstants = function() {
		pixelsPerSecond = audio.timeline.getPixelsPerSecond();
		globalMaximum = parseInt(parseFloat(audioPlayerRange.getAttribute("max")) * pixelsPerSecond) + 2;
	};
	var calculateLastMaxElement = function(rhythm) {
		var number = 25000;
		return [number, Math.round(number*rhythm.maxLength)];
	};
	var createBaseTaktPointsArr = function(rhythm) {
		return [[0, 0], calculateLastMaxElement(rhythm)];
	}

	this.timeframe = new function() {
		var self = this;

		this.rhythm = new function() {
			var selfCurrent = this;
			this.map;

			var createSelect = function() {
				// create static element
				var dataToAppend = [];
				selfCurrent.map.forEach(function(element){
					dataToAppend.push("<option value='" + element.accessCode + "'>" + element.name + "</option>");
				});
				dataToAppend = dataToAppend.join("");
				$(".timeFrame:not(#Timeframe) .rhythmSelect").each(function(){
					$(this).attr("opt", $(this).val());
					$(this).html(dataToAppend);
					$(this).val($(this).attr("opt"));
				});
				$("#Timeframe.timeFrame .rhythmSelect").html(dataToAppend);
			};

			this.getRhythmByAccessCode = function(accessCode) {
				for(rhythmMapIndex = selfCurrent.map.length - 1;  rhythmMapIndex > 0 ;  rhythmMapIndex--)
					if(selfCurrent.map[rhythmMapIndex].accessCode == accessCode)
						break;
				return selfCurrent.map[rhythmMapIndex];
			}

			this.reloadMap = function(reloadData) {
				$.ajax({
					url: '/Choreographer/app/RhythmGetMap',
					type: 'GET',
					success: function(result){
						if(result.substr(0, 3) == "OK:"){
							selfCurrent.map = JSON.parse(result.substr(3), (key, value) => key == "data" ? JSON.parse(value) : value);
							createSelect();
							if(reloadData)
								selfMain.reloadData();
						} else {
							errorOnAction(result)
						}
					},
					error: errorOnAction
				});
			};

			this.updateElement = function(selectElement) {
				var parent = selectElement.closest(".timeFrame");
				parent.timeframe.rhythm = selectElement.value;
				parent.timeframe.takts = createBaseTaktPointsArr(selfCurrent.getRhythmByAccessCode(selectElement.value));
				self.paint(parent);
			};
		}();

		this.add = function (timeframe, reloadDragResizeAndRedraw) {
			var newElement = Timeframe.cloneNode(true);
			newElement.timeframe = timeframe;
			newElement.removeAttribute("id");
			newElement.canvas = newElement.querySelector(".TimeframeCanvas");
			newElement.ctx = newElement.canvas.getContext('2d');
			audioTimelineCustom.appendChild(newElement);
			attachMouseDown(
					$(newElement.canvas),
					function(e, element) {
						return select.mouse.down.on(e, element, newElement);
					},
					function(e, element) {
						select.mouse.move.on(e, element, newElement);
					},
					select.mouse.up.on
				);
			if(reloadDragResizeAndRedraw){
				self.update();
				selfMain.reloadDragAndResize();
			}
		};

		this.addNew = function() {
			var dataDistance = 5;
			var maxTime = -dataDistance;
			for(var i = 0; i < audioTimelineCustom.childNodes.length; i++) {
				var oldTimeframe = audioTimelineCustom.childNodes[i].timeframe
				var end = oldTimeframe.start + oldTimeframe.length;
				if(end > maxTime) maxTime = end;
			}

			if(audioPlayerRange.getAttribute("max") < maxTime + dataDistance) // too smal length
				return;

			var rhythm = self.rhythm.map[0];
			var timeframe = {
				rhythm: rhythm.accessCode,
				start: maxTime + dataDistance,
				length: Math.min((audioPlayerRange.getAttribute("max") - (maxTime + dataDistance)), 60),
				takts: createBaseTaktPointsArr(rhythm)
				};
			self.add(timeframe, true);
		};

		this.ensureDataPoint = function(element, takt) {
			var takts = element.timeframe.takts;
			var l = 0;
			var r = takts.length - 1;
			var mid = -1;
			while(l < r - 1){
				mid = Math.floor((l + r) / 2);
				var current = takts[mid][0];
				if(current == takt) {
					return mid;
				} else if(current < takt) {
					l = mid;
				} else {
					r = mid;
				}
			}

			if(takts[l][0] == takt)
				return l;
			if(takts[r][0] == takt)
				return r;

			var taktL = takts[l];
			var taktR = takts[r];
			var percent = (takt - taktL[0])/(taktR[0] - taktL[0]);
			var time = taktL[1] + percent*(taktR[1] - taktL[1]);

			takts.splice(r, 0, [takt, time]);
			return r;
		};

		this.getTaktIndex = function(element, x) {
			var s = x / pixelsPerSecond;
			var takts = element.timeframe.takts;
			var l = 0;
			var r = takts.length - 1;
			var mid = -1;
			while(l < r - 1){
				mid = Math.floor((l + r) / 2);
				var current = takts[mid][1];
				if(current == s) {
					l = mid;
					r = l + 1;
					break;
				} else if(current < s) {
					l = mid;
				} else {
					r = mid;
				}
			}

			var taktL = takts[l];
			var taktR = takts[r];
			var percent = (s - taktL[1])/(taktR[1] - taktL[1]);
			var closest = Math.round(taktL[0] + percent*(taktR[0] - taktL[0]));
			return closest;
		}

		this.paint = function (element) {
			var timeframe = element.timeframe;
			var ctx = element.ctx;

			var taktSelectStart = (element == select.element) ? Math.max(Math.min(select.start, select.end), 0) : 0;
			var taktSelectEnd   = (element == select.element) ? Math.max(select.start, select.end) : 0;

			var rhythmData = self.rhythm.getRhythmByAccessCode(timeframe.rhythm).data;
			var len = parseInt(element.style.width);
			var takts = timeframe.takts;
			var previousTakt = takts[0];
			var takt = 0;
			var taktInfoIndex = 1;
			var x = 0;
			var isInSelect = false;
			ctx.clearRect(0, 0, element.canvas.width, element.canvas.height);
			ctx.strokeStyle="#cccccc";
			ctx.fillStyle = "#0000ff";
			while(x <= len) {
				if(takt == taktSelectStart) isInSelect = true;
				if(takt == taktSelectEnd) isInSelect = false;
				let nextTakt = null;
				if(taktInfoIndex < takts.length)
					nextTakt = takts[taktInfoIndex];
				if(nextTakt) {
					if((nextTakt != previousTakt) && (nextTakt[0] <= takt)) {
						previousTakt = nextTakt;
						taktInfoIndex++;
						continue;
					}
				}
				let taktStart = previousTakt[1] + ((takt - previousTakt[0]) / (nextTakt[0] - previousTakt[0])) * (nextTakt[1] - previousTakt[1]);
				/*?*/let nextTaktStart = previousTakt[1] + ((takt + 1 - previousTakt[0]) / (nextTakt[0] - previousTakt[0])) * (nextTakt[1] - previousTakt[1]);
				x = taktStart * pixelsPerSecond;
				let taktSize = nextTaktStart - taktStart;
				if(isInSelect){
					ctx.rect(x, 0, taktSize * pixelsPerSecond, 40);
					ctx.fill();
				}
				for(let i = 0;  i < rhythmData.length;  i++) {
					let h = rhythmData[i].power * 40 / 100;
					let delta = (rhythmData[i].time * taktSize / 100) * pixelsPerSecond;
					let finalX = Math.round(x + delta);
					ctx.beginPath();
					ctx.moveTo(finalX, 40 - h);
					ctx.lineTo(finalX, 40);
					ctx.stroke();
				}
				takt++;
			}
		}

		this.remove = function(element) {
			audioTimelineContainer.removeChild(element.parentNode.parentNode.parentNode);
		};

		this.update = function() {
			for(var i = 0;  i < audioTimelineCustom.childNodes.length;  i++) {
				var element = audioTimelineCustom.childNodes[i]
				var timeframe = element.timeframe;
				element.style.left  = (timeframe.start  * pixelsPerSecond) + "px";
				element.style.width = (timeframe.length * pixelsPerSecond) + "px";
				element.canvas.width = parseInt(element.style.width);
				element.querySelector(".rhythmSelect").value = timeframe.rhythm;
				self.paint(element);
			}
		};
	}();






	this.initialize = function() {
		audioTimelineContainer = audioTimelineCustom;
		audio.music.load(getParameter("CODE"));
		selfMain.timeframe.rhythm.reloadMap(true);
	};

	this.calculate = function() {
		var newData = [];
		$(".timeFrame:not(#Timeframe)").each(function() {
			newData.push(this.timeframe);
		});
		newData.sort(function(a, b) {
			return a.start - b.start;
		});
		audio.music.timeFrames = newData;
		var previous = 0;
		newData.forEach(function(element) {
			element.number = Math.round(audio.player.timeToTakt(element.start + element.length)) - previous;
			previous += element.number;
		});
		return newData;
	};

	this.calculateAndUpdate = function() {
		selfMain.update();
		return calculate();
	};

	this.parseData = function(_data) {
		audioTimelineCustom.innerHTML = "";
		_data.forEach(function(element){
			selfMain.timeframe.add(element, false);
		});
		selfMain.timeframe.update();
		selfMain.reloadDragAndResize();
	};

	this.reloadData = function() {
		$.ajax({
			/* 
			 * Parameters.Action.ACTION
			 * Parameters.Object.MUSIC
			 * Parameters.ObjectData.CODE 
			 */
			url: "/Choreographer/app/ObjectDataGet?ACTION=MUSIC&CODE=" + getParameter("CODE"),
			method: "GET",
			success: function(result){
				if(result.substr(0, 3) == "OK:"){
					selfMain.parseData(JSON.parse(result.substr(3)));
				} else {
					errorOnAction(result);
				}

			},
			error: errorOnAction
		});
	}

	this.reloadDragAndResize = function() {
		$(".timeFrame").draggable({
			axis: "x",
			handle: ".dragTimeFrame",
			containment: "parent",
			scroll: false,
			drag: function(event, ui) {

				var pc = ui.helper[0].previousSibling;
				var nc = ui.helper[0].nextSibling;
				var min = 0;
				var max = 100000;

				if(pc)
					min = (pc.timeframe.start + pc.timeframe.length) * pixelsPerSecond;
				if(nc)
					max = (nc.timeframe.start) * pixelsPerSecond;
				if(max > globalMaximum)
					max = globalMaximum;
				max -= ui.helper[0].timeframe.length * pixelsPerSecond;

				ui.position.left = Math.min(ui.position.left, max); 
				ui.position.left = Math.max(ui.position.left, min); 
			},
			start: function(event, ui) {
				ui.helper.find(".grab").removeClass("grab").addClass("grabbing");
			},
			stop: function(event, ui) {
				ui.helper.find(".grabbing").removeClass("grabbing").addClass("grab");
				ui.helper[0].timeframe.start = ui.position.left / pixelsPerSecond;
			}
		});

		$(".timeFrame")
		.resizable()
		.resizable({
			handles: 'w, e',
			minWidth: 160,
			resize: function(event, ui) {
				var pc = ui.helper[0].previousSibling;
				var nc = ui.helper[0].nextSibling;
				var min = 0;
				var max = 100000;
				if(pc)
					min = (pc.timeframe.start + pc.timeframe.length) * pixelsPerSecond;
				if(nc)
					max = (nc.timeframe.start) * pixelsPerSecond;
				if(max > globalMaximum)
					max = globalMaximum;
				max -= ui.helper[0].timeframe.start * pixelsPerSecond;
				if(ui.position.left == ui.originalPosition.left) {
					ui.size.width = Math.min(ui.size.width, max); 
				} else {
					ui.position.left = Math.max(ui.position.left, min);
					ui.size.width = ui.originalPosition.left + ui.originalSize.width - ui.position.left;
				}
				ui.helper[0].canvas.width = parseInt(ui.size.width);
				selfMain.timeframe.paint(ui.helper[0]);
			},
			/*start: function(event, ui) {
				updateConstants();
			},*/
			stop: function(event, ui) {
				ui.helper[0].timeframe.start = ui.position.left / pixelsPerSecond;
				ui.helper[0].timeframe.length = ui.size.width / pixelsPerSecond;
			}
		});
	};

	this.update = function() {
		updateConstants();
		selfMain.timeframe.update();
	};
}();
