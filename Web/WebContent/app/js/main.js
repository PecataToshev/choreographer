var current = new function() {
	var self = this;

	this.angle = new function() {
		var selfCurrent = this;
		var x = Math.PI / 2;
		var z = Math.PI / 2;
		const minimumZ = 5.3/180*Math.PI;

		this.get = {
			x: function() { return x; },
			z: function() { return z; }
		};

		this.set = {
			x: function(newValue) {
				if(newValue < 0 || newValue > Math.PI/2)
					return;
				x = newValue;
			},
			z: function(newValue) {
				if(newValue < Math.PI/4 || newValue > 3*Math.PI/4)
					return;
				if(((newValue + minimumZ > Math.PI / 2) && (newValue < Math.PI / 2)) || ((newValue - minimumZ < Math.PI / 2) && (newValue > Math.PI / 2)))
					z = Math.PI / 2;
				else
					z = newValue;
			}
		}
	}();
	this.takt = 0;
}();
var currentTime = 0;
var canvasParameters = {origin: {x: 0, y: 0}};
var projectionDistance;
var selectedFrame = null;
var matrix;
var matrixInverted;
var data;
//var personRadius = 0.25;
var selectedPerformer = null;
var selectedBezierCurvePointTimelineIndex = null;
var updateTimeFunction = null;

math.vLength = function(v){
	var len = 0;
	for(var i = 0; i < v.length; i++)len += v[i] * v[i];
	return Math.sqrt(len);
}

math.vNormalize = function(v) {
	var res = [];
	var len = math.vLength(v);
	for(var i = 0; i < v.length; i++)
		res.push(v[i] / len);
	return res;
}

function getPerformerByID(id){
	for(var i = 0;  i < data.performers.length;  i++)
		if(data.performers[i].id == id)
			return data.performers[i];

	return null;
}










function getData(timeline, takt) {
	if(timeline.length == 0)
		return null;
	let c = getPath.func(timeline, takt, takt+1);
	return {
		x: c[0].x,
		y: c[0].y,
		dz: -1,
		phi: 50
	};
}

function getPathLengthTypeLinear(left, right, fromK, toK, fromTimeCurrent, toTimeCurrent, config) {
	if(right == null)
		return 0;
	let x1 = extrapolateLinear(left.x, right.x, fromK);
	let y1 = extrapolateLinear(left.y, right.y, fromK);
	let x2 = extrapolateLinear(left.x, right.x, toK);
	let y2 = extrapolateLinear(left.y, right.y, toK);
	return Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
}

function getPathLengthTypeBezierCurve(left, right, fromK, toK, fromTimeCurrent, toTimeCurrent, config) {
	let p0 = {x: left.x, y: left.y};
	let p1 = {x: config.point.x, y: config.point.y};
	let p2 = {x: right.x, y: right.y};
	let res1 = bezierIntegral(p0, p1, p2, bezierCurveFindClosestT(p0, p1, p2, fromK));
	let res2 = bezierIntegral(p0, p1, p2, bezierCurveFindClosestT(p0, p1, p2, toK));
	return res2 - res1;
}

function getPathLengthTypeFollowOffset(left, right, fromK, toK, fromTimeCurrent, toTimeCurrent, config) {
	var targetTimeline = getPerformerByID(left.follow.id);
	if(targetTimeline == null)
		return 0;
	return getPathLength(targetTimeline, fromTimeCurrent, toTimeCurrent);
}

function getPathLengthTypeFollowFollow(left, right, fromK, toK, fromTimeCurrent, toTimeCurrent, config) {
	return 0;
}

function getPathLength(timeline, fromTime, toTime){
	let indexFromTime = findClosestNextFrameIndex(timeline, fromTime) - 1;
	let indexNextToTime = findClosestNextFrameIndex(timeline, toTime);
	for(let i = indexFromTime;  i < indexNextToTime;  i++) {
		let left = timeline[i];
		let right = ((i+1 < timeline.lenght) ? timeline[i+1] : null);
		let fromTimeCurrent = Math.max(fromTime, left.time);
		let toTimeCurrent = (right != null) ? Math.min(toTime, right.time) : (toTime);
		let fromK = (right != null) ? (fromTimeCurrent - left.time)/(right.time - left.time) : 0;
		let toK = (right != null) ? (toTimeCurrent - left.time)/(right.time - left.time) : 0;
		var extrapolate = getPathLengthTypeLinear;
		var config = null;
		if(left.follow) {
			config = left.follow;
			if(left.follow.type == "offset" || left.follow.type == "mirror")
				extrapolate = getPathLengthTypeFollowOffset;
			else if(left.follow.type == "follow")
				extrapolate = getPathLengthTypeFollowFollow;
			else if(left.follow.type == "bezier")
				extrapolate = getPathLengthTypeBezierCurve;
		}
		return extrapolate(left, right, fromK, toK, fromTimeCurrent, toTimeCurrent, config);
	}
}
	
function getPathLengthOld(timeline, fromTime, toTime){
	var localCurrentTime;
	var prevData = getData(timeline, fromTime);
	var currentData;

	var length = 0;
	for(localCurrentTime = Math.ceil(fromTime);  localCurrentTime <= Math.floor(toTime);  localCurrentTime++){
		currentData = getData(timeline, localCurrentTime);
		length += Math.sqrt(Math.pow(currentData.x - prevData.x, 2) + Math.pow(currentData.y - prevData.y, 2));
		prevData = currentData
	}

	currentData = getData(timeline, toTime);
	length += Math.sqrt(Math.pow(currentData.x - prevData.x, 2) + Math.pow(currentData.y - prevData.y, 2));

	return length;
}


function distanceBetweenPointAndRay(rayPoint, rayVector, point){
	var d = -rayVector.x*point.x - rayVector.y*point.y - rayVector.z*point.z;
	var k = -(rayVector.x*rayPoint.x + rayVector.y*rayPoint.y + rayVector.z*rayPoint.z + d);
	k /= rayVector.x*rayVector.x + rayVector.y*rayVector.y + rayVector.z*rayVector.z;
	var intersectionPoint = [rayPoint.x + k*rayVector.x, rayPoint.y + k*rayVector.y, rayPoint.z + k*rayVector.z];
	return Math.sqrt(Math.pow(point.x-intersectionPoint[0], 2) + Math.pow(point.y-intersectionPoint[1], 2) + Math.pow(point.z-intersectionPoint[2], 2));
}








function transformAndProject(point){
	var res = math.multiply(matrixInverted, [[point.x], [point.y], [point.z], [1]]);

	//perspektiva
	res[0][0] = res[0][0]*projectionDistance/res[2][0];
	res[1][0] = res[1][0]*projectionDistance/res[2][0];


	return {x: res[0][0] + canvasParameters.origin.x, y: res[1][0] + canvasParameters.origin.y, z: res[2][0]};
}

function setAngle(angle){
	camera.position.y = camera.distance*Math.cos(angle*Math.PI/180);
	camera.position.z = camera.distance*(-Math.sin(angle*Math.PI/180));
	redraw.now();
}



var updateCurrentHeight = function(){
	var actionButtonsMenuWidth = 73; // px
	$("section").width($(window).width() - (($("#buttonShowHideActionButtons").attr("state") == 1) ? actionButtonsMenuWidth : 0));

	var stage = $("#StageCanvas");
	stage.unbind();
	attachMouseDown(stage, mouse.stage.down, mouse.stage.move, mouse.stage.up);
	stage.bind('DOMMouseScroll', mouse.stage.wheel);

	stage.attr("width", Math.floor($("#stageContainer").width()));
	stage.attr("height", Math.floor($("#stageContainer").height()));
	$("#StageWebGL").attr("width", stage.attr("width")).attr("height", stage.attr("height"));

	document.addEventListener('contextmenu', function(event) { return event.preventDefault(); });

	redraw.now();
}


function initializeBody(){
	var actionButtonsMenuWidth = 73; // px
	$("nav, footer").remove();

	$("#action-side-nav-button").sideNav({
		menuWidth: actionButtonsMenuWidth,
		edge: 'right'
	});

	updateCurrentHeight();

	//forms
	$('#modalNewPerson').submit(actionButtons.performer.createNew);
	$('#performanceSettings').submit(actionButtons.saveSettings);

	StageWebGL.addEventListener("webglcontextrestored", webGL.onContextLost, false);
}

function updateAll(){
	redraw.now();
	generateTimelineData();
}

var dataHistory = new function() {
	var self = this;
	var currentPosition = 0;
	var queue = [];

	this.store = function() {
		if(currentPosition != 0)
			queue.splice(currentPosition);
		queue.push(JSON.stringify(data));
		currentPosition++;
	};
	
	this.undo = function() {
		if(currentPosition <= 0)
			return;
		if(currentPosition == queue.length) {
			self.store();
			currentPosition--;
		}
		currentPosition--;
		references.parseData(JSON.parse(queue[currentPosition]));
	}
	
	this.redo = function() {
		if(currentPosition >= queue.length)
			return;
		currentPosition++;
		references.parseData(JSON.parse(queue[currentPosition]));
	}
}();

var references = new function() {
	var self = this;
	const form = "performanceSettings";
	const extention = "performance";

	this.getForm = function() {
		return $("#" + form);
	}

	this.getExtention = function() {
		return extention;
	}

	this.init = function() {

	}

	this.calculateData = function(_data) {
		return data;
	}

	this.parseData = function(_data) {
		data = _data;
		self.update();
	}
	
	this.undo = dataHistory.undo;

	this.redo = dataHistory.redo;

	this.update = function() {
		updateAll();
	};

	this.updateHeight = updateCurrentHeight;
}();

$(document).ready(function(){

	initializeBody();
	reloadSavedPerformanceData();

});