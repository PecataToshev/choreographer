var personModelClass = function(model) {
	var self = this;
	var faces = model.f;
	var verts = model.v;
	var normals = model.vn;
	var positions = [];
	var positionsNormals = [];
	var colors = [];
	var colorBuffer;
	var positionBuffer;
	var lightBuffer;

	function concatv3(a, v) {
		a.push(v[0]);
		a.push(v[1]);
		a.push(v[2]);
	}

	for(var i = 0; i < faces.length; i++) {
		var face = faces[i];
		//normals[face[2] - 1]
		for(var j = 3; j < face.length - 3; j+=2) {
			concatv3(positions, verts[face[1] - 1]);
			concatv3(positions, verts[face[j] - 1]);
			concatv3(positions, verts[face[j + 2] - 1]);
			concatv3(positionsNormals, normals[face[2] - 1]);
			concatv3(positionsNormals, normals[face[j + 1] - 1]);
			concatv3(positionsNormals, normals[face[j + 3] - 1]);
		}
	}

	this.init = function(gl) {
		positionBuffer = gl.createBuffer();
		colorBuffer = gl.createBuffer();
		lightBuffer = gl.createBuffer();

		gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);

		gl.bindBuffer(gl.ARRAY_BUFFER, lightBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positionsNormals), gl.STATIC_DRAW);

		gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);
	};

	this.render = function(gl, aVertexPosition, aVertexNormal) {
		gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
		gl.vertexAttribPointer(
				aVertexPosition,
				3,
				gl.FLOAT,
				false,
				0,
				0);
		gl.enableVertexAttribArray(aVertexPosition);

		gl.bindBuffer(gl.ARRAY_BUFFER, lightBuffer);
		gl.vertexAttribPointer(
				aVertexNormal,
				3,
				gl.FLOAT,
				false,
				0,
				0);
		gl.enableVertexAttribArray(aVertexNormal);

		gl.drawArrays(gl.TRIANGLES, 0, positions.length / 3);
	};
};