var getPath = new function() {
	var self = this;

	function concat(a, b) {
		for(let i = 0;  i < b.length;  i++)
			a.push(b[i]);
	}

	function extrapolateLinear(x1, x2, k){
		return (x1*(1-k)+x2*k);
	}

	function linear(left, right, fromK, toK, fromTimeCurrent, toTimeCurrent, config) {
		if(right == null)
			right = left;
		let x1 = extrapolateLinear(left.x, right.x, fromK);
		let y1 = extrapolateLinear(left.y, right.y, fromK);
		let x2 = extrapolateLinear(left.x, right.x, toK);
		let y2 = extrapolateLinear(left.y, right.y, toK);
		let x = (x1 + x2)/2;
		let y = (y1 + y2)/2;
		return [
			{x: x1, y: y1, time: fromTimeCurrent},
			{x: x, y: y},
			{x: x2, y: y2, time: toTimeCurrent}
		];
	}

	function solveLinear2(a11, a12, a13, a21, a22, a23) {
		let zn = a11*a22 - a12*a21;
		if(Math.abs(zn) < 0.0001) return null;
		let x = -(a13*a22 - a12*a23)/zn;
		let y = -(a11*a23 - a13*a21)/zn;
		return {x: x, y: y};
	}

	function separateBezierCurveBase (p0, p1, p2, t, curr) {
		/**
		 * return array
		 * arr[0] = {x, y} old helper point
		 * arr[1] = {x, y} new helper point
		 */
		let pn = curr;
		if(pn == undefined)
			pn = calculateBezierCurve(p0, p1, p2, t);
		let ap2 = -(p2.y - p1.y);
		let bp2 = +(p2.x - p1.x);
		let cp2 = -ap2*p2.x - bp2*p2.y;
		let ap0 = -(p1.y - p0.y);
		let bp0 = +(p1.x - p0.x);
		let cp0 = -ap0*p0.x - bp0*p0.y;
		let bm = +(t*(2*p0.x - 4*p1.x + 2*p2.x) + 2*p1.x - 2*p0.x);
		let am = -(t*(2*p0.y - 4*p1.y + 2*p2.y) + 2*p1.y - 2*p0.y);
		let cm = -am*pn.x - bm*pn.y;
		let mid2 = solveLinear2(ap2, bp2, cp2, am, bm, cm);
		let mid0 = solveLinear2(ap0, bp0, cp0, am, bm, cm);
		if(mid0 == null) {
			mid0 = {x: (p0.x + pn.x) / 2, y:(p0.y + pn.y) / 2};
		}
		if(mid2 == null) {
			mid2 = {x: (p2.x + pn.x) / 2, y:(p2.y + pn.y) / 2};
		}
		return [mid0, mid2];
	}

	this.separateBezierCurve = function(prev, curr, next) {
		let p0 = {x: prev.x, y: prev.y};
		let p1 = {x: prev.follow.point.x, y: prev.follow.point.y};
		let p2 = {x: next.x, y: next.y};
		let k = (curr.time - prev.time) / (next.time - prev.time);
		let t = bezierCurveFindClosestT(p0, p1, p2, k);
		return separateBezierCurveBase(p0, p1, p2, t, curr);
	}

	function bezier(left, right, fromK, toK, fromTimeCurrent, toTimeCurrent, config) {
		let p0 = {x: left.x, y: left.y, time: left.time, follow: left.follow};
		let p1 = {x: left.follow.point.x, y: left.follow.point.y};
		let p2 = {x: right.x, y: right.y, time: right.time};
		let fromT = bezierCurveFindClosestT(p0, p1, p2, fromK);
		if(fromK > 0) {
			p0 = calculateBezierCurve(p0, p1, p2, fromT);
			p0.time = fromTimeCurrent;
			let sep = self.separateBezierCurve(left, p0, right);
			p0.follow = {point: p1 = sep[1]};
		}

		let toT = bezierCurveFindClosestT(p0, p1, p2, (toK - fromK)/(1 - fromK));
		if(toK < 1) {
			p2 = calculateBezierCurve(p0, p1, p2, toT);
			p2.time = toTimeCurrent;
			let sep = self.separateBezierCurve(p0, p2, right);
			p1 = sep[0];
		}
		delete p0.follow;
		return [p0, p1, p2];
	}

	function mirror(left, right, fromK, toK, fromTimeCurrent, toTimeCurrent, config) {
		let targetTimeline = getPerformerByID(left.follow.id);
		if(targetTimeline == null)
			return left;
		targetTimeline = targetTimeline.timeline;
		let res = getBezierPoints(targetTimeline, fromTimeCurrent, toTimeCurrent);
		if(res.length == 0)
			return [];
		let x = left.follow.mirrorX;
		let y = left.follow.mirrorY;
		let leftTimeTargetTimelineData = getData(targetTimeline, left.time);

		let v1 = {
				x: left.x - leftTimeTargetTimelineData.x,
				y: left.y - leftTimeTargetTimelineData.y
			};
		let v2 = {x: 0, y: 0};
		if(right != null) {
			let rightTimeTargetTimelineData = getData(targetTimeline, right.time);
			rightTimeTargetTimelineData.x = left.x + ((x) ? -1 : 1)*(rightTimeTargetTimelineData.x + v1.x - left.x);
			rightTimeTargetTimelineData.y = left.y + ((x) ? -1 : 1)*(rightTimeTargetTimelineData.y + v1.y - left.y);
			v2.x = right.x - rightTimeTargetTimelineData.x;
			v2.y = right.y - rightTimeTargetTimelineData.y;
		}
		for(let i = 0;  i < res.length;  i++) {
			let p = res[i];
			if(i%2 == 0) {
				p.x = left.x + ((x) ? -1 : 1)*(p.x + v1.x - left.x);
				p.y = left.y + ((y) ? -1 : 1)*(p.y + v1.y - left.y);
				if(right != null) {
					let k = (p.time - left.time)/(right.time - left.time);
					p.x += v2.x*k;
					p.y += v2.y*k;
				}
			} else {
				p.x = left.x + ((x) ? -1 : 1)*(p.x + v1.x - left.x);
				p.y = left.y + ((y) ? -1 : 1)*(p.y + v1.y - left.y);
				if(right != null) {
					let k1 = (res[i-1].time - left.time)/(right.time - left.time);
					let k2 = (res[i+1].time - left.time)/(right.time - left.time);
					let k = (k1 + k2) / 2;
					p.x += v2.x*k;
					p.y += v2.y*k;
				}
			}
		}
		return res;
	}

	function findTimeForDistance(arr, targetDistance) {
		let itd = targetDistance;
		for(let i = 2;  i < arr.length;  i+=2) {
			let p0 = arr[i-2];
			let p1 = arr[i-1];
			let p2 = arr[i];
			let integral0 = bezierIntegral(p0, p1, p2, 0);
			let integral1 = bezierIntegral(p0, p1, p2, 1);
			let currentDistance = (integral1 - integral0);
			if(currentDistance <= targetDistance) {
				targetDistance-= currentDistance;
				continue;
			}
			return [
				(p0.time - arr[0].time)
				+ ((targetDistance / currentDistance) * (p2.time - p0.time)),
				0];
		}
		let time = (arr[arr.length-1].time - arr[0].time);
		if(time > 0.001) {
			return [null, (itd - targetDistance) / (arr[arr.length-1].time - arr[0].time)];
		}
		else return [null, 0];
	}

	function cutBezierPathToMatchTime(arr, fromTime, toTime) {
		while(arr[2].time <= fromTime)
			arr.splice(0, 2);
		while(arr.length > 2 && arr[arr.length - 3].time >= toTime)
			arr.splice(arr.length - 2, 2);

		if(arr[0].time < fromTime) {
			arr[0].follow = {point: arr[1]};
			let k = (fromTime - arr[0].time) / (arr[2].time - arr[0].time);
			let newP = calculateBezierCurve(arr[0], arr[1], arr[2], bezierCurveFindClosestT(arr[0], arr[1], arr[2], k));
			let sep = self.separateBezierCurve(arr[0], newP, arr[2]);
			newP.time = fromTime;
			arr[0] = newP;
			arr[1] = sep[1];
		}

		let last = arr.length - 1;
		if(arr[last].time > toTime) {
			let p0 = arr[last - 2];
			let p1 = arr[last - 1];
			let p2 = arr[last];
			p0.follow = {point: p1};
			let k = (toTime - p0.time) / (p2.time - p0.time);
			let newP = calculateBezierCurve(p0, p1, p2, bezierCurveFindClosestT(p0, p1, p2, k));
			let sep = self.separateBezierCurve(p0, newP, p2);
			newP.time = toTime;
			arr[last] = newP;
			arr[last - 1] = sep[0];
		}
	}

	function follow(left, right, fromK, toK, fromTimeCurrent, toTimeCurrent, config) {
		let targetTimeline = getPerformerByID(left.follow.id);
		if(targetTimeline == null)
			return left;
		targetTimeline = targetTimeline.timeline;
		let res = getBezierPoints(targetTimeline, left.time, toTimeCurrent + 10000);
		let vx = res[0].x - left.x;
		let vy = res[0].y - left.y;
		let vxo = vx;
		let vyo = vy;
		let distanceToReachFirstPoint = Math.sqrt(vx * vx + vy * vy);
		vx /=  distanceToReachFirstPoint;
		vy /=  distanceToReachFirstPoint;
		let timeData = findTimeForDistance(res, distanceToReachFirstPoint);
		let path;
		let neededTimeToReachFirstPoint = timeData[0];
		if(neededTimeToReachFirstPoint == null) {
			let mul = (toTimeCurrent - left.time) * timeData[1];
			let vx1 = vx * mul;
			let vy1 = vy * mul;
			path = linear(left, {x: left.x + vx1, y: left.y + vy1}, 0, 1, left.time, toTimeCurrent)
		} else {
			path = linear(left, res[0], 0, 1, left.time, res[0].time);
			path.length = 2;
			concat(path, cloneJSON(res));
			path[2].time += neededTimeToReachFirstPoint;
			let targetTime = res[0].time;
			let targetIndex = 0;
			let followerIndex = 0;
			while(followerIndex < path.length) {
				let targetDistanceToKeyFrame;
				let targetNextKeyFrameIndex;
				while(targetIndex < res.length - 2) {
					if(res[targetIndex + 2].time > targetTime)
						break;
					targetIndex += 2;
				}
				if(targetIndex == res.length - 1) {
					path.splice(followerIndex + 1);
					break;
				} else if(targetIndex < res.length - 1) {
					targetNextKeyFrameIndex = targetIndex + 2;
					let k = (targetTime - res[targetIndex].time) / (res[targetNextKeyFrameIndex].time - res[targetIndex].time);
					let p0 = res[targetIndex];
					let p1 = res[targetIndex + 1];
					let p2 = res[targetIndex + 2];
					let integral0 = bezierIntegral(p0, p1, p2, 0);
					let integral1 = bezierIntegral(p0, p1, p2, 1);
					targetDistanceToKeyFrame = (integral1 - integral0) * (1 - k);
				} else {
					targetDistanceToKeyFrame = 0;
					targetNextKeyFrameIndex = targetIndex;
				}

				let followerDistanceToKeyFrame;
				if(followerIndex < path.length - 1) {
					let p0 = path[followerIndex];
					let p1 = path[followerIndex + 1];
					let p2 = path[followerIndex + 2];
					let integral0 = bezierIntegral(p0, p1, p2, 0);
					let integral1 = bezierIntegral(p0, p1, p2, 1);
					followerDistanceToKeyFrame = (integral1 - integral0);
					if(followerDistanceToKeyFrame < 0.0001) {
						followerIndex += 2;
						continue;
					}
				} else
					break;
				if(targetDistanceToKeyFrame >= followerDistanceToKeyFrame) {
					let targetTimeAtDistance = 0;
					if(targetDistanceToKeyFrame > 0.0001) {
						targetTimeAtDistance = extrapolateLinear(
								targetTime,
								res[targetNextKeyFrameIndex].time,
								(followerDistanceToKeyFrame / targetDistanceToKeyFrame));
					}
					followerIndex+=2;
					path[followerIndex].time = targetTimeAtDistance;
					targetTime = targetTimeAtDistance;
				} else {
					let p0 = {
							x:path[followerIndex].x,
							y:path[followerIndex].y
						};
					let p1 = {
							x:path[followerIndex + 1].x,
							y:path[followerIndex + 1].y
						};
					let k = targetDistanceToKeyFrame / followerDistanceToKeyFrame;
					let t = bezierCurveFindClosestT(p0, path[followerIndex + 1], path[followerIndex + 2], k);
					let pnew = calculateBezierCurve(p0, path[followerIndex + 1], path[followerIndex + 2], t);
					let mids = separateBezierCurveBase(
							p0,
							p1,
							path[followerIndex + 2],
							t
						);
					path[followerIndex + 1] = mids[0];
					pnew.time = res[targetNextKeyFrameIndex].time;
					path.splice(followerIndex + 2, 0, pnew);
					path.splice(followerIndex + 3, 0, mids[1]);
					targetTime = res[targetNextKeyFrameIndex].time;
					followerIndex += 2;
				}
			}
		}
		// combine with right key frame
		let v2 = {x: 0, y: 0};
		if(right != null) {
			let rightTimeTargetTimelineData = {x: right.x, y: right.y};
			if(neededTimeToReachFirstPoint == null) {
				let mul = (right.time - left.time) * timeData[1];
				rightTimeTargetTimelineData = {x : left.x + vx * mul, y: left.y + vy * mul};
			}
			else {
				let i;
				for(i = 0; i < path.length - 2; i+=2) {
					if(path[i + 2].time > right.time)break;
				}
				if(i == path.length - 1) {
					rightTimeTargetTimelineData = path[i];
				}
				else {
					deltaTime = path[i + 2].time - path[i].time;
					if(deltaTime > 0.0001) {
						let t = bezierCurveFindClosestT(path[i], path[i + 1], path[i + 2], (right.time - path[i].time) / deltaTime);
						rightTimeTargetTimelineData = calculateBezierCurve(path[i], path[i + 1], path[i + 2], t);
					}
					else {
						rightTimeTargetTimelineData = path[i];
					}
				}
			}
			v2.x = right.x - rightTimeTargetTimelineData.x;
			v2.y = right.y - rightTimeTargetTimelineData.y;

			for(let i = 0;  i < path.length;  i++) {
				let p = path[i];
				if(i%2 == 0) {
					if(right != null) {
						let k = (p.time - left.time)/(right.time - left.time);
						p.x += v2.x*k;
						p.y += v2.y*k;
					}
				} else {
					if(right != null) {
						let k1 = (path[i-1].time - left.time)/(right.time - left.time);
						let k2 = (path[i+1].time - left.time)/(right.time - left.time);
						let k = (k1 + k2) / 2;
						p.x += v2.x*k;
						p.y += v2.y*k;
					}
				}
			}
		}
		cutBezierPathToMatchTime(path, fromTimeCurrent, toTimeCurrent);
		return path;
	}

	function getBezierPoints(timeline, fromTime, toTime) {
		let indexFromTime = findClosestNextFrameIndex(timeline, fromTime) - 1;
		let indexNextToTime = findClosestNextFrameIndex(timeline, toTime);
		let res = [];
		for(let i = indexFromTime;  i < indexNextToTime;  i++) {
			let left = timeline[i];
			let right = ((i+1 < timeline.length) ? timeline[i+1] : null);
			let fromTimeCurrent = Math.max(fromTime, left.time);
			let toTimeCurrent = (right != null) ? Math.min(toTime, right.time) : (toTime);
			let fromK = (right != null) ? (fromTimeCurrent - left.time)/(right.time - left.time) : 0;
			let toK = (right != null) ? (toTimeCurrent - left.time)/(right.time - left.time) : 0;
			let calculate = linear;
			let config = null;
			if(left.follow) {
				config = left.follow;
				if(left.follow.type == "offset" || left.follow.type == "mirror")
					calculate = mirror;
				else if(left.follow.type == "follow")
					calculate = follow;
				else if(left.follow.type == "bezier")
					calculate = bezier;
			}
			if(res.length > 0)
				res.length = res.length - 1;
			concat(res, calculate(left, right, fromK, toK, fromTimeCurrent, toTimeCurrent, config));
		}
		return res;
	}
//	var on = false;
//	var onStack = [];

	this.func = function(timeline, fromTakt, toTakt) {
//		onStack.push(on);
//		for(let i = 0 ; i < data.performers.length; i++) {
//			if(data.performers[i].timeline == timeline) {
//				if(data.performers[i].name == "tdg1") {
//					on = true;
//					break;
//				}
//			}
//		}

		let res = getBezierPoints(timeline, fromTakt, toTakt);
//		on = onStack.pop();
		return res;
	};

}();