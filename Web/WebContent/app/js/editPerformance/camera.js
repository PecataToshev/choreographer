var camera = new function() {
	var self = this;
	const zoomStep = 2;
	const minDistange = 8;
	this.target = {x: 0, y: 0, z: 0};
	this.position = {x: 0, y:0, z: -15};
	this.up = [0, -50, -50];
	this.scale = 5;
	this.projectionAngle = 90;
	this.distance = 15;

	this.update = function() {
		self.position.x = self.target.x + self.distance * Math.cos(current.angle.get.x()) * Math.cos(current.angle.get.z());
		self.position.y = self.target.y + self.distance * Math.cos(current.angle.get.x()) * Math.sin(current.angle.get.z());
		self.position.z = -self.target.z - self.distance * Math.sin(current.angle.get.x());
		self.up = (current.angle.get.x() < Math.PI * 0.4) ? [0, 0, -1] : [-Math.cos(current.angle.get.z()), -Math.sin(current.angle.get.z()), 0];
	}

	this.zoomIn = function() {
		self.distance -= zoomStep;
		if(self.distance < minDistange)
			self.distance = minDistange;
		self.update();
	}

	this.zoomOut = function() {
		self.distance += zoomStep;
		self.update();
	}
}();