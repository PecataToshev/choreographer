var webGL = new function() {
	var self = this;
	var isContextLost = true;
	var gl;
	var positionBuffer;
	var colorBuffer;
	var lightBuffer;
	var shaderProgram;
	var aVertexPosition;
	var aVertexNormal;
	var aVertexColor;
	var uColor;
	var uModelViewMatrix;
	var uProjectionMatrix;
	var uLightVector3;
	var uniformMatrixProjMatrix;
	var uniformMatrixModelMatrix;
	const nearPlane = 5;
	const farPlane = 120;
	var personModels = [
		new personModelClass(personGeometryMale), // male
		new personModelClass(personGeometryFemale), // female
		new personModelClass(personGeometryOther) // other
	];
	var stageModel = new personModelClass(geometryStage);
	const personScale = 0.1;
	const colors = [
		[(0/255), (255/255), (0/255)], // selected
		[(21/255), (101/255), (192/255)], //male
		[(240/255), (98/255), (146/255)], //female
		[(255/255), (98/255), (0/255)] // other
	];

	this.getUniformMatrixProjMatrix = function() { return uniformMatrixProjMatrix; }
	this.getUniformMatrixModelMatrix = function() { return uniformMatrixModelMatrix; }

	function initShaderProgram(gl, vsSource, fsSource) {
		const vertexShader = loadShader(gl, gl.VERTEX_SHADER, vsSource);
		const fragmentShader = loadShader(gl, gl.FRAGMENT_SHADER, fsSource);

		// Create the shader program
		const shaderProgram = gl.createProgram();
		gl.attachShader(shaderProgram, vertexShader);
		gl.attachShader(shaderProgram, fragmentShader);
		gl.linkProgram(shaderProgram);

		// If creating the shader program failed, alert
		if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
			alert('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderProgram));
			return null;
		}

		return shaderProgram;
	};

	function loadShader(gl, type, source) {
		const shader = gl.createShader(type);

		// Send the source to the shader object
		gl.shaderSource(shader, source);

		// Compile the shader program
		gl.compileShader(shader);

		// See if it compiled successfully
		if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
			alert('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(shader));
			gl.deleteShader(shader);
			return null;
		}

		return shader;
	};

	this.init = function() {
		isContextLost = false;

		gl = StageWebGL.getContext("webgl");
		if(!gl) {
			alert("WebGL required");
			throw "WebGL required";
		}

		shaderProgram = initShaderProgram(gl, vertexShader.innerHTML, fragmentShader.innerHTML);
		positionBuffer = gl.createBuffer();
		colorBuffer = gl.createBuffer();
		lightBuffer = gl.createBuffer();

		gl.enable(gl.DEPTH_TEST);
		gl.disable(gl.CULL_FACE);
		gl.depthFunc(gl.LEQUAL);

		aVertexPosition = gl.getAttribLocation(shaderProgram, 'aVertexPosition');
		aVertexNormal   = gl.getAttribLocation(shaderProgram, 'aVertexNormal');

		uColor = gl.getUniformLocation(shaderProgram, 'uColor');
		uProjectionMatrix = gl.getUniformLocation(shaderProgram, 'uProjectionMatrix');
		uModelViewMatrix  = gl.getUniformLocation(shaderProgram, 'uModelViewMatrix');
		uLightVector3     = gl.getUniformLocation(shaderProgram, 'uLigthVector');

		stageModel.init(gl);
		for(var i = 0;  i < personModels.length;  i++)
			personModels[i].init(gl);
	};

	function mul(point, matrix) {
		return {
			x: matrix[0] * point.x + matrix[4] * point.y + matrix[8]  * point.z + matrix[12],
			y: matrix[1] * point.x + matrix[5] * point.y + matrix[9]  * point.z + matrix[13],
			z: matrix[2] * point.x + matrix[6] * point.y + matrix[10] * point.z + matrix[14],
			w: matrix[3] * point.x + matrix[7] * point.y + matrix[11] * point.z + matrix[15],
		}
	}

	function JSONPointToArr(arr, point) {
		arr.push(point.x, point.y, point.z);
	}

	this.draw = function(takt) {
		if(isContextLost)
			self.init();

		canvasParameters.origin.x = StageWebGL.width/2;
		canvasParameters.origin.y = StageWebGL.height/2;
		var z = math.vNormalize([
			camera.target.x - camera.position.x,
			camera.target.y - camera.position.y,
			camera.target.z - camera.position.z
		]);

		var x = math.vNormalize(math.cross(z, camera.up));
		var y = math.vNormalize(math.cross(z, x));

		matrix = [
			[x[0], y[0], z[0], camera.position.x],
			[x[1], y[1], z[1], camera.position.y],
			[x[2], y[2], z[2], camera.position.z],
			[0, 0, 0, 1]
		];
		matrixInverted = math.inv(matrix);
		var matrixScale = [
			[camera.scale, 0, 0, 0],
			[0, camera.scale, 0, 0],
			[0, 0, camera.scale, 0],
			[0, 0, 0, 1]
		];

		matrixInverted = math.multiply(matrixScale, matrixInverted);
		matrix = math.inv(matrixInverted);

		gl.viewport(0, 0, StageWebGL.width, StageWebGL.height);
		gl.clearColor((0/255), (148/255), (134/255), 1.0);
		gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

		gl.useProgram(shaderProgram);

		projectionDistance = (nearPlane + farPlane) / 0.3;

		var c = 1 / projectionDistance;
		var a33 =  c*(farPlane + nearPlane)/(farPlane - nearPlane);
		uniformMatrixProjMatrix = [
			[2/StageWebGL.width, 0, 0, 0],
			[0, -2/StageWebGL.height, 0, 0],
			[0, 0, a33, c/* d */],
			[0, 0, -nearPlane*(c+a33), 0]
		];
		uniformMatrixModelMatrix = [
			[matrixInverted[0][0], matrixInverted[1][0], matrixInverted[2][0], matrixInverted[3][0]],
			[matrixInverted[0][1], matrixInverted[1][1], matrixInverted[2][1], matrixInverted[3][1]],
			[matrixInverted[0][2], matrixInverted[1][2], matrixInverted[2][2], matrixInverted[3][2]],
			[matrixInverted[0][3], matrixInverted[1][3], matrixInverted[2][3], matrixInverted[3][3]]
		];
		var uniformMatrixProj = matrix2DToArray1D(uniformMatrixProjMatrix);
		var uniformMatrixModel = matrix2DToArray1D(uniformMatrixModelMatrix);

		/* draw scene */
		gl.uniformMatrix4fv(uProjectionMatrix, false, new Float32Array(uniformMatrixProj));
		gl.uniformMatrix4fv(uModelViewMatrix, false, new Float32Array(uniformMatrixModel));
		gl.uniform3fv(uColor, new Float32Array([(239/255), (235/255), (233/255)])); // scene color
		gl.uniform3fv(uLightVector3, new Float32Array([-1.0, -1.0, 1.0])); // scene light vector
		stageModel.render(gl, aVertexPosition, aVertexNormal);

		/* draw performers */
		gl.uniform3fv(uLightVector3, new Float32Array([-1.0, 1.0, -1.0])); // persons light vector
		for(i = 0;  i < data.performers.length; i++){
			let performer = data.performers[i];
			gl.uniform3fv(uColor,
					new Float32Array(colors[(selectedPerformer == performer) ? 0 : performer.gender])
			);
			var currentPersonData = getData(performer.timeline, takt);
			var matrixTransform = [
				[personScale, 0, 0, 0],
				[0, personScale, 0, 0],
				[0, 0, personScale, 0],
				[currentPersonData.x, currentPersonData.y, 0, 1]
			];
			gl.uniformMatrix4fv(uModelViewMatrix, false, new Float32Array(matrix2DToArray1D(math.multiply(matrixTransform, uniformMatrixModelMatrix))));
			personModels[performer.gender-1].render(gl, aVertexPosition, aVertexNormal);

		}
		var t1 = (new Date()).getTime();
		for(i = 0;  i < data.performers.length; i++){
			let performer = data.performers[i];
			var currentPersonData = getData(performer.timeline, takt);

		}
		var t2 = (new Date()).getTime();
		window.gdt = t2 - t1;
	};

	this.onContextLost = function(event) {
		isContextLost = true;
	};
}();