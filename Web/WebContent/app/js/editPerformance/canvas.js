var canvas = new function() {
	var self = this;
	var hasBeenInitialized = false;
	var ctx;
	this.snapLines = [];

	this.draw = function(takt) {
		if(!hasBeenInitialized) {
			ctx = StageCanvas.getContext('2d');
			hasBeenInitialized = true;
		}

		ctx.clearRect(0, 0, StageCanvas.width, StageCanvas.height);
		if(mouse.stage.isMoveStarted()) {
			for(let i = 0;  i < self.snapLines.length;  i++) {
				let line = self.snapLines[i];
				let point1 = transformAndProject({x: line.x, y: line.y, z: 0});
				let point2 = transformAndProject({
					x: ((line.direction == 'x') ? (line.x + 1) : line.x),
					y: ((line.direction == 'y') ? (line.y + 1) : line.y),
					z: 0
				});
				let vx = point2.x - point1.x;
				let vy = point2.y - point1.y;
				let k = [];
				if(Math.abs(vx) > 0.00001) {
					k.push((/* Xmin */0 - point1.x) / vx);
					k.push((/* Xmax */StageCanvas.width - point1.x) / vx);
				}
				if(Math.abs(vy) > 0.00001) {
					k.push((/* Ymin */0 - point1.y) / vy);
					k.push((/* Ymax */StageCanvas.height - point1.y) / vy);
				}
				if(k.length > 0) {
					k.sort();
					ctx.beginPath();
					ctx.moveTo(point1.x + k[0]*vx, point1.y + k[0]*vy);
					ctx.lineTo(point1.x + k[k.length - 1]*vx, point1.y + k[k.length - 1]*vy);
					ctx.strokeStyle = line.color;
					ctx.stroke();
				}
			}
		}

		ctx.strokeStyle = "#000000";
		if(selectedPerformer != null) {
			let performer = selectedPerformer;
			for(let t = 0;  t < performer.timeline.length;  t++) {
				let timeFrame = performer.timeline[t];
				if(timeFrame.follow && timeFrame.follow.type == 'bezier') {
					let nextTimeFrame = performer.timeline[t + 1];
					if(!(timeFrame.time <= currentTime && nextTimeFrame.time >= currentTime))
						continue;
					let point1 = transformAndProject({x: timeFrame.x, y: timeFrame.y, z: timeFrame.dz});
					let point2 = transformAndProject({x: timeFrame.follow.point.x, y: timeFrame.follow.point.y, z: timeFrame.follow.point.dz});
					let point3 = transformAndProject({x: nextTimeFrame.x, y: nextTimeFrame.y, z: nextTimeFrame.dz});
	
					// bezierCurve
					ctx.strokeStyle = "#FBC02D"; /* yellow darken-2 */
					ctx.beginPath();
					ctx.moveTo(point1.x, point1.y);
					ctx.quadraticCurveTo(point2.x, point2.y, point3.x, point3.y);
					ctx.stroke();
	
					// point
					ctx.strokeStyle = "#000000";
					ctx.beginPath();
					ctx.arc(point2.x, point2.y, 7, 0, 2*Math.PI);
					if(t == selectedBezierCurvePointTimelineIndex) {
						ctx.fillStyle = "#FBC02D"; /* yellow darken-2 */
						ctx.fill();
					}
					ctx.stroke();
				}
			}
		}
	};

	/* REMOVE FROM PRODUCTION */
	this.getCTX = function() { return ctx; };
}();





var redraw = new function() {
	var self = this;
	var executed = true;

	this.inTakt = function(takt) {
		if(data == null || !executed)
			return;
		executed = false;
		window.requestAnimationFrame(function() {
			executed = true;
			var t1 = (new Date()).getTime();
			webGL.draw(takt);
			var t2 = (new Date()).getTime();
			canvas.draw(takt);
			var t3 = (new Date()).getTime();
			window.wt = t2 - t1;
			window.ct = t3 - t2;
		});
	};

	this.now = function() {
		self.inTakt(currentTime);
	}
}();