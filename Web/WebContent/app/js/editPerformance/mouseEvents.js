var mouse = new function() {
	var selfMain = this;

	this.select = function(performer, takt) {
		if(!performer || (takt == undefined))
			return;
		takt = Math.round(takt);
		var selector = ".timelineDiv[performerID=\"" + performer.id + "\"] .frame[takt=\"" + takt + "\"]";
		var currentNodeSelectedFrame = $(selector);
		if(!currentNodeSelectedFrame.length)
			return;

		if(selectedFrame != currentNodeSelectedFrame){
			if(selectedFrame != null){
				selectedFrame.removeAttr("selected");
				selectedFrame.parent().parent().parent().removeClass("active");
			}

			selectedFrame = currentNodeSelectedFrame;
			selectedPerformer = performer;

			selectedFrame.attr("selected", "true");
			selectedFrame.parent().parent().parent().addClass("active");

			audio.player.setTimeFromTakt(takt);
			redraw.now();
		}
	}

	this.timeline = new function() {
		var self = this;

		this.close = function() {
			if(keyFrameDataShow.style.display != 'none')
				keyFrameDataShow.style.display = 'none';
		}

		this.remove = function(e) {
			actionButtons.performer.removeKeyFrame(e, false);
			self.close();
			references.update();
			dataHistory.store();
		}

		this.update = function() {
			var tmp = keyFrameDataShow;
			let performer = tmp.performer;
			performer.name = tmp.querySelector("input[name='name']").value;
			let frame = tmp.timeFrame;
			frame.x = parseFloat(tmp.querySelector("input[name='x']").value);
			frame.y = parseFloat(tmp.querySelector("input[name='y']").value);
			if(tmp.querySelector("input[name='removeFollow']").checked)
				delete frame.follow;
			self.close();
			references.update();
			dataHistory.store();
		}

		this.down = function(e, target) {
			if(e.which == 3) {
				var tmp = keyFrameDataShow;
				let performer = getPerformerByID(target.getAttribute('performerID'));
				let takt = target.getAttribute('takt');
				let frame = getKeyFrame(performer.timeline, takt);
				tmp.querySelector("#idHolder").innerHTML = performer.id + ".";
				tmp.querySelector("input[name='name']").value = performer.name;
				tmp.querySelector("#takt").innerHTML = parseInt(takt) + 1;
				tmp.querySelector("input[name='x']").value = frame.x;
				tmp.querySelector("input[name='y']").value = frame.y;
				tmp.querySelector("input[name='removeFollow']").checked = false;
				tmp.timeFrame = frame;
				tmp.performer = performer;
				let followed = '-';
				if(frame.follow) {
					followed = '[' + frame.follow.type + (frame.follow.mirrorX ? " X" : "") + (frame.follow.mirrorY ? " Y" : "") + '] ';
					if(frame.follow.id) {
						let following = getPerformerByID(frame.follow.id);
						followed += following.id + ". " + following.name;
					}
					followed += "<span class='red-text hand' title='Премахни follow'"
						+ " onclick=\"keyFrameDataShow.querySelector('input[name=removeFollow]').checked = true; this.parentElement.innerHTML = '';\""
						+ " style='position: absolute; right: 3px;'>X</span>"
				}
				tmp.querySelector("#follow").innerHTML = followed;
				tmp.style.left = (target.getBoundingClientRect().left) + "px";
				tmp.style.top  = (e.clientY - e.layerY) + "px";
				tmp.style.display = 'block';
			} else {
				self.close();
			}
		};

		this.move = function(node, e) {
			var rect = node.getBoundingClientRect();
			var x = e.clientX - rect.left;
			var y = e.clientY - rect.top;
			node = $(node);

			if(x < 0)
				x = 0;
			x = Math.floor(x/7);
			currentTime = x;
			selfMain.select(getPerformerByID(node.find(".frame[takt='" + x + "']").attr("performerID")), x);
		};
	}();

	this.stage = new function() {
		var target;
		var clickButtonOverStage = 0;
		var hasSthChanched = false;
		var rect;
		var x;
		var y;
		var z;

		function clickInit(e) {
			rect = StageWebGL.getBoundingClientRect();
			x = e.clientX - rect.left - canvasParameters.origin.x
			y = e.clientY - rect.top - canvasParameters.origin.y;
			var clickVector = [[x], [y], [projectionDistance], [0]];
			clickVector = math.multiply(matrix, clickVector);
			x = clickVector[0][0];
			y = clickVector[1][0];
			z = clickVector[2][0];
		}

		function calculateMouseStagePosition(e, matrix, cameraPosition) {
			clickInit(e);

			var plain = [0, 0, 1, 0];
			var rayKdown = plain[0]*x + plain[1]*y + plain[2]*z;
			if(Math.abs(rayKdown) < 1/1000)
				return null;

			var rayK = plain[0]*cameraPosition.x + plain[1]*cameraPosition.y + plain[2]*cameraPosition.z + plain[3];
			rayK = -rayK/rayKdown;

			if(rayK < 0)
				return null;
			x = cameraPosition.x + rayK*x;
			y = cameraPosition.y + rayK*y;

			return [x, y, z];
		}

		var view = new function() {
			var self = this;
			var cameraTargetClone;
			var clickPoint;
			var clickMatrix;
			var clickCameraPosition;
			var clickAngleX;
			var clickAngleZ;
			const stageMoveCoef = 0.01;

			this.down = function(e) {
				if(clickButtonOverStage == 1){
					clickMatrix = cloneJSON(matrix);
					clickCameraPosition = cloneJSON(camera.position);
					cameraTargetClone = cloneJSON(camera.target);
					clickPoint = calculateMouseStagePosition(e, clickMatrix, clickCameraPosition);
					return true;
				} else if(clickButtonOverStage == 3){
					clickAngleX = current.angle.get.x();
					clickAngleZ = current.angle.get.z();
					var rect = StageWebGL.getBoundingClientRect();
					clickPoint = [e.clientX - rect.left, e.clientY - rect.top];
					return true;
				}
				return false;
			};

			this.move = function(e) {
				if(!keyboardHandler.checkKeyPressed(keyboardHandler.map.control))
					return;

				if(clickButtonOverStage == 1){
					var currentMousePosition = calculateMouseStagePosition(e, clickMatrix, clickCameraPosition);
					camera.target.x = cameraTargetClone.x - (currentMousePosition[0] - clickPoint[0]);
					camera.target.y = cameraTargetClone.y - (currentMousePosition[1] - clickPoint[1]);
					camera.update();
					redraw.now();
				} else if(clickButtonOverStage == 3){
					var rect = StageWebGL.getBoundingClientRect();
					current.angle.set.z(clickAngleZ + (e.clientX - rect.left - clickPoint[0])*stageMoveCoef);
					current.angle.set.x(clickAngleX + (e.clientY - rect.top  - clickPoint[1])*stageMoveCoef);
					camera.update();
					redraw.now();

					return false;
				}
			};

			this.up = function(e) {};

			this.wheel = function(e) {
				if(keyboardHandler.checkKeyPressed(keyboardHandler.map.control)) {
					if (e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0) // scroll up
						camera.zoomIn();
					else // scroll down
						camera.zoomOut();
					redraw.now();
					return false;
				}
			}
		}();

		var performer = new function() {
			var isSelectedPerformer = false;
			const minimumMoveDistance = 6;/* px */
			const gridLineNumber = 3;
			const gridStep = 0.5; /* meters */
			const snapPerformer = 0.25; /* meters */
			const selectPersonRadius = 0.75; /* meters */
			const bezierHelperPointRadius = 0.3; /* meters */
			var moveStarted = false;
			var downMouseCoordinates = {x: 0, y: 0};
			var personsPositionNowX = [];
			var personsPositionNowY = [];

			this.isMoveStarted = function() { return moveStarted; };

			/* returns index in arr */function findClosest(arr, pos) {
				if(arr.length == 0)
					return null;
				let l = 0;
				let r = arr.length - 1;

				while(l < r - 1) {
					let mid = Math.floor((l+r)/2);
					if(arr[mid] < pos)
						l = mid;
					else if(arr[mid] > pos)
						r = mid;
					else
						return mid;
				}
				let index = r;
				let distance = Math.abs(arr[r] - pos);
				let lDist = Math.abs(arr[l] - pos);
				if(lDist < distance) {
					distance = lDist;
					index = l;
				}
				if(distance < snapPerformer)
					return index;
				return null;
			}

			function movePoint(e) {
				let rayK = calculateMouseStagePosition(e, matrix, camera.position);
				if(rayK == null)
					return;
				hasSthChanched = true;
				let timeframe = selectedPerformer.timeline[selectedBezierCurvePointTimelineIndex];
				let point = timeframe.follow.point;
				point.x = rayK[0];
				point.y = rayK[1];
			}

			function movePerformer(e) {
				if(!moveStarted) {
					if((e.clientX - downMouseCoordinates.x) * (e.clientX - downMouseCoordinates.x)
						+ (e.clientY - downMouseCoordinates.y) * (e.clientY - downMouseCoordinates.y)
						> minimumMoveDistance*minimumMoveDistance)
						moveStarted = true;
					else
						return;
				}
				if(!isSelectedPerformer)
					return;
				var rayK = calculateMouseStagePosition(e, matrix, camera.position);
				if(rayK == null)
					return;

				hasSthChanched = true;
				let lines = canvas.snapLines;
				lines.length = 0;
				let newKeyFrame = createKeyFrame(selectedPerformer.timeline, currentTime);
				let newX = rayK[0];
				let newY = rayK[1];

				if(keyboardHandler.checkKeyPressed(keyboardHandler.map.shift)) {
					if(keyboardHandler.checkKeyPressed(keyboardHandler.map.s)) { // snap to performers
						let color = "#FBC02D"; /* yellow darken-2 */
						newKeyFrame.x = newX;
						newKeyFrame.y = newY;
						let tmpXInd = findClosest(personsPositionNowX, newX);
						if(tmpXInd !== null) {
							newKeyFrame.x = personsPositionNowX[tmpXInd];
							lines.push({
								color: color,
								x: newKeyFrame.x,
								y: newKeyFrame.y,
								direction: 'y'
							});
						}

						let tmpYInd = findClosest(personsPositionNowY, newY);
						if(tmpYInd !== null) {
							newKeyFrame.y = personsPositionNowY[tmpYInd];
							lines.push({
								color: color,
								x: newKeyFrame.x,
								y: newKeyFrame.y,
								direction: 'x'
							});
						}
					} else { // snap to grid
						let moveX = true;
						let moveY = true;
						if(keyboardHandler.checkKeyPressed(keyboardHandler.map.x))
							moveY = false;
						if(keyboardHandler.checkKeyPressed(keyboardHandler.map.y))
							moveX = false;

						newX = gridStep * Math.round(newX / gridStep);
						newY = gridStep * Math.round(newY / gridStep);
						let color = "#673ab7"; /* deep-purple */
						lines.push({
							color: color,
							x: newKeyFrame.x,
							y: newKeyFrame.y,
							direction: 'x'
						});
						lines.push({
							color: color,
							x: newKeyFrame.x,
							y: newKeyFrame.y,
							direction: 'y'
						});
						if(moveX) {
							newKeyFrame.x = newX;
							for(let i = 1;  i < gridLineNumber;  i++) {
								lines.push({
									color: color,
									x: newKeyFrame.x + i*gridStep,
									y: newKeyFrame.y,
									direction: 'y'
								});
								lines.push({
									color: color,
									x: newKeyFrame.x - i*gridStep,
									y: newKeyFrame.y,
									direction: 'y'
								});
							}
						}
						if(moveY) {
							newKeyFrame.y = newY;
							for(let i = 1;  i < gridLineNumber;  i++) {
								lines.push({
									color: color,
									x: newKeyFrame.x,
									y: newKeyFrame.y + i*gridStep,
									direction: 'x'
								});
								lines.push({
									color: color,
									x: newKeyFrame.x,
									y: newKeyFrame.y - i*gridStep,
									direction: 'x'
								});
							}
						}
					}
				} else {
					newKeyFrame.x = newX;
					newKeyFrame.y = newY;
				}

				return true;
			}

			function checkForPoint(indexInTimeline) {
				let selectedPerformerTimeFrame = selectedPerformer.timeline[indexInTimeline];
				if(!!selectedPerformerTimeFrame && selectedPerformerTimeFrame.follow && selectedPerformerTimeFrame.follow.type == "bezier") {
					let point = selectedPerformerTimeFrame.follow.point;
					var currentDistanceRes = distanceBetweenPointAndRay(
							camera.position,
							{x:x, y:y, z:z},
							{
								x: point.x,
								y: point.y,
								z: point.dz
							}
						);
					if(currentDistanceRes < bezierHelperPointRadius) {
						selectedBezierCurvePointTimelineIndex = indexInTimeline;
						return true;
					}
				}
				return false;
			}

			this.down = function(e) {
				isSelectedPerformer = false;
				clickInit(e);
				moveStarted = false;
				downMouseCoordinates.x = e.clientX;
				downMouseCoordinates.y = e.clientY;
				personsPositionNowX = [];
				personsPositionNowY = [];
				let selectedIndex = -1;
				selectedBezierCurvePointTimelineIndex = null;

				if(selectedPerformer != null) {
					let selectedPerformerFrameIndex = findClosestNextFrameIndex(selectedPerformer.timeline, currentTime) - 1;
					let check = checkForPoint(selectedPerformerFrameIndex);
					if(!check && selectedPerformer.timeline[selectedPerformerFrameIndex].time == currentTime)
						check = checkForPoint(selectedPerformerFrameIndex - 1);
					if(check)
						return true;
				}

				var i;
				var minimumZ, minimumZPerformer = null;
				var minimumDistance = 100000000;
				for(i=0; i < data.performers.length;  i++){
					var currentPerformerPoint = getData(data.performers[i].timeline, currentTime);
					var currentDistanceRes = distanceBetweenPointAndRay(
							camera.position,
							{x:x, y:y, z:z},
							{
								x:currentPerformerPoint.x,
								y:currentPerformerPoint.y,
								z: 0//((current.angle.get.x() == Math.PI / 2) ? 0 : -taktZ(currentTime - Math.floor(currentTime)))
							}
						);

					personsPositionNowX.push(currentPerformerPoint.x);
					personsPositionNowY.push(currentPerformerPoint.y);
					if(currentDistanceRes < selectPersonRadius){
						currentPerformerPoint = transformAndProject({x: currentPerformerPoint.x, y: currentPerformerPoint.y, z: currentPerformerPoint.dz});
						if(!minimumZPerformer || currentDistanceRes < minimunDistance) {// || currentPerformerPoint.z < minimumZ){
							minimumZPerformer = data.performers[i];
							minimumZ = currentPerformerPoint.z;
							minimunDistance = currentDistanceRes;
							selectedIndex = i;
						}
					}
				}

				if(minimumZPerformer) {
					isSelectedPerformer = true;
					selectedPerformer = minimumZPerformer;
					selfMain.select(selectedPerformer, currentTime);
					personsPositionNowX.splice(selectedIndex, 1);
					personsPositionNowY.splice(selectedIndex, 1);
					personsPositionNowX.sort(function(a,b) { return a-b; });
					personsPositionNowY.sort(function(a,b) { return a-b; });
					return true;
				}

				return false;
			};

			this.move = function(e) {
				if(selectedBezierCurvePointTimelineIndex != null)
					movePoint(e);
				else
					movePerformer(e);
				redraw.now();
			}

			this.up = function(e) {
				moveStarted = false;
				selectedBezierCurvePointTimelineIndex = null;
			};

			this.wheel = function(e) {}
		}();

		function selectTarget() {
			if(keyboardHandler.checkKeyPressed(keyboardHandler.map.control)) {
				target = view;
			} else {
				target = performer;
			}
		}

		this.isMoveStarted = function() { return performer.isMoveStarted(); };

		this.down = function(e) {
			selectTarget();
			selfMain.timeline.close();
			hasSthChanched = false;
			clickButtonOverStage = e.which;
			return target.down(e);
		};

		this.move = function(e) {
			preventAndStop(e);
			return target.move(e);
		};

		this.up = function(e) {
			preventAndStop(e);
			clickButtonOverStage = 0;
			if(hasSthChanched) {
				references.update();
				dataHistory.store();
			}
			return target.up(e);
		};

		this.wheel = function(e) {
			preventAndStop(e);
			selectTarget();
			return target.wheel(e);
		};
	}();
}();