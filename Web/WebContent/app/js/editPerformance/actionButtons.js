var actionButtons = new function() {
	var mainSelf = this;

	this.showHideActionButtons = function(event, element) {
		preventAndStop(event);
		const timimg = 750;
		const elementWidth = 73;
		let state = element.getAttribute("state");
		element.setAttribute("state", 1-state);
		if(state == 1){
			// hide
			$(element).find("i").html("fast_rewind");
			$(element).animate(
					{
						right: "0px"
					},
					timimg
				);
			$("#action-side-nav").animate(
					{
						right: "-" + elementWidth + "px"
					},
					timimg
				);
			$("section").animate(
					{
						width: "100%"
					},
					timimg,
					updateCurrentHeight
				);
		} else {
			// show
			$(element).find("i").html("fast_forward");
			$(element).animate(
					{
						right: elementWidth + 4 + "px"
					},
					timimg
				);
			$("#action-side-nav").animate(
					{
						right: "0px"
					},
					timimg
				);
			$("section").animate(
					{
						width: (($("section").width() - elementWidth) + "px")
					},
					timimg,
					updateCurrentHeight
				);
		}
	};

	this.reloadData = function() {

	};

	this.saveData = function() {
		$.ajax({
			/* 
			 * Parameters.Action.ACTION
			 * Parameters.Object.PERFORMANCE
			 * Parameters.ObjectData.CODE 
			 */
			url: "/Choreographer/app/ObjectUpdate?ACTION=PERFORMANCE&CODE=" + getParameter("CODE"),
			method: "POST",
			data: "DATA=" + JSON.stringify(data),
			success: objectUpdateResult, 
			error: errorOnAction
		});
	};

	this.saveSettings = function(e) {
		preventAndStop(e);
		$.ajax({
			/* 
			 * Parameters.Action.ACTION
			 * Parameters.Object.PERFORMANCE_SETTINGS
			 * Parameters.ObjectData.CODE 
			 */
			url: "/Choreographer/app/ObjectUpdate?ACTION=PERFORMANCE_SETTINGS&CODE=" + getParameter("CODE"),
			method: "POST",
			data: $("#performanceSettings").serialize(),
			success: objectUpdateResult, 
			error: errorOnAction
		});

	};

	this.performer = new function() {
		var self = this;
		const bezierCurveNewPointScale = 0.25;

		function hasSelectedPerformerAndFrame() {
			if(selectedPerformer == null){
				Materialize.toast('Не сте избрали човек, на когото да приложите функцията!', 5000);
				return false;
			} else if(currentTime == undefined){
				Materialize.toast('Не сте избрали момент, в който да приложите функцията!', 5000);
				return false;
			}
			return true;
		}

		function hasNextKeyFrame(performer, currentTakt) {
			return performer.timeline[performer.timeline.length - 1].time > currentTakt;
		}

		function initializeModalOffsetPerson(typeShow, type, custom){
			var hide = true;
			if(hasSelectedPerformerAndFrame()) {
				hide = false;
				let tmp = $("#modalOffsetPerson > div.modal-content > div:first > div:first");
				tmp.find("div:first").html("<span class='bolded'>" + selectedPerformer.id + ". " + selectedPerformer.name + "</span> да " + typeShow + ":");
				tmp.find("div:nth-child(2)").html((!custom) ? "" : custom);

				var performers = "";
				for(var i = 0;  i < data.performers.length;  i++){
					if(selectedPerformer == data.performers[i])
						continue;

					performers += '<div class="btn btn-flat offsetPersonSelect waves-effect waves-green" opt="' + data.performers[i].id
						+ '" onclick="$(\'#modalOffsetPerson input[name=personToOffset]\').val($(this).attr(\'opt\'));$(\'#modalOffsetPerson button[type=submit]\').click();">'
						+ data.performers[i].id + '. ' + data.performers[i].name + '</div>';
				}
				$("#modalOffsetPerson > div.modal-content > div > div.performersScrollContainer").html(performers);


				$("#modalOffsetPerson").unbind("submit");
				$("#modalOffsetPerson").submit(function(e){
					preventAndStop(e);
					var res = createKeyFrame(selectedPerformer.timeline, currentTime);
					for(var i = 0;  i < selectedPerformer.timeline.length;  i++){
						let tmp = selectedPerformer.timeline[i];
						if(tmp.time == res.time){
							tmp.follow = {
								type: type,
								id: parseInt($("#modalOffsetPerson input[name='personToOffset']").val())
							};
							if(type == "mirror") {
								tmp.follow.mirrorX = $("#modalOffsetPerson input[name=mirrorX]")[0].checked;
								tmp.follow.mirrorY = $("#modalOffsetPerson input[name=mirrorY]")[0].checked;
							}
							dataHistory.store();
							updateAll();
							return;
						}
					}
				});
				return true;
			}

			setTimeout(function(){
				if(hide)
					$("#modalOffsetPerson").modal('close');
			}, 50);
		}

		this.follow = function() {
			initializeModalOffsetPerson("следва", "follow");
		};

		this.mirror = function() {
			initializeModalOffsetPerson("се движи огледално на", "mirror", $("#mirrorDirection").html());
		}

		this.keepOffset = function() {
			initializeModalOffsetPerson("пази растояние от", "offset");
		};

		this.bezierCurve = function() {
			if(!hasSelectedPerformerAndFrame())
				return;
			if(!hasNextKeyFrame(selectedPerformer, currentTime)) {
				Materialize.toast('Задължително е наличието на следващ ключов момент!', 5000);
				return;
			}
			let timeline = selectedPerformer.timeline;
			let r = findClosestNextFrameIndex(timeline, currentTime);
			let has = false;
			for(let i = r;  i >= 0;  i--) {
				if(timeline[i].follow && timeline[i].follow.type == 'bezier'){
					has = true;
					break;
				}
			}
			let currentFrame = createKeyFrame(timeline, currentTime);
			let nextFrameTimelineIndex = findClosestNextFrameIndex(timeline, currentTime);
			let nextFrame = timeline[nextFrameTimelineIndex];
			let point = {};
			if(has) {
				let prevFrame = timeline[nextFrameTimelineIndex - 2];
				let res = getPath.separateBezierCurve(prevFrame, currentFrame, nextFrame);
				prevFrame.follow.point.x = res[0].x;
				prevFrame.follow.point.y = res[0].y;
				point = res[1]; 
			} else {
				let vx = nextFrame.x - currentFrame.x;
				let vy = nextFrame.y - currentFrame.y;
				let newV = {x: -vy, y: vx};
				point = {x: (nextFrame.x + currentFrame.x)/2, y: (nextFrame.y + currentFrame.y)/2};
				point.x += newV.x * bezierCurveNewPointScale;
				point.y += newV.y * bezierCurveNewPointScale;
			}
			point.dz = 0;
			currentFrame.follow = {
				type: 'bezier',
				point: point
			}
			dataHistory.store();
			references.update();
		};





		function removeKeyFrame(timeline, time){
			/**
			 *  1 - success
			 *  0 - no such frame
			 * -1 - reset first
			 * -2 - next to bezier
			 */
			if(timeline.length == 0)
				return false;

			let l = 0;
			let r = timeline.length - 1;
			while(l <= r){
				mid = Math.floor((l+r)/2);

				if(timeline[mid].time < time)
					l = mid + 1;
				else if(timeline[mid].time > time)
					r = mid - 1;
				else {
					if(mid == 0){
						timeline[mid] = {
								"x": 0,
								"y": 0,
								"dz": -1,
								"phi": 50,
								"time": 0
							};
						return -1;
					}
					let canSplice = 1;
					let prev = timeline[mid - 1];
					let curr = timeline[mid];
					let bezier = "bezier";
					if(prev.follow && prev.follow.type == bezier) {
						if((curr.follow && curr.follow.type == bezier) || (mid < timeline.length - 1))
							canSplice = 1;
						else
							canSplice = -2;
					}
					if(canSplice == 1) {
						timeline.splice(mid, 1);
						return 1;
					}
					return canSplice;
				}
			}
			return 0;
		}

		this.removeKeyFrame = function(e, needRedraw) {
			preventAndStop(e);
			switch(removeKeyFrame(selectedPerformer.timeline, currentTime)){
			case 1:
				Materialize.toast('Моментът беше успешно изтрит!', 2000, "rounded");
				generateTimelineData();
				break;
			case -1:
				Materialize.toast('Пърият ключов момент беше изчистен!', 4000, "rounded");
				break;
			case -2:
				Materialize.toast('Трябва да има поне един ключов елемен след следването на пътя!', 4000, "rounded");
				break;
			default:
				Materialize.toast('Не сте избрали валиден ключов момент!', 3000, "rounded");
				break;
			}

			if(needRedraw)
				redraw.now();
		};





		this.createNew = function(e) {
			preventAndStop(e);
			if(!data.lastPerformerID)
				data.lastPerformerID = 0;

			data.performers.push(
				{
					gender: parseInt($("#modalNewPerson").find("input[name='type']:checked").val()),
					id: data.lastPerformerID,
					name: $("#modalNewPerson").find("input[name='name']").val(),
					timeline: [
						{
							x: 0,
							y: 0,
							dz: -1,
							phi: 50,
							time: 0
						}
					]
				}
			);
			data.lastPerformerID++;
			$("#modalNewPerson")[0].reset();
			$("#modalNewPerson").modal('close');
			references.update();
			dataHistory.store();
		};

		this.remove = function(e) {
			preventAndStop(e);
			let id = selectedPerformer.id;
			let arrayID = -1;
			let lenPerf = data.performers.length;
			var i = 0;
			for(i = 0;  i < lenPerf;  i++){
				if(data.performers[i].id == id){
					arrayID = i;
					break;
				}
			}
			data.performers.splice(arrayID, 1);
			lenPerf = data.performers.length;
			for(i = 0;  i < lenPerf;  i++){
				var performerTimeline = data.performers[i].timeline;
				for(var j = 0;  j < performerTimeline.length;  j++){
					if(performerTimeline[j].follow && performerTimeline[j].follow.id == id){
						delete performerTimeline[j]["follow"];
					}
				}
			}
			selectedPerformer = null;
			references.update();
			dataHistory.store();
			Materialize.toast('Изпълнителят беше успешно изтрит!', 2000, "rounded");
		};
	}();
}();