function bezierIntegral(p0, p1, p2, x) {
	const min = 0.000001;
	let k1x = p0.x - 2*p1.x + p2.x;
	let k1y = p0.y - 2*p1.y + p2.y;
	let k2x = p1.x - p0.x;
	let k2y = p1.y - p0.y;
	let a = 4*(k1x*k1x + k1y*k1y);
	let b = 8*(k1x*k2x + k1y*k2y);
	let c = 4*(k2x*k2x + k2y*k2y);
	let t0 = x*(a*x + b) + c;
	if(t0 < -min || a < min) {
		let dx = (p2.x - p0.x)*x;
		let dy = (p2.y - p0.y)*x;
		return Math.sqrt(dx*dx + dy*dy);
	}
	let t1 = 2*Math.sqrt(a)*Math.sqrt(t0) + 2*a*x + b;
	if(t1 < min) {
		let dx = (p2.x - p0.x)*x;
		let dy = (p2.y - p0.y)*x;
		return Math.sqrt(dx*dx + dy*dy);
	}
	return ((2*a*x + b)*Math.sqrt(t0))/(4*a) - ((b*b-4*a*c)*Math.log(t1))/(8*Math.pow(a, 3/2));
}

function calculateBezierCurve(p0, p1, p2, t) {
	return {
			x:p0.x * (1-t) * (1-t) + 2 * p1.x * t * (1-t) + p2.x * t * t,
			y:p0.y * (1-t) * (1-t) + 2 * p1.y * t * (1-t) + p2.y * t * t
		};
}

function bezierCurveFindClosestT(p0, p1, p2, k) {
	const minimum = 0.003;
	let integral0 = bezierIntegral(p0, p1, p2, 0);
	let integral1 = bezierIntegral(p0, p1, p2, 1);
	let targetDistance = (integral1 - integral0) * k;
	let l = 0;
	let r = 1;
	let tmp, mid;
	while(r - l > minimum) {
		mid = (l+r)/2;
		tmp = bezierIntegral(p0, p1, p2, mid) - integral0;
		if(tmp > targetDistance)
			r = mid;
		else
			l = mid;
	}
	return l;
}