$(document).ready(function(){
	Materialize.updateTextFields();
	$("#rhythm").submit(rhythm.save);

	rhythm.initialize();
});





function updateCurrentHeight() {
	rhythm.calculateAndRedraw();
}

var rhythm = new function(){
	var self = this;
	var numberOfBeats = 0;
	const numberOfAudioFiles = 10;
	const powerNormalization = 2.5;
	var data = [];


	this.parseData = function(_data){
		$(".beat-cont-handler-drag:not(#beat-cont)").remove();
		data = _data;
		numberOfBeats = data.length;

		var lineOffsetL = $("#line").offset().left;
		var normalizationWidth = 100/$("#line").width();
		var lineHeight = $("#line").height();
		var normalizationHeight = 100/lineHeight;

		for(var i = 0;  i < numberOfBeats;  i++){
			var newBeat = $("#beat-cont").clone().removeAttr("id");
			newBeat.appendTo("#line");
			newBeat.css({
				position: "absolute",
				left: (data[i].time/normalizationWidth)
			});
			newBeat.find(".beat-cont-data").text(data[i].time);
			newBeat.find(".beat-cont").css("top", lineHeight-data[i].power/normalizationHeight);
		}

		self.update();
	}

	this.initialize = function() {
		// create audio players
		var audioTagID = "beatPlayMusic";
		for(let i = 1;  i < numberOfAudioFiles;  i++)
			$("#" + audioTagID).clone().attr("id", audioTagID + i).appendTo("#ruler-container");

		$("#" + audioTagID).attr("id", audioTagID + "0");

		self.reload();
	};





	var setBeatPosition = function(elementBeat, position) {
		elementBeat.find(".beat-cont-data").text(((position / $("#line").width())*100).toFixed(0));
	}





	this.add = function() {
		if(numberOfBeats >= (($("#line").width() - 19) / /* column width */ 20))
			return;

		var newBeat = $("#beat-cont").clone().removeAttr("id");
		newBeat.appendTo("#line");
		setBeatPosition(newBeat, newBeat.position().left);
		numberOfBeats++;
		self.update();
		return newBeat;
	}

	this.remove = function(element) {
		$(element).parent().parent().remove();
		numberOfBeats--;
		self.update();
	}

	this.update = function() {
		$("#line, .beat-cont-data").disableSelection();

		$(".beat-cont-handler-drag").draggable({
			axis: "x",
			containment: "parent",
			scroll: false,
			drag: function(event, ui) {
				setBeatPosition(ui.helper, (ui.offset.left - $("#line").offset().left));
			},
			start: function(event, ui) {
				ui.helper.find(".grab").removeClass("grab").addClass("grabbing");
			},
			stop: function(event, ui) {
				ui.helper.find(".grabbing").removeClass("grabbing").addClass("grab");
				self.calculateAndRedraw();
			}
		});

		$(".beat-cont")
		.resizable()
		.resizable({
			handles: 'n',
			minHeight: 35,
			maxHeight: 150,
			stop: function(event, ui) {
				self.calculateAndRedraw();
			}
		});
	};





	this.calculate = function() {
		var arr = [];
		var normalizationHeight = 100/$("#line").height();
		$(".beat-cont-handler-drag:not(#beat-cont)").each(function(){
			var power = Math.round($(this).find(".beat-cont").height() * normalizationHeight);
			power = Math.round(power/powerNormalization)*powerNormalization;
			if(power > 100)
				power = 100;

			arr.push({
				time: parseInt($(this).find(".beat-cont-data").text()),
				power: power
			});
		});
		arr.sort(function(a, b) {
			return a.time - b.time;
		});
		return arr;
	}

	this.calculateAndRedraw = function() {
		self.parseData(self.calculate());
	}

	this.reload = function(e) {
		if(e)
			e.preventDefault();

		numberOfBeats = 0;
		data = [];

		$.ajax({
			/* 
			 * Parameters.Action.ACTION
			 * Parameters.Object.RHYTHM
			 * Parameters.ObjectData.CODE 
			 */
			url: "/Choreographer/app/ObjectDataGet?ACTION=RHYTHM&CODE=" + getParameter("CODE"),
			type: "GET",
			success: function(result){
				if(result.substr(0, 3) == "OK:"){
					self.parseData(JSON.parse(result.substr(3)));
				} else if(result == "FORBIDDEN") {
					Materialize.toast('Нямате достъп до този ритъм', 10000);
				} else if(result == "MISSING ID") {
					Materialize.toast('Не е намерен идентификацонният номер на ритъма! '
							+ 'Моля презаредете страницата и опитайте отново!',
							10000
						);
				} else {
					errorOnAction(result);
				}
			},
			error: errorOnAction
		});
	}

	this.save = function(e) {
		e.preventDefault();

		if(numberOfBeats == 0){
			Materialize.toast('Няма въведени тактове!', 10000);
			return false;
		}

		$("#DATA").val(JSON.stringify(self.calculate()));

		$.ajax({
			/* 
			 * Parameters.Action.ACTION
			 * Parameters.Object.RHYTHM
			 * Parameters.ObjectData.CODE 
			 */
			url: "/Choreographer/app/ObjectUpdate?ACTION=RHYTHM&CODE=" + getParameter("CODE"),
			type: "POST",
			data: $("#rhythm").serialize(),
			success: objectUpdateResult,
			error: errorOnAction
		});
	};





	this.player = new function() {
		var isRhythmPlaying = false;
		var rhythmPlayingStartTime;
		var rhythmPlayingTimePerBar;
		var rhythmPlayingInterval;
		var rhythmDataIndex;
		var rhythmLastOffset;
		var beatPlayMusicIndex = 0;
		var playerSelf = this;

		this.play = function () {
			var playAudioOffsetInMS = 30;
			var minimumVolume = 0.3;
			var offset = ((new Date()).getTime() - rhythmPlayingStartTime) / rhythmPlayingTimePerBar;
			offset -= Math.floor(offset);
			if(offset < rhythmLastOffset){
				rhythmDataIndex = 0;
			}
			rhythmLastOffset = offset;
			if(rhythmDataIndex < data.length) {
				var nextBeat = data[rhythmDataIndex];
				if(nextBeat.time <= (offset + playAudioOffsetInMS / rhythmPlayingTimePerBar) * 100) {
					var player = document.getElementById("beatPlayMusic" + (beatPlayMusicIndex++ % numberOfAudioFiles));
					var volume = minimumVolume + (1-minimumVolume)*(nextBeat.power/100);
					if(volume < 0)
						volume = 0;

					if(volume > 1)
						volume = 1;

					player.volume = volume; 
					player.play();
					rhythmDataIndex++;
				}
			}
			offset *= $("#line").width();
			rhythmPlayMarker.style.left = Math.round(offset) + "px";
		}

		this.playPause = function() {
			isRhythmPlaying = !isRhythmPlaying;
			if(isRhythmPlaying) {
				$("#playPause").html('<i class="material-icons left">pause</i>Пауза');

				self.calculateAndRedraw();
				rhythmDataIndex = 0;
				rhythmLastOffset = 0;
				rhythmPlayingStartTime = (new Date()).getTime();

				let playLengthElement = $("#play-length");
				if(parseFloat(playLengthElement.val()) > parseFloat(playLengthElement.attr("max")))
						playLengthElement.val(playLengthElement.attr("max"));

				if(parseFloat(playLengthElement.val()) < parseFloat(playLengthElement.attr("min")))
					playLengthElement.val(playLengthElement.attr("min"));

				rhythmPlayingTimePerBar = parseFloat(playLengthElement.val())*1000;
				rhythmPlayingInterval = setInterval(playerSelf.play, 20);
			}
			else {
				$("#playPause").html('<i class="material-icons left">play_arrow</i>Пусни');
				clearInterval(rhythmPlayingInterval);
				rhythmPlayMarker.style.left = "0px";
			}
		}
	}();
}();











var references = new function() {
	var self = this;
	const form = "rhythm";
	const extention = "rhythm";

	this.getForm = function() {
		return $("#" + form);
	}

	this.getExtention = function() {
		return extention;
	}

	this.calculateData = rhythm.calculate;

	this.parseData = rhythm.parseData;

	this.update = rhythm.update;
}();