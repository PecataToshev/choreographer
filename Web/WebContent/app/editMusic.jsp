<%@page import="models.Music"%>
<%@page import="access.AccessMusic"%>
<%@page import="container.EnumContainer.Parameters"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%
String code = request.getParameter(Parameters.ObjectData.CODE.toString());
if(code == null || code.isEmpty() || code.equals("")){
	response.sendRedirect("/Choreographer/app/viewMusic.jsp");
	return;
}
AccessMusic am = new AccessMusic();
Music music = am.getByAccessCode(code);
am.closeEntityManager();
%>


<c:set var="body">
	<form id="updateMusic" enctype="multipart/form-data">
		<div class="input-field">
			<input id="<%= Parameters.ObjectData.NAME %>" name="<%= Parameters.ObjectData.NAME %>"
				type="text" value="<%= music.getName() %>" data-length="50" maxlength="50" required>
			<label for="<%= Parameters.ObjectData.NAME %>">Име на танца</label>
		</div>
		<a class="waves-effect waves-light btn modal-trigger col s6" href="#uplodFile">Променете музикалния файл</a>
		<div class="col s6">
			<div class="switch">
				<label>
					Личен
					<input id="<%= Parameters.ObjectData.ISPUBLIC %>" name="<%= Parameters.ObjectData.ISPUBLIC %>"
						type="checkbox" <%= music.isPublic() ? "checked disabled" : "" %> />
					<span class="lever"></span>
					Публичен
				</label>
			</div>
		</div>
		<div class="clear"></div>


		<div id="uplodFile" class="modal">
			<div class="modal-content">
				<h5>Изберете новия файл</h5>
				<div class="file-field">
					<div class="btn">
						<span>File</span>
						<input type="file" accept=".mp3" name="<%= Parameters.Music.FILE %>" />
					</div>
					<div class="file-path-wrapper">
						<input class="file-path validate" type="text">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<a href="#" class="modal-action modal-close waves-effect waves-red btn-flat">
					Откажи
					<i class="material-icons right">highlight_off</i>
				</a>
				<button href="#" class="modal-action modal-close waves-effect waves-green btn-flat" type="submit">
					Запази
					<i class="material-icons right">send</i>
				</button>
			</div>
		</div>


		<input id="<%= Parameters.ObjectData.CODE %>" name="<%= Parameters.ObjectData.CODE %>" type="hidden" value="<%= music.getAccessCode() %>" required/>
		<input id="<%= Parameters.ObjectData.DATA %>" name="<%= Parameters.ObjectData.DATA %>" type="hidden" required/>





		<div class="centered top-padding-20">
			<jsp:include page="/private/appHTML/basics.jsp">
				<jsp:param name="floating" value="f" />
				<jsp:param name="dataFor" value="музиката" />
				<jsp:param name="hasText" value="t" />
				<jsp:param name="isListedItem" value="f" />
			</jsp:include>
			${imortExport}
			<%
			final String [][] buttons = {
				// {buttonClass, buttonHref, onclickFunction, tooltip, icon, text}
				//{"blue", "#", "rhythm.reload(event);", "Презареди данните за ритъма", "refresh", "Презареди"}
			};

			for(String[] button : buttons){
			%>
				<jsp:include page="/private/appHTML/components/actionButton.jsp">
					<jsp:param name="floating" value="f" />
					<jsp:param name="buttonClasses" value="<%= button[0] %>" />
					<jsp:param name="href" value="<%= button[1] %>" />
					<jsp:param name="onClick" value="<%= button[2] %>" />
					<jsp:param name="dataTooltip" value="<%= button[3] %>" />
					<jsp:param name="icon" value="<%= button[4] %>" />
					<jsp:param name="text" value="<%= button[5] %>" />
				</jsp:include>
			<% } %>
			<button class="btn blue" type="submit"><i class="material-icons left">save</i>Запази музиката</button>
		</div>
	</form>
	<div class="hidden">
		<div id="Timeframe" class="timeFrame">
			<div class="taktContainer" tabindex="0"
				onfocus="select.onFocus(event, this);"
				onfocusout="select.onFocusOut(event, this);"
				>
				<canvas class="TimeframeCanvas" width="100" height="40"></canvas>
			</div>
			<div class="row" style="height: 20px; position: relative;">
				<div style="width: 100px; float: left">
					<select class="rhythmSelect browser-default skip"
						onchange="music.timeframe.rhythm.updateElement(this);">

					</select>
				</div>
				<div class="dragTimeFrame grab"></div>
				<div class="centered" style="width: 30px; float: right;">
					<i class="material-icons tiny white-text hand" onclick="music.timeframe.remove(this);">close</i>
				</div>
			</div>
		</div>
	</div>
	<button id="addToAudioGeneratedData" class="btn waves" onclick="music.timeframe.addNew();">Добави</button>

	<jsp:include page="/private/appHTML/basics.jsp">
		<jsp:param name="playerClass" value="audioEditMusic" />
	</jsp:include>
	${audioPlayer}

	<script type="text/javascript" src="/Choreographer/app/js/editMusic/music.js"></script>
	<script type="text/javascript" src="/Choreographer/app/editMusic.js"></script>
</c:set>

<t:basePage>
	<jsp:attribute name="pageTitle">Редакция на музика</jsp:attribute>
	<jsp:attribute name="sectionClasses">row padding-20</jsp:attribute>
	<jsp:attribute name="_head">
		<link rel="stylesheet" href="/Choreographer/css/jquery-ui.min.css" />
		<script type="text/javascript" src="/Choreographer/js/jquery-ui.min.js"></script>

		<link rel="stylesheet" href="/Choreographer/app/editMusic.css" />
	</jsp:attribute>

	<jsp:body>${body}</jsp:body>
</t:basePage>