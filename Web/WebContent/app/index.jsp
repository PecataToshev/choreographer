<%@page import="head.Basics"%>
<%@page import="container.EnumContainer"%>
<%@page import="head.SessionHandler"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="models.PerformancesAccess"%>
<%@page import="models.Performance"%>
<%@page import="models.User"%>
<%@page import="access.AccessPerformance"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="body">

	<jsp:include page="/private/appHTML/basics.jsp">
		<jsp:param name="objectType" value="<%= EnumContainer.Parameters.Object.PERFORMANCE %>" />
		<jsp:param name="objectName" value="представление" />
	</jsp:include>
	${createObjectForm}





	<h5>Моите представления</h5>
	<hr />
	<div class="bottom-padding-20">
	<%
	User user = SessionHandler.getUser(session);
	List<Performance> lp = null;

	lp = user.getPerformances();
	if(lp != null && lp.size() > 0){
		Performance[] performances = Basics.sortListForPrinting(Performance.class, lp);
		for(Performance p : performances){
			boolean canEdit = !p.isPublic();
			if(canEdit) {
				canEdit = user.equals(p.getUser());
				if(canEdit == false){
					List<PerformancesAccess> lpa = user.getPerformancesAccesses();
					for(PerformancesAccess pa : lpa){
						if(pa.getPerformanceBean().equals(p)){
							canEdit = true;
							break;
						}
					}
				}
			}
			%>
			<jsp:include page="/private/appHTML/components/dataShowRowLink.jsp">
				<jsp:param name="typeName" value="performance" />
				<jsp:param name="id" value='<%= p.getAccessCode() %>' />
				<jsp:param name="name" value='<%= p.getName() %>' />
				<jsp:param name="isPublic" value='<%= p.isPublic() ? "t" : "f" %>' />
				<jsp:param name="canEdit" value='<%= canEdit ? "t" : "v" %>' />
			</jsp:include>
			<%
		}
	} else {
	%>
		<h6 class="center bolded">Нямате собствени представления</h6>
	<%
	}

	%>
	</div>



	<h5>Представления с достъп</h5>
	<hr />
	<div class="bottom-padding-20">
	<%
	Map<Performance, PerformancesAccess> mp = null;
	mp = user.getPerformancesWithAccess();
	if(mp != null && mp.size() > 0) {
		for(Map.Entry<Performance, PerformancesAccess> p : mp.entrySet()){
			%>
			<jsp:include page="/private/appHTML/components/dataShowRowLink.jsp">
				<jsp:param name="typeName" value="performance" />
				<jsp:param name="id" value='<%= p.getKey().getAccessCode() %>' />
				<jsp:param name="name" value='<%= p.getKey().getName() %>' />
				<jsp:param name="isPublic" value='<%= p.getKey().isPublic() ? "t" : "f" %>' />
				<jsp:param name="canEdit" value='<%= (((p.getValue().getRights() == EnumContainer.DB.PerformanceAccess.EDIT) && !p.getKey().isPublic()) ? "t" : "v") %>' />
			</jsp:include>
			<%
		}
	} else {
		%>
		<h6 class="center bolded">Няма представления, до които да ви е осигурен достъп</h6>
		<%
	}

	%>
	</div>




	<h5>Публични представления</h5>
	<hr />
	<div id="publicPerformanceHolder">
	<%
	lp = null;

	AccessPerformance ap = new AccessPerformance();
	lp = ap.getPublic();
	ap.closeEntityManager();

	if(lp != null && lp.size() > 0){
		Performance[] performances = Basics.sortListForPrinting(Performance.class, lp);
		for(Performance p : performances){
			%>
			<jsp:include page="/private/appHTML/components/dataShowRowLink.jsp">
				<jsp:param name="typeName" value="performance" />
				<jsp:param name="id" value='<%= p.getAccessCode() %>' />
				<jsp:param name="name" value='<%= p.getName() %>' />
				<jsp:param name="isPublic" value='t' />
				<jsp:param name="canEdit" value='<%= ((user.equals(p.getUser()) && !p.isPublic()) ? "t" : "v") %>' />
			</jsp:include>
			<%
		}
	} else {
	%>
		<h6 class="center bolded">Няма налични публични представления</h6>
	<%
	}
	%>
	</div>

</c:set>

<t:basePage>
	<jsp:attribute name="pageTitle">Моят профил</jsp:attribute>
	<jsp:attribute name="sectionClasses">row section container</jsp:attribute>
	<jsp:attribute name="_head">
		<script type="text/javascript">
			function edit(link){
				window.location.href = "/Choreographer/app/editPerformance.jsp?CODE=" + link;
			}

			function copy(code) {
				/* 
				 * Parameters.Object.PERFORMANCE
				 */
				copyObject(code, "PERFORMANCE");
			}
		</script>
	</jsp:attribute>

	<jsp:body>${body}</jsp:body>
</t:basePage>