$(document).ready(function(){
	document.addEventListener('contextmenu', function(event) { return event.preventDefault(); });

	$("#updateMusic").submit(function(e){
		e.preventDefault();

		$("#DATA").val(JSON.stringify(music.calculate()));

		$.ajax({
			/* 
			 * Parameters.Action.ACTION
			 * Parameters.Object.MUSIC
			 * Parameters.ObjectData.CODE 
			 */
			url: "/Choreographer/app/ObjectUpdate?ACTION=MUSIC&CODE=" + getParameter("CODE"),
			type: 'POST',
			data: new FormData($("#updateMusic")[0]),
			cache: false,
			contentType: false,
			processData: false,
			success: objectUpdateResult,
			error: errorOnAction
		});
	});

	music.initialize();
});





var references = new function() {
	var self = this;
	const form = "updateMusic";
	const extention = "music";

	this.getForm = function() {
		return $("#" + form);
	}

	this.getExtention = function() {
		return extention;
	}

	this.calculateData = music.calculate;

	this.parseData = music.parseData;

	this.update = music.update;





	var moveEnd = function(e, step) {
		if(select.element) {
			preventAndStop(e);
			select.end += step;
			if(select.end < 0)
				select.end = 0;
			music.timeframe.paint(select.element);
		}
	};

	this.keyDown = function(e, current) {
		if(select.element && select.element.querySelector(".taktContainer") == document.activeElement) {
			if(keyboardHandler.checkKeyPressed(keyboardHandler.map.shift)) {
				if(current == keyboardHandler.map.right) { // move end to right
					moveEnd(e, +1);
				} else if(current == keyboardHandler.map.left) { // move end to left
					moveEnd(e, -1);
				}
			} else if(keyboardHandler.checkKeyPressed(keyboardHandler.map.control)) {
				if(current == keyboardHandler.map.end) { // move end to last element
					preventAndStop(e);
					var takts = select.element.timeframe.takts;
					select.end = takts[takts.length - 1][0];
					music.timeframe.paint(select.element);
				} else if(current == keyboardHandler.map.home) { // move end to 0
					preventAndStop(e);
					select.end = 0;
					music.timeframe.paint(select.element);
				}
			}
		}
	};

	this.keyUp = function(e, current) {};
}();





var select = new function() {
	var selfMain = this;
	this.start; // takt
	this.end; // takt
	this.element; // js


	this.mouse = new function() {
		var self = this;
		var buttonClicked = -1;
		var moveType = -1;
		var movingTakt = -1;
		var mouseDownX = -1;
		var indexFrom = -1;
		var indexTo = -1;
		var initialDataPointsArray = [];

		var reset = function() {
			buttonClicked = -1;
			moveType = -1;
			/* moving types:
			 * 1 - when clicked on first takt from selection 
			 * 		or only 1 takt without selection
			 * 		+ before selection - scale the last takt before selection
			 * 		+ selected are translating (without last selected takt)
			 * 		+ last selected takt - scale
			 * 		+ after selection - no change
			 * 
			 * 2 - when clicked and moving in middle of the selection
			 * 		+ before selection - no change
			 * 		+ selection - scale
			 * 		+ after selection - translate
			*/
			movingTakt = -1;
			mouseDownX = -1;
			indexFrom = -1;
			indexTo = -1;
			initialDataPointsArray = [];
		};

		this.down = new function() {
			var left = function(e, element, baseElement) {
				var isElementChanged = (baseElement != selfMain.element);
				if(isElementChanged) {
					var old = selfMain.element;
					selfMain.element = baseElement;
					if(old) // remove last selection
						music.timeframe.paint(old);
				}

				var rect = element[0].getBoundingClientRect();
				var x = e.clientX - rect.left;
				if(!keyboardHandler.checkKeyPressed(keyboardHandler.map.shift) || isElementChanged)
					selfMain.start = music.timeframe.getTaktIndex(selfMain.element, x);
				selfMain.end = music.timeframe.getTaktIndex(selfMain.element, x);
				music.timeframe.paint(selfMain.element);
			};

			var right = function(e, element, baseElement) {
				var pixelsPerSecond = audio.timeline.getPixelsPerSecond();
				var rect = element[0].getBoundingClientRect();
				mouseDownX = e.clientX - rect.left;
				movingTakt = music.timeframe.getTaktIndex(baseElement, mouseDownX);
				var first = Math.min(selfMain.start, selfMain.end);
				var last  = Math.max(selfMain.start, selfMain.end);

				if(selfMain.start == selfMain.end){
					selfMain.start = movingTakt;
					selfMain.end   = movingTakt;
					first = movingTakt;
					last  = movingTakt;
					moveType = 1;
				} else if(movingTakt == first) {
					moveType = 1;
				} else if(movingTakt > first && movingTakt <= last) {
					moveType = 2;
				}

				if(moveType == 1) { // add to takts
					if(movingTakt == 0){
						reset();
						return;
					}

					music.timeframe.ensureDataPoint(baseElement, first - 1);
					indexFrom = music.timeframe.ensureDataPoint(baseElement, first);
					indexTo   = music.timeframe.ensureDataPoint(baseElement, last - 1);
					music.timeframe.ensureDataPoint(baseElement, last);
					if(indexTo < indexFrom)
						indexTo = indexFrom;
					if(first == last)
						music.timeframe.ensureDataPoint(baseElement, last + 1);

					initialDataPointsArray = cloneJSON(baseElement.timeframe.takts.slice(indexFrom, indexTo + 1));
				} else if(moveType == 2) {
					indexFrom = music.timeframe.ensureDataPoint(baseElement, first);
					indexTo   = music.timeframe.ensureDataPoint(baseElement, last);
					initialDataPointsArray = cloneJSON(baseElement.timeframe.takts.slice(indexFrom, baseElement.timeframe.takts.length/*indexTo + 1*/));
				}
			};

			this.on = function(e, element, baseElement) {
				baseElement.querySelector(".taktContainer").focus();
				buttonClicked = e.which;
				if(buttonClicked == 1) {
					left(e, element, baseElement);
				} else if(buttonClicked == 3) {
					right(e, element, baseElement);
				}
				return true;
			};
		}();





		this.move = new function() {
			var left = function(e, element, baseElement) {
				var rect = element[0].getBoundingClientRect();
				var x = e.clientX - rect.left;
				selfMain.end = music.timeframe.getTaktIndex(selfMain.element, x);
				music.timeframe.paint(selfMain.element);
			};

			var rightType1 = function(e, element, baseElement) {
				var takts = baseElement.timeframe.takts;
				var rhythm = music.timeframe.rhythm.getRhythmByAccessCode(baseElement.timeframe.rhythm);
				var pixelsPerSecond = audio.timeline.getPixelsPerSecond();
				var rect = element[0].getBoundingClientRect();
				var x = e.clientX - rect.left;
				var deltaX = x - mouseDownX;
				var deltaTime = deltaX / pixelsPerSecond;
				var minDeltaTime = takts[indexFrom - 1][1] - initialDataPointsArray[0][1] + rhythm.min;
				var maxDeltaTime;
				if(indexTo >= takts.length - 1)
					maxDeltaTime = 1 << 29;
				else
					maxDeltaTime = takts[indexTo + 1][1] - initialDataPointsArray[initialDataPointsArray.length - 1][1] - rhythm.minLength;

				if(deltaTime < minDeltaTime)
					deltaTime = minDeltaTime;
				if(deltaTime > maxDeltaTime)
					deltaTime = maxDeltaTime;

				for(var i = indexFrom;  i <= indexTo;  i++) {
					takts[i][1] = initialDataPointsArray[i - indexFrom][1] + deltaTime;
				}
				music.timeframe.paint(baseElement);
			};

			var rightType2 = function(e, element, baseElement) {
				var takts = baseElement.timeframe.takts;
				var fromTime = initialDataPointsArray[0][1];
				var toTime   = initialDataPointsArray[indexTo - indexFrom][1];

				var rhythm = music.timeframe.rhythm.getRhythmByAccessCode(baseElement.timeframe.rhythm);
				var pixelsPerSecond = audio.timeline.getPixelsPerSecond();
				var rect = element[0].getBoundingClientRect();
				var x = e.clientX - rect.left;
				var deltaX = x - mouseDownX;
				var deltaTime = deltaX / pixelsPerSecond;

				var minDeltaTime = -(1 << 29);
				var maxDeltaTime = +(1 << 29)
				for(var i = indexFrom + 1;  i <= indexTo;  i++){
					var currentTakt = initialDataPointsArray[i - indexFrom];
					var previousTakt = initialDataPointsArray[i - indexFrom - 1];
					var currentMinDeltaTime = (toTime - fromTime)*(rhythm.minLength*(currentTakt[0] - previousTakt[0]) - (currentTakt[1] - previousTakt[1]))/(currentTakt[1] - previousTakt[1]);
					var currentMaxDeltaTime = (toTime - fromTime)*(rhythm.maxLength*(currentTakt[0] - previousTakt[0]) - (currentTakt[1] - previousTakt[1]))/(currentTakt[1] - previousTakt[1]);
					minDeltaTime = Math.max(minDeltaTime, currentMinDeltaTime);
					maxDeltaTime = Math.min(maxDeltaTime, currentMaxDeltaTime);
				}

				if(deltaTime < minDeltaTime)
					deltaTime = minDeltaTime;
				if(deltaTime > maxDeltaTime)
					deltaTime = maxDeltaTime;

				// scale
				var scaleKoef = ((toTime - fromTime) + deltaTime)/(toTime - fromTime);
				for(var i = indexFrom + 1;  i <= indexTo;  i++){
					takts[i][1] = fromTime + (initialDataPointsArray[i - indexFrom][1] - fromTime)*scaleKoef;
				}

				// translate
				for(var i = indexTo + 1;  i < takts.length;  i++){
					takts[i][1] = initialDataPointsArray[i - indexFrom][1] + deltaTime;
				}
				music.timeframe.paint(baseElement);
			}

			this.on = function(e, element, baseElement) {
				if(buttonClicked == 1) {
					left(e, element, baseElement);
				} else if(buttonClicked == 3) {
					switch (moveType) {
					case 1:
						rightType1(e, element, baseElement);
						break;

					case 2:
						rightType2(e, element, baseElement);
						break;

					default:
						break;
					}
				}
				return true;
			};
		}();





		this.up = new function() {
			this.on = function(e, element) {
				reset();
			}
		}();
	}();





	this.onFocus = function(event, element) {
		const borderSize = 3;
		var parent = element.parentElement;
		var border = document.createElement("div");
		border.style.left   = -borderSize + "px";
		border.style.top    = -borderSize + "px";
		border.style.right  = -borderSize + "px";
		border.style.bottom = -borderSize + "px";
		border.style.border = borderSize + "px solid #82b1ff";
		border.style.pointerEvents = "none";
		border.style.position = "absolute";
		border.style.zIndex = 10;
		parent.appendChild(border);
		parent.border = border;
	};

	this.onFocusOut = function(event, element) {
		var parent = element.parentElement;
		parent.removeChild(parent.border);
	};
}();
