<%@ page language="java" contentType="text/html; charset=UTF-8"
		pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:basePage>
	<jsp:attribute name="pageTitle">Моето място май не е точно тук...</jsp:attribute>
	<jsp:attribute name="sectionClasses"></jsp:attribute>
	<jsp:body>
		<div class="section teal">
			<div class="container">
				<div class="row">
					<div class="col s12">
						<h1 class="header center-on-small-only вхите-теьт">404</h1>
						<h4 class ="light red-text text-lighten-5 center-on-small-only">Page not found.</h4>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col s12 ">
					<div class="section scrollspy">
						<h2 class="header">Are You Lost?</h2>
						<p class="caption">Тази страница май никой не я е намерил досега...</p>
						<a href="/Choreographer/" class="btn-large"><i class="material-icons left">home</i>Беги дом</a>
					</div>
				</div>
			</div>
		</div> 
	</jsp:body>
</t:basePage>
