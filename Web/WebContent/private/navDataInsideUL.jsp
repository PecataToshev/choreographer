<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:choose>
	<c:when test="${param.name eq ''}">
		<li><a href="/Choreographer/sign/in.jsp"><i class="material-icons left">account_circle</i>Вход</a></li>
	</c:when>
	<c:otherwise>
		<li><a href="/Choreographer/app"><i class="material-icons left">account_balance</i>Представления</a></li>
		<li>
			<a class='dropdown-button' href='#' data-activates='${param.prefix}other-functions-dropdown' data-beloworigin="true">
				<i class="material-icons left">whatshot</i>
				Други
			</a>
		</li>
		<li>
			<a class='dropdown-button' href='#' data-activates='${param.prefix}user-dropdown' data-beloworigin="true">
				<i class="material-icons left">reorder</i>
				Потребител
			</a>
		</li>


		<ul id='${param.prefix}other-functions-dropdown' class='dropdown-content'>
			<li><a href="/Choreographer/app/viewRhythm.jsp"><i class="material-icons left">music_note</i>Ритми</a></li>
			<li><a href="/Choreographer/app/viewMusic.jsp"><i class="material-icons left">library_music</i>Музика</a></li>
		</ul>

		<ul id='${param.prefix}user-dropdown' class='dropdown-content'>
			<li><a href="/Choreographer/app/settings.jsp"><i class="material-icons left">settings</i>Настройки</a></li>
			<li class="divider"></li>
			<li><a href="/Choreographer/sign/Out"><i class="material-icons left">exit_to_app</i>Изход</a></li>
		</ul>
	</c:otherwise>
</c:choose>