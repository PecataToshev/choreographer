<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<div class="container">
	<div class="row">
		<div class="col m4 s12">
			<a class="white-text" href="/Choreographer/about/us.jsp">За нас</a>
		</div>
		<div class="col m4 s12">
			<a class="white-text" href="/Choreographer/about/help.jsp">Наръчник за работа със сайта</a>
		</div>
		<div class="col m4 s12">
			<a class="white-text hover" href="/Choreographer/about/contact.jsp">Контакти</a>
		</div>
	</div>
</div>