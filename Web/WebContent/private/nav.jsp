<%@page import="head.SessionHandler"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<nav class="teal navbar-fixed" role="navigation">
	<div class="nav-wrapper container">
		<jsp:include page="/private/appHTML/components/logoHomeLink.jsp">
			<jsp:param name="logoHomeLink_id" value="logo-container" />
			<jsp:param name="logoHomeLink_class" value="brand-logo" />
		</jsp:include>

		<ul class="right hide-on-med-and-down white-text">
			<jsp:include page="/private/navDataInsideUL.jsp">
				<jsp:param name="name" value="<%= SessionHandler.getLoggedUserFirstName(session) %>" />
				<jsp:param name="prefix" value="" />
			</jsp:include>
		</ul>

		<ul id="nav-mobile" class="side-nav">
			<jsp:include page="/private/navDataInsideUL.jsp">
				<jsp:param name="name" value="<%= SessionHandler.getLoggedUserFirstName(session) %>" />
				<jsp:param name="prefix" value="mobile-" />
			</jsp:include>
		</ul>

		<a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons white-text text-lighten-1">menu</i></a>
	</div>
</nav>