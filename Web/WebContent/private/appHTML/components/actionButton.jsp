<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<a 
	class="${(param.removeBTN eq 't') ? '' : 'btn'} ${(param.floating ne 'f') ? 'btn-floating' : ''} tooltipped ${param.buttonClasses}"
	href="${param.href}"
	onclick="${param.onClick}"
	data-position="${(param.tooltipPosition ne '') ? param.tooltipPosition : 'top'}"
	data-delay="50"
	data-tooltip="${param.dataTooltip}"
	>
	<i class="material-icons ${(param.floating ne 't') ? 'left' : ''}">${param.icon}</i>
	${param.text}
</a>