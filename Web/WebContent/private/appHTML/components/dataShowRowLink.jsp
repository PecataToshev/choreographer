<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="publicDataHolder">
	<c:choose>
		<c:when test="${param.typeName eq 'music'}">Музиката е публична!</c:when>
		<c:when test="${param.typeName eq 'performance'}">Представлението е публично!</c:when>
		<c:when test="${param.typeName eq 'rhythm'}">Ритъмът е публичен!</c:when>
	</c:choose>
</c:set>

<div class="holder row">
	<div class="hidden id-holder">${param.id}</div>
	<div class="col s6">${param.name}</div>
	<div class="col s2 centered">
		<c:if test="${param.isPublic eq 't'}">
			<i class="material-icons green-text" title="${publicDataHolder}">public</i>
		</c:if>
	</div>
	<div class="col s2 centered">
		<c:choose>
			<c:when test="${param.canEdit eq 't'}">
				<i class="material-icons hand" title="Редактирай" onclick="edit('${param.id}');">mode_edit</i>
			</c:when>
			<c:when test="${param.canEdit eq 'v'}">
				<i class="material-icons hand" title="Прегледай" onclick="edit('${param.id}');">remove_red_eye</i>
			</c:when>
		</c:choose>
	</div>
	<div class="col s2 centered">
		<i class="material-icons hand" title="Копирай" onclick="copy('${param.id}');">content_copy</i>
	</div>
</div>