<%@page import="container.EnumContainer.Parameters"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<c:set var="isPublic" scope="request">
	<c:choose>
		<c:when test="${param.isObjectPublic eq 't'}">
			<i class="material-icons tooltipped green-text" data-tooltip="${param.publicObjectText}">public</i>
		</c:when>
		<c:otherwise>
			<script type="text/javascript">
				function alertForGetingPublic(e) {
					document.body.appendChild(modalWarningBeforePublic);
					if(!$('#<%= container.EnumContainer.Parameters.ObjectData.ISPUBLIC %>')[0].checked) {
						/* not checked */
						preventAndStop(e);
						$("#modalWarningBeforePublic").modal('open');
					}
				}
			</script>
			<div class="switch">
				<label>
					Лично
					<input id="<%= container.EnumContainer.Parameters.ObjectData.ISPUBLIC %>" 
						name="<%= container.EnumContainer.Parameters.ObjectData.ISPUBLIC %>"
						type="checkbox" >
					<span class="lever" onclick="alertForGetingPublic(event);"></span>
					Публично
				</label>
			</div>
			<div id="modalWarningBeforePublic" class="modal">
				<div class="modal-content">
					<h4>Предупреждение</h4>
					<p>
						Внимание, ако продължите операцията обектът ще стане публичен и 
						няма да може да бъде редактиран вече! Наистина ли желаете да продължите?
					</p>
				</div>
				<div class="modal-footer">
					<a href="#" class="modal-close waves-effect waves-red btn-flat">
						Не<i class="material-icons right">highlight_off</i>
					</a>
					<a href="#" class="modal-close waves-effect waves-green btn-flat"
						onclick="$('#<%= container.EnumContainer.Parameters.ObjectData.ISPUBLIC %>')[0].checked = true;">
						Да<i class="material-icons right">send</i>
					</a>
				</div>
			</div>
		</c:otherwise>
	</c:choose>
</c:set>





<c:set var="createObjectForm" scope="request">
	<form id="createObject" class="modal">
		<input name="<%= Parameters.Action.ACTION %>" type="hidden" value="${param.objectType}" />
		<div class="modal-content">
			<h4 class="row">Добави ${param.objectName}</h4>
			<div class="row input-field">
				<input type="text" id="newObjectName" class="validate"
					name="<%= Parameters.ObjectData.NAME %>" data-length="50"
					required aria-required="true" />
				<label for="newObjectName">Име</label>
			</div>
		</div>
		<div class="modal-footer">
			<a href="#" class="modal-action modal-close waves-effect waves-red btn-flat">
				Откажи<i class="material-icons right">highlight_off</i>
			</a>
			<button class="waves-effect waves-green btn-flat" type="submit">
				Добави <i class="material-icons right">send</i>
			</button>
		</div>
	</form>

	<div class="fixed-action-btn">
		<jsp:include page="/private/appHTML/components/actionButton.jsp">
			<jsp:param name="buttonClasses" value="btn-large modal-trigger red" />
			<jsp:param name="href" value="#createObject" />
			<jsp:param name="dataTooltip" value="Добави ${param.objectName}" />
			<jsp:param name="icon" value="add" />
		</jsp:include>
	</div>

	<script type="text/javascript">
	$(document).ready(function(){
		$("#createObject").submit(function(e){
			e.preventDefault();

			$.ajax({
				url: "/Choreographer/app/ObjectCreate",
				type: "POST",
				data: $("#createObject").serialize(),
				success: function(result){
					if(result.substring(0, 3) == "OK:"){
						edit(result.substring(3));
					} else if(result == "EMPTY OBJETC REQUEST"){
						Materialize.toast(
								'Липсва важна част за създаването на нов обект. Моля обновете страницата и опитайте отново!',
								10000
							);
					}else {
						toastError();
					}
				},
				error: function(result){
					toastError();
				}
			});
		});
	});
	</script>
</c:set>





<c:set var="audioPlayer" scope="request">
	<div id="audioContainer" class="${param.playerClass}">
		<div id="audioTimelineContainer">
			<div style="width:10px;height:40px"></div>
			<div class="range-field">
				<input id="audioPlayerRange" type="range"
					value="0" min="0" max="100" step="0.001" onchange="audio.player.onClickOver();" />
			</div>
			<div id="audioTimeline"></div>
			<div id="audioTimelineCustom" style="position:relative;left:20px;"></div>
		</div>

		<div id="audioConstrols">
			<audio id="audioFileHolder" onloadeddata='audio.player.ready()'>
				Грешка! Моля опитайте по-късно!
			</audio>
			<button id="audioButtonPlayPause" class="btn red lighten-1 left" stat="pause" onclick="audio.player.playPause()">
				<i class="material-icons">play_arrow</i>
			</button>
		</div>


		<div class="hidden">
			<div id="audioTimestamp" class="audioTimestamp">
				<div style=""></div>
				<span class="audioTimestampTime"></span>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="/Choreographer/app/js/audio.js"></script>
	<link rel="stylesheet" href="/Choreographer/app/audio.css" />
</c:set>





<c:set var="imortExport" scope="request">
	<%
	final String [][] buttons = {
		// {onclickFunction, tooltip prefix, icon, text}
		{"importExport.exportData(event);", "Изтегли данните", "file_download", "Изтегли"},
		{"importExport.importData(event);", "Вмъкни данни", "file_upload", "Вмъкни"}
	};

	for(String[] button : buttons){
		pageContext.setAttribute("availableText", button[3]);
		pageContext.setAttribute("dataTooltipObject", button[1] + " за " + request.getParameter("dataFor"));
	%>
		${(param.isListedItem eq 'f') ? '' : "<li>"}
			<jsp:include page="/private/appHTML/components/actionButton.jsp">
				<jsp:param name="floating" value="${param.floating}" />
				<jsp:param name="removeBTN" value="${param.removeBTN}" />
				<jsp:param name="tooltipPosition" value="${param.tooltipPosition}" />
				<jsp:param name="buttonClasses" value="blue ${param.buttonClasses}" />
				<jsp:param name="href" value="" />
				<jsp:param name="onClick" value="<%= button[0] %>" />
				<jsp:param name="dataTooltip" value="${dataTooltipObject}" />
				<jsp:param name="icon" value="<%= button[2] %>" />
				<jsp:param name="text" value="${(param.hasText eq 't') ? availableText : ''}" />
			</jsp:include>
		${(param.isListedItem eq 'f') ? '' : "</li>"}
	<% } %>
	<input type="file" id="importFileHolder" class="hidden" />
	<script type="text/javascript" src="/Choreographer/app/js/importExport.js"></script>
</c:set>