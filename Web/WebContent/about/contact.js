$(document).ready(function(){
	$("#contactForm").submit(function(e){
		e.preventDefault();
		Materialize.Toast.removeAll();

		$.ajax({
			url: "/Choreographer/about/ContactForm",
			type: "POST",
			data: $("#contactForm").serialize(),
			success: function(result){
				if(result == "OK"){

					$("#contactForm")[0].reset();
					$("#contactForm").slideUp("fast", function(e){
						$("#thankYou").slideDown("slow");
					});

				} else if(result == "NOT-OK") {
					Materialize.toast('Нещо се обърка!<br />Моля прооверете валидността на вашия e-mail и опитайте отново!<button class="btn-flat toast-action" onclick="$(this).closest(\'.toast\').first()[0].M_Toast.remove();">X</button>', 30000);
				} else {
					toastError();
				}
			}
		});
	});
});
