<%@ page language="java" contentType="text/html; charset=UTF-8"
		pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:basePage>
	<jsp:attribute name="pageTitle">Помощ за работа с приложението</jsp:attribute>
	<jsp:attribute name="sectionClasses">row section container</jsp:attribute>
	<jsp:attribute name="_head">
		<script type="text/javascript">
			$(document).ready(function(){
				$('.collapsible').collapsible();
			});
		</script>
		<style>
			img.maximize {
				width: 100% !important;
			}
		</style>
	</jsp:attribute>

	<jsp:body>
		<ul class="collapsible popout" data-collapsible="accordion">
			<li>
				<div class="collapsible-header"><i class="material-icons left">account_balance</i>Представления</div>
				<div class="collapsible-body">
					<img class="maximize" src="/Choreographer/img/performancesView.png" />

					<p class="justify">
						Тук са показани представленията, които можете да виждате. Разделени са в три категории - "Моите представления",
						"Представления с достъп" и "Публични представления". Всеки потребител може да редедактира представленията, 
						които имат иконката <i class="material-icons">mode_edit</i>. Когато такова представление бъде редактирано 
						и промените бъдат запаметени, те се променят за постоянно. Представленията, които имат иконка 
						<i class="material-icons">remove_red_eye</i>, при направени промени няма да бъдат запаметени, поради неосигурен 
						достъп от създателя на представлението.
					</p>

					<div class="bolded">Функции</div>
					<ol>
						<li class="justify">
							Чрез иконката <i class="material-icons green-text">public</i> се обозначават публичните представления.
						</li>
						<li class="justify">
							При натискане на <i class="material-icons">content_copy</i> представлението се копира, като собствен за текущия 
							потребител.
						</li>
						<li class="justify">
							Чрез натискане на бутона 
							<span class="btn btn-floating btn-large red"><i class="material-icons">add</i></span> 
							се появява форма за добавяне на ново представление. След попълването на формата и натискане 
							набутона добави, се добавя ново лично представление.
						</li>
					</ol>
				</div>
			</li>

			<li>
				<div class="collapsible-header">
					<i class="material-icons left">account_balance</i>
					<i class="material-icons left">mode_edit</i>
					Редакция на представления
				</div>
				<div class="collapsible-body">
					<img class="maximize" src="/Choreographer/img/performanceEditTop.png" />
					<label>Изображение 1</label>

					<p class="justify">
						Чрез тази страница се редактират представленията. На <span class="italic">Изображение 1</span> 
						е показан изгледът при отваряне на представлението (отгоре).
					</p>

					<div class="bolded">Функции</div>
					<ol>
						<li class="justify">
							<span class="bolded">Промяна на ъгъла на камерата</span> - чрез задържане на клавиша 
							<span class="bolded italic">ctrl</span> и местене на мишката с натиснат 
							<span class="bolded italic">десен</span> бутон на мишката в полето за редакция.
						</li>
						<li class="justify">
							<span class="bolded">Местене спрямо равнината на камерата</span> - чрез задържане на клавиша 
							<span class="bolded italic">ctrl</span> и местене на мишката с натиснат 
							<span class="bolded italic">ляв</span> бутон на мишката в полето за редакция.
						</li>
						<li class="justify">
							<span class="bolded">Приближаване и отдалечаване</span> - чрез задържане на клавиша 
							<span class="bolded italic">ctrl</span> и <span class="bolded italic">среден</span> 
							бутон на мишката в полето за редакция.
						</li>
						<li class="justify">
							<span class="bolded">Бърз преглед</span> - чрез натискане и задържане на 
							<span class="bolded italic">ляв</span> бутон на мишката и местенето й през 
							времевата линия за изпълнителите, показва местоположението на изпълнителите в дадения 
							такт.
						</li>
						<li class="justify">
							<span class="bolded">Преместване на хората</span> - извършва се чрез избиране на човека и 
							на ключовия момент, в който изпълнителят да се намира в позиция. След което с кликане 
							се избира местоположението на човека.
							<br />
							<span class="bolded italic">Забележка:</span> Ако няма клчов момент, автоматично се добавя.
						</li>
						<div class="bolded italic">
							<span style="text-decoration: underline;">Останалите функции се появяват при 
								настикане на бутона за редакция</span>
							 - 
							<span class="btn-floating btn-large red">
								<i class="large material-icons">mode_edit</i>
							</span>
						</div>
						<li class="justify">
							<span class="bolded">Добавяне на танцьори</span> - чрез бутона 
							<a class="btn btn-floating green"><i class="material-icons">add</i></a> и след попълнена и изпратена 
							форма се добавя нов танцьор. 
						</li>
						<li class="justify">
							<span class="bolded">Запазване на разстоянието от</span> - чрез бутона 
							<a class="btn btn-floating yellow darken-2"><i class="material-icons">settings_ethernet</i></a> 
							и след попълнена и изпратена форма се изпълнява запазването на разстоянието на избрания изпълнител 
							от друг танцьор до следващ ключов момент.
						</li>
						<li class="justify">
							<span class="bolded">Следване на</span> - чрез бутона 
							<a class="btn btn-floating yellow darken-2"><i class="material-icons">reply_all</i></a> 
							и след попълнена и изпратена форма се изпълнява следването на избрания изпълнител на друг танцьор 
							до следващ ключов момент.
						</li>
						<li class="justify">
							<span class="bolded">Премахване на избрания важен момент</span> - след натискане на бутона 
							<a class="btn btn-floating red"><i class="material-icons">filter_frames</i></a> 
							се премахва избраният ключов момент.
						</li>
						<li class="justify">
							<span class="bolded">Премахване на избрания изпълнител</span> - след натискане на бутона 
							<a class="btn btn-floating red"><i class="material-icons">account_circle</i></a> 
							се премахва избраният изпълнител.
						</li>
						<li class="justify">
							<span class="bolded">Запазване на прогреса</span> - след натискане на бутона 
							<a class="btn btn-floating blue"><i class="material-icons">save</i></a> 
							се запазва текущият прогрес на представлението.
						</li>
						<li class="justify">
							<span class="bolded">Презареждане на данните</span> - след натискане на бутона 
							<a class="btn btn-floating blue"><i class="material-icons">refresh</i></a> 
							данните се презареждат и <span class="bolded">НЕ СЕ ЗАПАМЕТЯВАТ</span>.
						</li>
						<li class="justify">
							<span class="bolded">Настройки</span> - след натискане на бутона 
							<a class="btn btn-floating blue"><i class="material-icons">settings</i></a> 
							се показва форма за настройките на представлението.
						</li>
					</ol>
				</div>
			</li>

			<li>
				<div class="collapsible-header"><i class="material-icons left">music_note</i>Ритми</div>
				<div class="collapsible-body">
					<img class="maximize" src="/Choreographer/img/rhythmView.png" />

					<p class="justify">
						Тук са показани ритмите, които могат да бъдат използвани при разпознаването на разположението на 
						тактовете при автоматичното им намиране. Разделени са в три категории - "Лични ритми",
						"Вградени ритми" и "Публични ритми". Всеки потребител може да редедактира само собствените си ритми (тези, 
						които имат иконката <i class="material-icons">mode_edit</i>). Когато такъв ритъм бъде редактиран 
						и промените бъдат запаметени, те се променят за постоянно.
					</p>

					<div class="bolded">Функции</div>
					<ol>
						<li class="justify">
							Чрез иконката <i class="material-icons green-text">public</i> се обозначават публичните ритми.
						</li>
						<li class="justify">
							При натискане на <i class="material-icons">content_copy</i> ритъмът се копира, като личен за текущия 
							потребител.
						</li>
						<li class="justify">
							Чрез натискане на бутона 
							<span class="btn btn-floating btn-large red"><i class="material-icons">add</i></span> 
							се появява форма за добавяне на нов ритъм. След попълването на формата и натискане набутона добави, 
							се добавя нов личен ритъм.
						</li>
						<li class="justify">

						</li>
					</ol>
				</div>
			</li>

			<li>
				<div class="collapsible-header">
					<i class="material-icons left">music_note</i>
					<i class="material-icons left">mode_edit</i>
					Редакция на ритми
				</div>
				<div class="collapsible-body">
					<img class="maximize" src="/Choreographer/img/rhythmEdit.png" />

					<p class="justify">
						Чрез тази страница е възможна промяната на ритмите. Чрез синьото разграфено поле се 
						определя разположението на ударите в такта и тяхната сила. Всяко червено стълбче 
						е удар, а височината му - сила на удара. Чрез бутона добави се добавя ново стълбче. 
						Ударите в такта могат да се премахват, чрез натискането на <span class="bolded">Х</span> в долната част на стълбчето.
						<br />
						При натискане на бутона пусни се възпроизвежда създаденият ритъм. Скоростта за изпълнение на 
						1 такт се измерва в секунди, и се задава от полето 
						<span class="italic">Дължина на такта в секунди</span>. В дясната част на редактора 
						стоят детайлите за ритъма - <span class="italic">Минимална дължина на такта в секунди</span>, 
						<span class="italic">Максимална дължина на такта в секунди</span>, Описание на ритъма и 
						дали ритъмът е публичен.
						<br /><br />
						Чрез бутона <a class="btn blue" href="#"><i class="material-icons left">file_download</i>Изтегли</a> 
						може да бъде изтеглено текущото състояние на ритъма. То ще бъде започнато изтеглянето на файл, чието име ще е 
						текущото име на ритъма.
						<br /><br />
						Чрез бутона <a class="btn blue" href="#"><i class="material-icons left">file_upload</i>Вмъкни</a> 
						могат да се вмъкват *.rhythm файлове, като да се зареждат настройките им в текущия ритъм.
						<br /><br />
						<span class="bolded italic"><span class="red-text">ВАЖНО</span>: вмъкнатите файлове <span class="red-text">НЕ</span> 
						се запаметяват автоматично!</span>
						<br /><br />
						Чрез бутона <a class="btn blue" href="#"><i class="material-icons left">refresh</i>Презареди</a> 
						се презареждат настройките на ритъма от базата данни, като се <span class="bolded red-text">ГУБЯТ</span> всички незапаметени корекции.
					</p>
				</div>
			</li>

			<li>
				<div class="collapsible-header"><i class="material-icons left">library_music</i>Музика</div>
				<div class="collapsible-body bolded">
					Този интерфейс все още се разработва!
				</div>
			</li>
		</ul>
	</jsp:body>
</t:basePage>