<%@page import="head.SessionHandler"%>
<%@page import="container.EnumContainer.Session"%>
<%@page import="models.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="container.EnumContainer.Parameters.ContactForm"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<%
String name = "";
String email = "";
if(SessionHandler.isLoggedIn(session)){
	User u = (User) session.getAttribute(Session.USER.toString()); 
	name = u.getFirstName() + " " + u.getLastName();
	email = u.getEmail();
}
%>

<c:set var="body">
	<div class="col s12 m6">
		<h3 class="centered">Връзка с нас</h3>
		Можете да се свържете с нас на E-mail: <a href="mailto:support@mail.choreographer.tk">support@mail.choreographer.tk</a>, или чрез формата за контакт.
	</div>

	<form id="contactForm" class="col s12 m6" style="padding-top: 20px;">
		<div class="row input-field">
			<i class="material-icons prefix">account_box</i>
			<input id="<%= ContactForm.NAME %>" name="<%= ContactForm.NAME %>" type="text" class="validate" value="<%= name %>" required>
			<label for="<%= ContactForm.NAME %>">Име и фамилия <span class="red-text">*</span></label>
		</div>
		<div class="row input-field">
			<i class="material-icons prefix">email</i>
			<input id="<%= ContactForm.EMAIL %>" name="<%= ContactForm.EMAIL %>" type="email" class="validate" value="<%= email %>" required>
			<label for="<%= ContactForm.EMAIL %>">E-mail за обратна връзка <span class="red-text">*</span></label>
		</div>
		<div class="row input-field">
			<i class="material-icons prefix">short_text</i>
			<input id="<%= ContactForm.TITLE %>" name="<%= ContactForm.TITLE %>" type="text" class="validate" required>
			<label for="<%= ContactForm.TITLE %>">Тема <span class="red-text">*</span></label>
		</div>
		<div class="row input-field">
			<i class="material-icons prefix">insert_comment</i>
			<textarea id="<%= ContactForm.CONTENT %>" name="<%= ContactForm.CONTENT %>"
					class="validate materialize-textarea" required style="max-height: 150px; min-height: 20px;"></textarea>
			<label for="<%= ContactForm.CONTENT %>">Споделете Вашето мнение/запитване/препоръка <span class="red-text">*</span></label>
		</div>


		<button class="btn waves-effect waves-light right" type="submit" name="action">
			Изпрати
			<i class="material-icons right">send</i>
		</button>
	</form>

	<div id="thankYou" class="col s12 m6" style="display: none;">
		<h4>Успешно изпратено!</h4>
		<p>
			Благодарим за обратната връзка! <br />
			Екипът на Choreographer
		</p>
	</div>
</c:set>

<t:basePage>
	<jsp:attribute name="pageTitle">Връзка с нас</jsp:attribute>
	<jsp:attribute name="sectionClasses">row container</jsp:attribute>
	<jsp:attribute name="_head">
		<script src="/Choreographer/about/contact.js"></script>
	</jsp:attribute>
	<jsp:body>${body}</jsp:body>
</t:basePage>