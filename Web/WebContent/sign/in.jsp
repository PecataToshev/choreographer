<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:sign signIn="t">
	<jsp:attribute name="pageTitle">Вход</jsp:attribute>
</t:sign>

<script type="text/javascript">
var file = "In";

function logRegSuccess(data){
	let res = false;
	if(data == "0"){
		Materialize.toast('Грешно потребителско име и/или парола', 20000);
		res = true;
	}
	return res;
}
</script>