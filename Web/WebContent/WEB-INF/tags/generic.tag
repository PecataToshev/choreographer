<%@ tag pageEncoding="UTF-8" %>
<%@ attribute name="pageTitle" fragment="true" required="true"%>
<%@ attribute name="nav" required="true" %>
<%@ attribute name="footer" required="true" %>

<%@ attribute name="sectionClasses" fragment="true" %>
<%@ attribute name="_head" fragment="true" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="icon" href="/Choreographer/img/icon.png" />

	<title><jsp:invoke fragment="pageTitle" /></title>

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
	<script src="/Choreographer/js/jquery-3.3.1.min.js"></script>

	<link rel="stylesheet" href="/Choreographer/css/materialize.min.css" />
	<script src="/Choreographer/js/materialize.min.js"></script>

	<link rel="stylesheet" href="/Choreographer/css/generic.css" />
	<script type="text/javascript" src="/Choreographer/js/generic.js"></script>
	<script type="text/javascript" src="/Choreographer/js/keyboardHandler.js"></script>
	<jsp:invoke fragment="_head" />
</head>
<body>
	<c:if test="${nav eq 't'}">
		<jsp:include page="/private/nav.jsp" />
	</c:if>

	<section>
		<div class='<jsp:invoke fragment="sectionClasses" />'>
			<jsp:doBody />
		</div>
	</section>

	<footer class="page-footer blue-grey center">
		<c:if test="${footer eq 't'}">
			<jsp:include page="/private/footer.jsp" />
		</c:if>
		<div class="footer-copyright justify">
			<span class="center-align valign-wrapper">
				<i class="material-icons">copyright</i> Petar Toshev 2017
			</span>
		</div>
	</footer>
</body>
</html>