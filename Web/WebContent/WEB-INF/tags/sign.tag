<%@ tag import="container.EnumContainer.Parameters.Sign"%>
<%@ tag pageEncoding="UTF-8" %>
<%@ attribute name="pageTitle" fragment="true" required="true" %>
<%@ attribute name="signIn" required="true" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<c:set var="body" scope="page">
	<div class="col s12 m8 l6 offset-m2 offset-l3 z-depth-4">
		<form id="log-reg">
			<div class="row center"><jsp:include page="/private/appHTML/components/logoHomeLink.jsp" /></div>
			<c:if test="${signIn eq 'f'}">
				<div class="row">
					<div class="input-field col s12 m6">
						<i class="material-icons prefix">account_circle</i>
						<input id="<%= Sign.FIRST_NAME %>" name="<%= Sign.FIRST_NAME %>" type="text" class="validate" data-length="35" required>
						<label for="<%= Sign.FIRST_NAME %>">First Name</label>
					</div>
					<div class="input-field col s12 m6">
						<input id="<%= Sign.LAST_NAME %>" name="<%= Sign.LAST_NAME %>" type="text" class="validate" data-length="35" required>
						<label for="<%= Sign.LAST_NAME %>">Last name</label>
					</div>
				</div>
			</c:if>
			<div class="row">
				<div class="input-field col s12">
					<i class="material-icons prefix">email</i>
					<input id="<%= Sign.EMAIL %>" name="<%= Sign.EMAIL %>" type="email" class="validate" data-length="255" required>
					<label for="<%= Sign.EMAIL %>" data-error="Невалиден e-mail адрес">E-mail</label>
				</div>
			</div>
			<div class="row">
				<div class="input-field col s12">
					<i class="material-icons prefix">lock_outline</i>
					<input id="<%= Sign.PASSWORD %>" name="<%= Sign.PASSWORD %>" type="password" class="validate" required>
					<label for="<%= Sign.PASSWORD %>">Password</label>
				</div>
			</div>
			<c:if test="${signIn eq 'f'}">
				<div class="row">
					<div class="input-field col s12">
						<i class="material-icons prefix"></i>
						<input id="<%= Sign.REPASSWORD %>" name="<%= Sign.REPASSWORD %>" type="password" class="validate" required>
						<label for="<%= Sign.REPASSWORD %>">Repeat password</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<i class="material-icons prefix">face</i>
						<select name="<%= Sign.GENDER %>" required>
							<option disabled selected>Аз съм...</option>
							<option value="1">Мъж</option>
							<option value="2">Жена</option>
						</select>
					</div>
				</div>
			</c:if>
			<div class="row center-on-small-only">
				<div class="input-field col s12">
					<c:choose>
						<c:when test="${signIn eq 'f'}">
							<a class="" href="/Choreographer/sign/in.jsp">Вече имаш акаунт?</a>
						</c:when>
						<c:otherwise>
							<a class="" href="/Choreographer/sign/up.jsp">Нямаш акаунт?</a>
						</c:otherwise>
					</c:choose>
					<button class="btn waves-effect waves-light right" type="submit" name="action">
						<c:choose>
							<c:when test="${signIn eq 'f'}">Регистрирай се</c:when>
							<c:otherwise>Влез</c:otherwise>
						</c:choose>
						<i class="material-icons right">send</i>
					</button>
				</div>
			</div>
		</form>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {
			if($(document).width() <= 600){ // fix align for last name on small devices
				$("#<%= Sign.LAST_NAME %>").parent().prepend("<i class='prefix'></i>");
			}

			$("#log-reg").submit(function(e){
				e.preventDefault();
				Materialize.Toast.removeAll();

				$.ajax({
					url: "/Choreographer/sign/" + file,
					method: "POST",
					data: $("#log-reg").serialize(),
					success: function(data){
						if(data == "1")
							window.location.href = "/Choreographer/app/";
						else if(logRegSuccess(data) == false)
							toastError();
					}
				});
			});
		});
	</script>

	<style type="text/css">
		#log-reg {
			padding-top: 10px;
			padding-bottom: 10px;
		}

		#log-reg > div.row.center:first-child  img {
			width: 100px;
			margin: 0px auto;
		}

		.row {
			margin-bottom: 11px;
		}

		section > .row {
			margin: 0px;
		}
	</style>
</c:set>

<t:generic nav="f" footer="f">
	<jsp:attribute name="pageTitle">
		<jsp:invoke fragment="pageTitle" />
	</jsp:attribute>
	<jsp:attribute name="sectionClasses">row</jsp:attribute>
	<jsp:body>${body}</jsp:body>
</t:generic>