<%@ tag pageEncoding="UTF-8" %>
<%@ attribute name="pageTitle" fragment="true" required="true"%>
<%@ attribute name="_head" fragment="true" %>
<%@ attribute name="sectionClasses" fragment="true" %>

<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:generic nav="t" footer="t">
	<jsp:attribute name="pageTitle">
		<jsp:invoke fragment="pageTitle" />
	</jsp:attribute>

	<jsp:attribute name="sectionClasses">
		<jsp:invoke fragment="sectionClasses" />
	</jsp:attribute>

	<jsp:attribute name="_head">
		<jsp:invoke fragment="_head" />
	</jsp:attribute>

	<jsp:body>
		<jsp:doBody />
	</jsp:body>
</t:generic>