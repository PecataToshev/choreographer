<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:basePage>
	<jsp:attribute name="pageTitle">Начало</jsp:attribute>
	<jsp:attribute name="_head"><link rel="stylesheet" href="/Choreographer/css/parallax.css" /></jsp:attribute>
	<jsp:body>
		<div id="index-banner" class="parallax-container">
			<div class="section no-pad-bot container">
				<h2 class="header center teal-text text-lighten-2">Choreographer</h2>
				<div class="row center">
					<h5 class="header col s12 teal-text text-lighten-2">Създаването и научаването на танци вече не е проблем!</h5>
				</div>
				<div class="row center">
					<a href="/Choreographer/sign/up.jsp" class="btn-large waves-effect waves-light teal lighten-1">Започни сега!</a>
				</div>
			</div>
			<div class="parallax"><img src="/Choreographer/img/screenshot01.png" alt=""></div>
		</div>

		<div class="container section row">
			<div class="col s12 m4 justify">
				<div class="icon-block">
					<h2 class="center brown-text"><i class="material-icons">flash_on</i></h2>
					<h5 class="center">Улеснява научаването</h5>

					<p class="light">Всеки танцьор се е сблъсквал с незнанието на някой танц. Чрез Choreographer всеки може лесно и бързо да научи всеки един танц, без да се губи ценното време на репетициите!</p>
				</div>
			</div>

			<div class="col s12 m4 justify">
				<div class="icon-block">
					<h2 class="center brown-text"><i class="material-icons">group</i></h2>
					<h5 class="center">Подпомага колективната работа</h5>

					<p class="light">Без изгубено време на репетициите за научаване на стари танци новите представления също изискват активна и упорита работа.</p>
				</div>
			</div>

			<div class="col s12 m4 justify">
				<div class="icon-block">
					<h2 class="center brown-text"><i class="material-icons">settings</i></h2>
					<h5 class="center">Лесен за употреба</h5>

					<p class="light">Choreographer е лесен за употреба инструмент, насочен към възможно най-добро потребителско изживяване.</p>
				</div>
			</div>
		</div>

		<script type="text/javascript">
			$('.button-collapse').sideNav();
			$('.parallax').parallax();
		</script>
	</jsp:body>
</t:basePage>