CREATE TABLE public.rhythm (
	variable(id),
	variable(timestamp),
	variable(owner),
	variable(access_code),
	variable(name),
	variable(data),
	description VARCHAR(255) NOT NULL DEFAULT '',
	min_length REAL NOT NULL DEFAULT 0.1,
	max_length REAL NOT NULL DEFAULT 1.0,
	variable(is_public),
	built_in BOOLEAN NOT NULL DEFAULT FALSE
);