CREATE TABLE public.logs (
	variable(id),
	variable(timestamp),
	error_message TEXT NOT NULL,
	stack_trace TEXT NOT NULL,
	user_id INTEGER REFERENCES public.users
);