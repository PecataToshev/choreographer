CREATE TABLE public.music (
	variable(id),
	variable(timestamp),
	variable(owner),
	variable(access_code),
	variable(name),
	variable(is_public),
	variable(data),
	generated_data TEXT DEFAULT NULL,
	path VARCHAR(50) DEFAULT NULL
);