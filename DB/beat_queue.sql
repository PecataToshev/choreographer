CREATE TABLE public.beat_queue (
	variable(id),
	variable(timestamp),
	music INTEGER REFERENCES public.music NOT NULL,
	rhythms TEXT DEFAULT '[]' NOT NULL,
	start_time REAL NOT NULL,
	end_time REAL NOT NULL,
	variable(owner),
	variable(data),
	recognized BOOLEAN NOT NULL DEFAULT FALSE
);