CREATE TYPE public.performance_rights AS ENUM ('READ', 'EDIT');

CREATE TABLE public.performances_access (
	variable(id),
	variable(timestamp),
	variable(owner),
	user_id INTEGER REFERENCES public.users NOT NULL,
	performance_id INTEGER REFERENCES public.performances NOT NULL,
	rights performance_rights NOT NULL
);