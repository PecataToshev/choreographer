CREATE TABLE public.performances (
	variable(id),
	variable(timestamp),
	variable(owner),
	variable(access_code),
	variable(name),
	variable(data),
	music INTEGER REFERENCES public.music DEFAULT NULL,
	variable(is_public)
);