CREATE TABLE public.users (
	variable(id),
	variable(timestamp),
	email VARCHAR(255) NOT NULL,
	first_name VARCHAR(35) NOT NULL,
	last_name VARCHAR(35) NOT NULL,
	gender BOOLEAN NOT NULL, -- true - male, false - female
	password VARCHAR(64) NOT NULL, -- user password hashed with sha256
	last_login TIMESTAMP DEFAULT NOW()
);